package com.addcel.billplease.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionEnquiryResponse {
	
	private ServiceResult result;
	private Integer transactionType;
	private String transactionReference;
	private String clientReference;
	private double amount;
	private Integer status;
	private String idAccount;
	private String trackingCode;
	private String movementNumber;
	private String h2HResponse;
	
	public TransactionEnquiryResponse() {
		// TODO Auto-generated constructor stub
	}

	public ServiceResult getResult() {
		return result;
	}

	public void setResult(ServiceResult result) {
		this.result = result;
	}

	public Integer getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getClientReference() {
		return clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}

	public String getTrackingCode() {
		return trackingCode;
	}

	public void setTrackingCode(String trackingCode) {
		this.trackingCode = trackingCode;
	}

	public String getMovementNumber() {
		return movementNumber;
	}

	public void setMovementNumber(String movementNumber) {
		this.movementNumber = movementNumber;
	}

	public String getH2HResponse() {
		return h2HResponse;
	}

	public void setH2HResponse(String h2hResponse) {
		h2HResponse = h2hResponse;
	}
	
	

}
