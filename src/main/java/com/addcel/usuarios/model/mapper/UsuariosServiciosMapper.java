package com.addcel.usuarios.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.usuarios.model.vo.Bank;
import com.addcel.usuarios.model.vo.BankCodes;
import com.addcel.usuarios.model.vo.CardCommerce;
import com.addcel.usuarios.model.vo.Establecimiento;
import com.addcel.usuarios.model.vo.ImgProfile;
import com.addcel.usuarios.model.vo.ProcessTagsVO;
import com.addcel.usuarios.model.vo.ProjectMC;
import com.addcel.usuarios.model.vo.TUsuarios;
import com.addcel.usuarios.model.vo.TemplateCorreo;
import com.addcel.usuarios.model.vo.AppCipher;
import com.mysql.jdbc.Blob;


public interface UsuariosServiciosMapper {

	public String guardarUsuario(TUsuarios usuario);

	public String ProcessCard(HashMap parameters);

	public String loginUsuario(TUsuarios usuario);
	
	public String login(TUsuarios usuario);

	public String getParameter(@Param(value = "parameter") String parameter);

	public String userActivate(@Param(value = "user") String user, @Param(value = "pass") String pass,
			@Param(value = "idioma") String idioma, @Param(value = "idApp") Integer idApp);
	
	public void commerceActivate(@Param(value = "idcommerce") long idcommerce);

	public String cambiarPasswordUsuario(TUsuarios usuario);

	public String resetPasswordCommerce(@Param(value = "idApp") int idApp,@Param(value = "id") long id,@Param(value = "pass") String pass, @Param(value = "idioma") String idioma);
	public String resetPasswordUsuario(TUsuarios usuario);

	public String updateUserInfo(@Param(value = "idioma") String idioma, @Param(value = "name") String name,
			@Param(value = "value") String value, @Param(value = "idUsuario") long idUsuario, @Param(value = "login") String login, @Param(value = "idApp") Integer idApp);

	public TUsuarios getUserInfo(@Param(value = "idUsuario") long idUsuario, @Param(value = "idApp") Integer idApp);

	public String processTags(ProcessTagsVO processTags);

	public String getTerminos(@Param(value = "idPais") int idPais, @Param(value = "idioma") String idioma,
			@Param(value = "idApp") Integer idApp);

	public TemplateCorreo getTemplateMail(@Param(value = "id") String nombre,
			@Param(value = "idioma") String idioma, @Param(value = "idApp") Integer idApp);

	public TUsuarios getUserInfoByLogin(@Param(value = "login") String login, @Param(value = "idApp") Integer idApp);
	
	public String getPrivacidad(@Param(value = "idPais") int idPais, @Param(value = "idioma") String idioma, @Param(value = "tipo") int tipo);
	
	public int searchEstab(@Param(value = "usuario") String usuario, @Param(value = "idApp") int idApp);
	
	public long getMaxIdBitacora();
	
	public Bank getBanco(@Param(value = "id_banco") long id_banco);
	
	public void insertEstablecimiento(@Param(value = "estab") Establecimiento estab, @Param(value = "img_ine") byte[] img_ine, @Param(value = "img_dom") byte[] img_dom, @Param(value = "cuenta_fav") int cuenta_fav);
	
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
	
	public long getAccountId(@Param(value = "id_transaction") long id_transaction);

	public void updateEstab(Establecimiento estab);
	
	public Establecimiento getEstablecimiento( @Param(value = "id_est") long id_est);
	
	public void AdminPassUpdate(@Param(value = "idAdmin")long idAdmin, @Param(value = "NewPass")String NewPass, @Param(value = "status")int status);
	
	public Establecimiento getEstablecimientoByUser(@Param(value = "admin") String admin,@Param(value = "idApp") int idApp);
	
	public List<BankCodes> getBankcodes();

	public int validaTarjetaPrevivale(@Param(value = "tarjeta") String t_previvale);

	public void updateUserSMSCode(@Param(value = "idUsuario") Long idUsuario, @Param(value = "code")String formatted);

	public void updateStatusSMS(@Param(value = "idUsuario") long idUsuario, @Param(value = "sms") String sms);

	public void updateConfirmEmail(@Param(value = "login") String login,@Param(value = "pass") String pass, @Param(value = "codigo") String codigo, @Param(value = "idApp") int idApp);
	
	public void updateCommerceSmsCode(@Param(value = "id") long id, @Param(value = "code") String formatted);

	public void updateStatusCommerce(@Param(value = "id")long id, @Param(value = "estatus") int estatus);

	public void updateStatusLogin(@Param(value = "login") String email, @Param(value = "estatus") int status);
	
	public CardCommerce getCardCommerce(@Param(value = "id") long id,@Param(value = "id_aplicacion") long id_aplicacion);
	
	public AppCipher getAppCipher(@Param(value = "id") Integer id);

	public String getCuentaClabeMobilecard(@Param(value = "id") long id);
	
	public Bank getBancoBillPocket(@Param(value = "idBanco") long idBanco);
	
	public List<BankCodes> getBankcodesBillPocket();
	
    public void addUserPhoto(@Param(value = "idUser") long idUser,@Param(value = "idApp") int idApp, @Param(value = "idPais")  int idPais, @Param(value = "photo")  byte[] photo);
	
    public void updateUserPhoto(@Param(value = "idUser") long idUser,@Param(value = "idApp") int idApp, @Param(value = "idPais") int  idPais, @Param(value = "photo")  byte[] photo);
    
    public ImgProfile getUserPhoto(@Param(value = "idUser") long idUser);
    
    public String isAppActive(@Param(value = "idApp") Integer idApp);

	public List<BankCodes> getBankcodesPeru();

	public Bank getBancoPeru(@Param(value = "idBanco") long idBanco);

	public void updateCommerceReference(@Param(value = "login")String login, 
			@Param(value = "scanReference") String scanReference);
	
	public String preRegistro(@Param(value = "idUser") long idUser,@Param(value = "idApp") int idApp, @Param(value = "idPais")  int idPais
			,@Param(value = "tipoUsuario") String tipoUsuario,@Param(value = "idioma") String idioma, @Param(value = "proceso") int proceso  
			,@Param(value = "eMail") String eMail,@Param(value = "imei") String imei,@Param(value = "telefono") String telefono);
	
	public void updateCommerceQR(@Param(value = "id") long id,@Param(value = "qrTag") String qrTag,@Param(value = "qrBase64") String qrBase64);
	
	public int iswhitelistuser(@Param(value = "idUsuario") long idUsuario);
	
	public void DesactivatecardPromoAct(@Param(value = "idUsuario") long idUsuario,@Param(value = "idApp") int idApp);
	public void DesactivatecardPromoDes(@Param(value = "idUsuario") long idUsuario,@Param(value = "idApp") int idApp);
	public int hasPrevivale(@Param(value = "idUsuario") long idUsuario);
	public void updateUserCountry(@Param(value = "idUsuario") long idUsuario,@Param(value = "idPais") int idPais);
	public Integer hasStaticQr(@Param(value = "idcomercio") long idcomercio,@Param(value = "idPais") int idPais, @Param(value = "idApp") int idApp);
	public Integer existsCommerce(@Param(value="login") String login, @Param(value="tel") String tel,@Param(value = "idPais") int idPais, @Param(value = "idApp") int idApp);
    public Integer isPeru();
}
