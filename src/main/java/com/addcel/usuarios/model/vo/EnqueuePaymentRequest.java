package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnqueuePaymentRequest {

	private String Token;
	private AccountRecord fromAccount;
	private AccountRecord toAccount;
	private double Amount;
	private String Description;
	private String ClientReference;
	
	public EnqueuePaymentRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return Token;
	}

	public void setToken(String token) {
		Token = token;
	}

	public AccountRecord getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(AccountRecord fromAccount) {
		this.fromAccount = fromAccount;
	}

	public AccountRecord getToAccount() {
		return toAccount;
	}

	public void setToAccount(AccountRecord toAccount) {
		this.toAccount = toAccount;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getClientReference() {
		return ClientReference;
	}

	public void setClientReference(String clientReference) {
		ClientReference = clientReference;
	}
	
	
}
