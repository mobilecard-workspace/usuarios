package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VerifyResponse  extends BaseResponse{
	
	 private String usr_telefono; 
	 private String usr_nombre;
	 private String usr_apellido;
	 private Integer id_usr_status;
	 private Integer idpais;
	 private String usr_email;
	 private String usr_sms;
	 private String usr_jumio;
	 private String scanReference;
	 
	 public VerifyResponse() {
		// TODO Auto-generated constructor stub
	}
	 	 
	public String getUsr_email() {
		return usr_email;
	}

	public void setUsr_email(String usr_email) {
		this.usr_email = usr_email;
	}

	public String getUsr_sms() {
		return usr_sms;
	}

	public void setUsr_sms(String usr_sms) {
		this.usr_sms = usr_sms;
	}

	public String getUsr_telefono() {
		return usr_telefono;
	}
	public void setUsr_telefono(String usr_telefono) {
		this.usr_telefono = usr_telefono;
	}
	public String getUsr_nombre() {
		return usr_nombre;
	}
	public void setUsr_nombre(String usr_nombre) {
		this.usr_nombre = usr_nombre;
	}
	public String getUsr_apellido() {
		return usr_apellido;
	}
	public void setUsr_apellido(String usr_apellido) {
		this.usr_apellido = usr_apellido;
	}
	public Integer getId_usr_status() {
		return id_usr_status;
	}
	public void setId_usr_status(Integer id_usr_status) {
		this.id_usr_status = id_usr_status;
	}
	public Integer getIdpais() {
		return idpais;
	}
	public void setIdpais(Integer idpais) {
		this.idpais = idpais;
	}

	public String getUsr_jumio() {
		return usr_jumio;
	}

	public void setUsr_jumio(String usr_jumio) {
		this.usr_jumio = usr_jumio;
	}

	public String getScanReference() {
		return scanReference;
	}

	public void setScanReference(String scanReference) {
		this.scanReference = scanReference;
	}
	 
	 

}
