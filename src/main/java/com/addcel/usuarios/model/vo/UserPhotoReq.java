package com.addcel.usuarios.model.vo;

public class UserPhotoReq {
	
	private long idUser;
	private String photo; 
  
	public UserPhotoReq() {
		// TODO Auto-generated constructor stub
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
	
}
