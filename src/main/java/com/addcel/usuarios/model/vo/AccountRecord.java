package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountRecord {

	private String holderName;
	private String rfc;
	private String phone;
	private String contact;
	private String email;
	private String actType;
	private String bankCode;
	private String actNumber;
	
	public AccountRecord() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @return holderName
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @holderName String con el nombre del titular de la cuenta. 
	 * Valor alfabético, sin caracteres acentuados obligatorio y no nulo. 
	 * La longitud máxima de este campo es de 60 caracteres.
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * 
	 * @return Return RFC
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * setear valor al RFC
	 * @param rfc
	 * String con el númro RFC del titular de la cuenta.
	 * Alfanumérico obligatorio y no nulo con una longitud máxima de 13 caracteres.
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * 
	 * @return PHONE
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * agrega valor al telefono
	 * @param phone
	 * String con el número de teléfono de contacto del titular de la cuenta.
	 * Valor numérico opcional. En caso de ser no nulo, debe ser un número de teléfono válido con solo dígitos, incluyendo el código de área.
	 * Este campo debe tener una longitud máxima de 15 caracteres
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * Retorna un String con el nombre de contacto
	 * @return contact
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * 
	 * @param contact 
	 * String con el nombre de persona de contacto, en caso de que el titular sea una persona moral.
	 * Valor alfabético, sin caracteres acentuados opcional.
	 * La longitud máxima de este campo es de 20 caracteres. 
	 * En caso de que sea nulo, se tomará como valor de contacto los primeros 20 caracteres del campo holderName.
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * String con el  email de contacto del titular de la cuenta. 
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 
	 * @param email
	 * String con el  email de contacto del titular de la cuenta. 
	 * Valor opcional. En caso de ser no nulo debe ser una dirección de email válida, con una longitud máxima de 39 caracteres (Esta limitación es por parte de la plataforma H2H Banorte). 
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 * @return actType
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * 
	 * @param actType
	 * String con el indicador del tipo de cuenta a proveer (cuenta o CLABE). Es obligatorio y no nulo, y sus valores permitidos son:<br/>
	 * <ul><li><b>ACT:</b>  El registro es de un número de cuenta de 10 dígitos (Aplica solo para el caso de cuentas Banorte)</li>
	 * <li><b>CLABE:</b> El registro es un numero Clabe de máximo 20 dígitos (Aplica solo para cuentas de otros bancos distintos a Banorte)</li></ul>
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}
	
	/**
	 * 
	 * @return bankCode
	 */
	public String getBankCode() {
		return bankCode;
	}

	/**
	 * 
	 * @param bankCode
	 * String con los dígitos del código CECOBAN del banco al que pertenece la cuenta.
	 * Valor obligatorio y no nulo
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	
	/**
	 * 
	 * @return actNumber
	 */
	public String getActNumber() {
		return actNumber;
	}

	/**
	 * 
	 * @param actNumber
	 * String con los dígitos del número de cuenta (Banorte) o CLABE (otros bancos)
	 * Valor obligatorio y no núlo.
	 * Debe constar únicamente de dígitos y con una longitud máxima de 20 caracteres
	 */
	public void setActNumber(String actNumber) {
		this.actNumber = actNumber;
	}
	
	
	
}
