package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse {

	 private int idError;
	 private String mensajeError;
	 
	 public BaseResponse(int idError,String mensajeError) {
		// TODO Auto-generated constructor stub
		 this.idError = idError;
		 this.mensajeError = mensajeError;
	}
	 
	 public BaseResponse() {
			// TODO Auto-generated constructor stub
		}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	 
	 
	
}
