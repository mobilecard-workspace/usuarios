package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnqueueAccountSignUpRequest {

	
	private String token;
	private AccountRecord account;
	private String clientReference;
	private String idAccount;
	
	public EnqueueAccountSignUpRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public AccountRecord getAccount() {
		return account;
	}

	public void setAccount(AccountRecord account) {
		this.account = account;
	}

	public String getClientReference() {
		return clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	public String getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(String idAccount) {
		this.idAccount = idAccount;
	}
	
	
}
