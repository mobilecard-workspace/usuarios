package com.addcel.usuarios.model.vo;

public class EstatusPrevivaleCard {

	private Integer code;
	
	private String message;
	
	private CardRequest data;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CardRequest getData() {
		return data;
	}

	public void setData(CardRequest data) {
		this.data = data;
	}

}
