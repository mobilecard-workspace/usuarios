package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VerifyResponseCommerce  extends BaseResponse{
	
	private long id;
	
	private int estatus;
	
	private String email_contacto;
	
	private String telefono_contacto;
	
	private String nombre;
	
	private String paterno;
	
	private String materno;
	
	private String accountId;
	
	private MobileCardCard card;
	
	private String jumioStatus;
	
	private String jumioReference;
	
	private int idPais;
	 
	public VerifyResponseCommerce() {
	
	}
	
	public void setCard(MobileCardCard card) {
		this.card = card;
	}
	
	public MobileCardCard getCard() {
		return card;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public String getEmail_contacto() {
		return email_contacto;
	}

	public void setEmail_contacto(String email_contacto) {
		this.email_contacto = email_contacto;
	}

	public String getTelefono_contacto() {
		return telefono_contacto;
	}

	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}
	
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getJumioReference() {
		return jumioReference;
	}

	public void setJumioReference(String jumioReference) {
		this.jumioReference = jumioReference;
	}

	public String getJumioStatus() {
		return jumioStatus;
	}

	public void setJumioStatus(String jumioStatus) {
		this.jumioStatus = jumioStatus;
	}

	public int getIdPais() {
		return idPais;
	}

	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}
}