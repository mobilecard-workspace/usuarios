package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LogoutResponse {

	private ServiceResult result;
	
	public LogoutResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public void setResult(ServiceResult result) {
		this.result = result;
	}
	
	public ServiceResult getResult() {
		return result;
	}
	
}
