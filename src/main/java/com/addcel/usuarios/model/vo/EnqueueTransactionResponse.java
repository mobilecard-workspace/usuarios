package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnqueueTransactionResponse {

	private ServiceResult result;
	private String clientReference;
	private String transactionReference;
	
	public EnqueueTransactionResponse() {
		// TODO Auto-generated constructor stub
	}

	public ServiceResult getResult() {
		return result;
	}

	public void setResult(ServiceResult result) {
		this.result = result;
	}

	public String getClientReference() {
		return clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}
	
	
	
}
