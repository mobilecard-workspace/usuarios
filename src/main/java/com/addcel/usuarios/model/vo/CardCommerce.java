package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CardCommerce {
	
	@SerializedName("id_tarjetas_establecimiento")
	@Expose
	private long id_tarjeta;
	private Integer id_aplicacion;
	private long id_establecimiento;
	private String numero_tarjeta;
	private String vigencia;
	private String ct;
	private Integer estatus;
	private Integer idtarjetas_tipo;
	private Integer idfranquicia;
	private String previvale;
	private String nombre_tarjeta;
	
	public CardCommerce() {
		// TODO Auto-generated constructor stub
	}

	public long getId_tarjeta() {
		return id_tarjeta;
	}

	public void setId_tarjeta(long id_tarjeta) {
		this.id_tarjeta = id_tarjeta;
	}

	public Integer getId_aplicacion() {
		return id_aplicacion;
	}

	public void setId_aplicacion(Integer id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}

	public long getId_establecimiento() {
		return id_establecimiento;
	}

	public void setId_establecimiento(long id_establecimiento) {
		this.id_establecimiento = id_establecimiento;
	}

	public String getNumero_tarjeta() {
		return numero_tarjeta;
	}

	public void setNumero_tarjeta(String numero_tarjeta) {
		this.numero_tarjeta = numero_tarjeta;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String ct) {
		this.ct = ct;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Integer getIdtarjetas_tipo() {
		return idtarjetas_tipo;
	}

	public void setIdtarjetas_tipo(Integer idtarjetas_tipo) {
		this.idtarjetas_tipo = idtarjetas_tipo;
	}

	public Integer getIdfranquicia() {
		return idfranquicia;
	}

	public void setIdfranquicia(Integer idfranquicia) {
		this.idfranquicia = idfranquicia;
	}

	public String getPrevivale() {
		return previvale;
	}

	public void setPrevivale(String previvale) {
		this.previvale = previvale;
	}

	public String getNombre_tarjeta() {
		return nombre_tarjeta;
	}

	public void setNombre_tarjeta(String nombre_tarjeta) {
		this.nombre_tarjeta = nombre_tarjeta;
	}
	
	

}
