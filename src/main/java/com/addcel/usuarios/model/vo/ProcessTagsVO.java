package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessTagsVO {
	
	private long idUsuario;
	private String tag;
	private int provider;
	private String etiquetatag;
	private int dv;
	private String idioma;
	private int proceso;
	
	private int idApp;
	
	public ProcessTagsVO() {
		// TODO Auto-generated constructor stub
	}
	
	public ProcessTagsVO(int idApp, long idUsuario,String tag,int provider,String etiquetatag, int dv,String idioma,int proceso) {
		// TODO Auto-generated constructor stub
		this.dv = dv;
		this.etiquetatag = etiquetatag;
		this.idioma = idioma;
		this.idUsuario = idUsuario;
		this.proceso = proceso;
		this.provider = provider;
		this.tag = tag;
		this.idApp = idApp;
	}
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public int getProvider() {
		return provider;
	}
	public void setProvider(int provider) {
		this.provider = provider;
	}
	public String getEtiquetatag() {
		return etiquetatag;
	}
	public void setEtiquetatag(String etiquetatag) {
		this.etiquetatag = etiquetatag;
	}
	public int getDv() {
		return dv;
	}
	public void setDv(int dv) {
		this.dv = dv;
	}
	public String getIdioma() {
		return idioma;
	}
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	public int getProceso() {
		return proceso;
	}
	public void setProceso(int proceso) {
		this.proceso = proceso;
	}

	public int getIdApp() {
		return idApp;
	}

	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}
	
	

}
