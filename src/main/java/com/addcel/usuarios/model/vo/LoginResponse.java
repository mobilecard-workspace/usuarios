package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {
	
	private ServiceResult result;
	private String token;
	
	public LoginResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public void setResult(ServiceResult result) {
		this.result = result;
	}
	
	public ServiceResult getResult() {
		return result;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}
}
