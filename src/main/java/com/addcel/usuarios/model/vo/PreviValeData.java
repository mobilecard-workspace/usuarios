package com.addcel.usuarios.model.vo;

public class PreviValeData {

	private String authorization;
	private Integer previvaleCode;
	private Long idPeticion;
	private Long idPrevivaleCardStock;
	
	public PreviValeData() {
		// TODO Auto-generated constructor stub
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public Integer getPrevivaleCode() {
		return previvaleCode;
	}

	public void setPrevivaleCode(Integer previvaleCode) {
		this.previvaleCode = previvaleCode;
	}

	public Long getIdPeticion() {
		return idPeticion;
	}

	public void setIdPeticion(Long idPeticion) {
		this.idPeticion = idPeticion;
	}
	
	public void setIdPrevivaleCardStock(Long idPrevivaleCardStock) {
		this.idPrevivaleCardStock = idPrevivaleCardStock;
	}
	
	public Long getIdPrevivaleCardStock() {
		return idPrevivaleCardStock;
	}
	
	
}
