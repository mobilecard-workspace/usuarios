package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BankCodes {
	
	private long id;
	private String nombre_razon_social;
	private String nombre_corto;

	
	public BankCodes() {
		// TODO Auto-generated constructor stub
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNombre_razon_social() {
		return nombre_razon_social;
	}


	public void setNombre_razon_social(String nombre_razon_social) {
		this.nombre_razon_social = nombre_razon_social;
	}


	public String getNombre_corto() {
		return nombre_corto;
	}


	public void setNombre_corto(String nombre_corto) {
		this.nombre_corto = nombre_corto;
	}
	
	

	

}
