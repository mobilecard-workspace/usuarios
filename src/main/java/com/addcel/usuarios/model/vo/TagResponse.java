package com.addcel.usuarios.model.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TagResponse extends BaseResponse{
	
	  List<Tag> tags;
	  
	  public TagResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	  
	  
	
}
