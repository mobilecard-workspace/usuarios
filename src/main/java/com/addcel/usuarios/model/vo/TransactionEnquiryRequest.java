package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionEnquiryRequest {

	private String token;
	private String transactionReference;
	
	public TransactionEnquiryRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}
	
	public String getTransactionReference() {
		return transactionReference;
	}
	
}
