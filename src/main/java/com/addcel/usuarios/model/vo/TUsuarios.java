package com.addcel.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TUsuarios extends AbstractVO {
    
    private Long ideUsuario;

    private String usrLogin;

    private String usrPwd;

    private String usrFechaNac;

    private String usrTelefono;
    
    private int operador;

    private String usrFechaRegistro;

    private String usrNombre;

    private String usrApellido;

    private String usrDireccion;

    private String usrTdcNumero;
    
    private String usrTdcCodigo;

	private String usrTdcVigencia;

    private Integer idBanco;

    private Integer idTipoTarjeta;

    private Integer idProveedor;

    private Integer idUsrStatus = 0;

    private String cedula;

    private Integer tipoCedula;

    private Integer recibirSMS;

    private Integer idPais;

    private Integer gemalto;

    private String eMail;

    private String imei;

    private String tipo;

    private String software;

    private String modelo;

    private String wkey;

    private String telefonoOriginal;

    private String usrMaterno;

    private String usrSexo;

    private String usrTelCasa;

    private String usrTelOficina;

    private Integer usrIdEstado;

    private String usrCiudad;

    private String usrCalle;

    private Integer usrNumExt;

    private String usrNumInterior;

    private String usrColonia;

    private String usrCp;

    private String usrDomAmex;

    private String usrTerminos;

    private String numExtStr;

    private String idIngo;
    
    private String usrNss;
    
    private String newPassword;
    
    private String idioma;
    
    private boolean debug;
    
    private String proveedor;
    
    private Integer idApp;
    
    private String tipoUsuario;
	   
    private long id;
    
    private boolean negocio;
    
    private String smsCode;
    
    private String usr_sms;
    
    private String usr_email;
    
    @SerializedName("photo")
    private String img;
    
    private String usr_jumio;
    
    private String scanReference;
    
    private String usrNacionalidad;
    
    private String usrDistrito;
    
    private String usrProvincia;
    
    private String usrDepartamento;
    
    private String usrCentroLaboral;
    
    private String usrOcupacion;
    
    private String usrCargoPublico;
    
    private String usrInstitucion;
    
    public void setUsr_jumio(String usr_jumio) {
		this.usr_jumio = usr_jumio;
	}
    
    public String getUsr_jumio() {
		return usr_jumio;
	}
    
    public void setImg(String img) {
		this.img = img;
	}
    
    public String getImg() {
		return img;
	}
    
    public void setUsr_email(String usr_email) {
		this.usr_email = usr_email;
	}
    
    public String getUsr_email() {
		return usr_email;
	}
    
    public void setUsr_sms(String usr_sms) {
		this.usr_sms = usr_sms;
	}
    
    public String getUsr_sms() {
		return usr_sms;
	}

    public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public void setIdeUsuario(Long ideUsuario) {
		this.ideUsuario = ideUsuario;
	}

	public String getUsrTdcCodigo() {
		return usrTdcCodigo;
	}

	public void setUsrTdcCodigo(String usrTdcCodigo) {
		this.usrTdcCodigo = usrTdcCodigo;
	}
    
    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public Long getIdeUsuario() {
        return ideUsuario;
    }

    public void setIdUsuario(Long ideUsuario) {
        this.ideUsuario = ideUsuario;
    }

    public String getUsrLogin() {
        return usrLogin;
    }

    public void setUsrLogin(String usrLogin) {
        this.usrLogin = usrLogin;
    }

    public String getUsrPwd() {
        return usrPwd;
    }

    public void setUsrPwd(String usrPwd) {
        this.usrPwd = usrPwd;
    }

    public String getUsrFechaNac() {
        return usrFechaNac;
    }

    public void setUsrFechaNac(String usrFechaNac) {
        this.usrFechaNac = usrFechaNac;
    }

    public String getUsrTelefono() {
        return usrTelefono;
    }

    public int getOperador() {
        return operador;
    }

    public void setOperador(int operador) {
        this.operador = operador;
    }
    
    public void setUsrTelefono(String usrTelefono) {
        this.usrTelefono = usrTelefono;
    }

    public String getUsrFechaRegistro() {
        return usrFechaRegistro;
    }

    public void setUsrFechaRegistro(String usrFechaRegistro) {
        this.usrFechaRegistro = usrFechaRegistro;
    }

    public String getUsrNombre() {
        return usrNombre;
    }

    public void setUsrNombre(String usrNombre) {
        this.usrNombre = usrNombre;
    }

    public String getUsrApellido() {
        return usrApellido;
    }

    public void setUsrApellido(String usrApellido) {
        this.usrApellido = usrApellido;
    }

    public String getUsrDireccion() {
        return usrDireccion;
    }

    public void setUsrDireccion(String usrDireccion) {
        this.usrDireccion = usrDireccion;
    }

    public String getUsrTdcNumero() {
        return usrTdcNumero;
    }

    public void setUsrTdcNumero(String usrTdcNumero) {
        this.usrTdcNumero = usrTdcNumero;
    }

    public String getUsrTdcVigencia() {
        return usrTdcVigencia;
    }

    public void setUsrTdcVigencia(String usrTdcVigencia) {
        this.usrTdcVigencia = usrTdcVigencia;
    }

    public Integer getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Integer idBanco) {
        this.idBanco = idBanco;
    }

    public Integer getIdTipoTarjeta() {
        return idTipoTarjeta;
    }

    public void setIdTipoTarjeta(Integer idTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Integer getIdUsrStatus() {
        return idUsrStatus;
    }

    public void setIdUsrStatus(Integer idUsrStatus) {
        this.idUsrStatus = idUsrStatus;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Integer getTipoCedula() {
        return tipoCedula;
    }

    public void setTipoCedula(Integer tipoCedula) {
        this.tipoCedula = tipoCedula;
    }

    public Integer getRecibirSMS() {
        return recibirSMS;
    }

    public void setRecibirSMS(Integer recibirSMS) {
        this.recibirSMS = recibirSMS;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public Integer getGemalto() {
        return gemalto;
    }

    public void setGemalto(Integer gemalto) {
        this.gemalto = gemalto;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getWkey() {
        return wkey;
    }

    public void setWkey(String wkey) {
        this.wkey = wkey;
    }

    public String getTelefonoOriginal() {
        return telefonoOriginal;
    }

    public void setTelefonoOriginal(String telefonoOriginal) {
        this.telefonoOriginal = telefonoOriginal;
    }

    public String getUsrMaterno() {
        return usrMaterno;
    }

    public void setUsrMaterno(String usrMaterno) {
        this.usrMaterno = usrMaterno;
    }

    public String getUsrSexo() {
        return usrSexo;
    }

    public void setUsrSexo(String usrSexo) {
        this.usrSexo = usrSexo;
    }

    public String getUsrTelCasa() {
        return usrTelCasa;
    }

    public void setUsrTelCasa(String usrTelCasa) {
        this.usrTelCasa = usrTelCasa;
    }

    public String getUsrTelOficina() {
        return usrTelOficina;
    }

    public void setUsrTelOficina(String usrTelOficina) {
        this.usrTelOficina = usrTelOficina;
    }

    public Integer getUsrIdEstado() {
        return usrIdEstado;
    }

    public void setUsrIdEstado(Integer usrIdEstado) {
        this.usrIdEstado = usrIdEstado;
    }

    public String getUsrCiudad() {
        return usrCiudad;
    }

    public void setUsrCiudad(String usrCiudad) {
        this.usrCiudad = usrCiudad;
    }

    public String getUsrCalle() {
        return usrCalle;
    }

    public void setUsrCalle(String usrCalle) {
        this.usrCalle = usrCalle;
    }

    public Integer getUsrNumExt() {
        return usrNumExt;
    }

    public void setUsrNumExt(Integer usrNumExt) {
        this.usrNumExt = usrNumExt;
    }

    public String getUsrNumInterior() {
        return usrNumInterior;
    }

    public void setUsrNumInterior(String usrNumInterior) {
        this.usrNumInterior = usrNumInterior;
    }

    public String getUsrColonia() {
        return usrColonia;
    }

    public void setUsrColonia(String usrColonia) {
        this.usrColonia = usrColonia;
    }

    public String getUsrCp() {
        return usrCp;
    }

    public void setUsrCp(String usrCp) {
        this.usrCp = usrCp;
    }

    public String getUsrDomAmex() {
        return usrDomAmex;
    }

    public void setUsrDomAmex(String usrDomAmex) {
        this.usrDomAmex = usrDomAmex;
    }

    public String getUsrTerminos() {
        return usrTerminos;
    }

    public void setUsrTerminos(String usrTerminos) {
        this.usrTerminos = usrTerminos;
    }

    public String getNumExtStr() {
        return numExtStr;
    }

    public void setNumExtStr(String numExtStr) {
        this.numExtStr = numExtStr;
    }

    public String getIdIngo() {
        return idIngo;
    }

    public void setIdIngo(String idIngo) {
        this.idIngo = idIngo;
    }

    public String getUsrNss() {
        return usrNss;
    }

    public void setUsrNss(String usrNss) {
        this.usrNss = usrNss;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public Integer getIdApp() {
		return idApp;
	}

	public void setIdApp(Integer idApp) {
		this.idApp = idApp;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isNegocio() {
		return negocio;
	}

	public void setNegocio(boolean negocio) {
		this.negocio = negocio;
	}

	public String getSmsCode() {
		return smsCode;
	}

	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}

	public String getScanReference() {
		return scanReference;
	}

	public void setScanReference(String scanReference) {
		this.scanReference = scanReference;
	}

	public String getUsrNacionalidad() {
		return usrNacionalidad;
	}

	public void setUsrNacionalidad(String usrNacionalidad) {
		this.usrNacionalidad = usrNacionalidad;
	}

	public String getUsrDistrito() {
		return usrDistrito;
	}

	public void setUsrDistrito(String usrDistrito) {
		this.usrDistrito = usrDistrito;
	}

	public String getUsrProvincia() {
		return usrProvincia;
	}

	public void setUsrProvincia(String usrProvincia) {
		this.usrProvincia = usrProvincia;
	}

	public String getUsrDepartamento() {
		return usrDepartamento;
	}

	public void setUsrDepartamento(String usrDepartamento) {
		this.usrDepartamento = usrDepartamento;
	}

	public String getUsrCentroLaboral() {
		return usrCentroLaboral;
	}

	public void setUsrCentroLaboral(String usrCentroLaboral) {
		this.usrCentroLaboral = usrCentroLaboral;
	}

	public String getUsrOcupacion() {
		return usrOcupacion;
	}

	public void setUsrOcupacion(String usrOcupacion) {
		this.usrOcupacion = usrOcupacion;
	}

	public String getUsrCargoPublico() {
		return usrCargoPublico;
	}

	public void setUsrCargoPublico(String usrCargoPublico) {
		this.usrCargoPublico = usrCargoPublico;
	}

	public String getUsrInstitucion() {
		return usrInstitucion;
	}

	public void setUsrInstitucion(String usrInstitucion) {
		this.usrInstitucion = usrInstitucion;
	}
	
}