package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Establecimiento extends AbstractVO{
	
	private long id;
	private String usuario;
	private String pass;
	private String nombre_establecimiento;
	private String rfc;
	private String razon_social;
	private String representante_legal;
	private String direccion_establecimiento;
	private String email_contacto;
	private String telefono_contacto;
	private String id_banco;
	private String cuenta_clabe;
	private String nombre_prop_cuenta;
	private long id_account;
	private int estatus;
	private double comision_fija; 
	private double comision_porcentaje;
	private String urlLogo;
	private String cuenta_tipo;
	private String codigo_banco;
	private int id_aplicacion;
	private boolean negocio;
	private String img_ine;
	private String img_domicilio;
	
	private String t_previvale;
	
	private String nombre;
	
	private String paterno;
	
	private String materno;
	
	private String colonia;
	
	private String ciudad;
	
	private String cp;
	
	private int id_estado;
	
	private String curp;
	
	private String smsCode;
	
	private int idPais;
	
	private String tipoDocumento;
	
	private String numeroDocumento;
	
	private String fechaNacimiento;
	
	private String nacionalidad;
	
	private String distrito;
	
	private String provincia;
	
	private String departamento;
	
	private String centroLaboral;
	
	private String ocupacion;
	
	private String cargo;
	
	private String institucion;
	
	private String cci;
	
	private String jumio_status;
	
	private String jumio_reference;
	
	private String digitoVerificador;
	
	private String direccion;
	
	private String qrTag;
	
	private String qrBase64;
	
	private String tipo_persona;
	
	private String digito_verificador;
	
	private String usr_institucion;
	
	public Establecimiento() {
		
	}
	
	public String getQrTag() {
		return qrTag;
	}

	public void setUsr_institucion(String usr_institucion) {
		this.usr_institucion = usr_institucion;
	}
	
	public String getUsr_institucion() {
		return usr_institucion;
	}
	
	public void setDigito_verificador(String digito_verificador) {
		this.digito_verificador = digito_verificador;
	}
	
	public String getDigito_verificador() {
		return digito_verificador;
	}

	public void setQrTag(String qrTag) {
		this.qrTag = qrTag;
	}



	public String getQrBase64() {
		return qrBase64;
	}



	public void setQrBase64(String qrBase64) {
		this.qrBase64 = qrBase64;
	}



	public String getTipo_persona() {
		return tipo_persona;
	}



	public void setTipo_persona(String tipo_persona) {
		this.tipo_persona = tipo_persona;
	}



	public String getImg_ine() {
		return img_ine;
	}



	public void setImg_ine(String img_ine) {
		this.img_ine = img_ine;
	}


	public String getImg_domicilio() {
		return img_domicilio;
	}



	public void setImg_domicilio(String img_domicilio) {
		this.img_domicilio = img_domicilio;
	}



	public boolean isNegocio() {
		return negocio;
	}



	public void setNegocio(boolean negocio) {
		this.negocio = negocio;
	}



	public void setId_aplicacion(int id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}
	
	public int getId_aplicacion() {
		return id_aplicacion;
	}
	
	
	public String getCuenta_tipo() {
		return cuenta_tipo;
	}



	public void setCuenta_tipo(String cuenta_tipo) {
		this.cuenta_tipo = cuenta_tipo;
	}



	public String getCodigo_banco() {
		return codigo_banco;
	}



	public void setCodigo_banco(String codigo_banco) {
		this.codigo_banco = codigo_banco;
	}



	public void setUrlLogo(String urlLogo) {
		this.urlLogo = urlLogo;
	}
	
	public String getUrlLogo() {
		return urlLogo;
	}

	public double getComision_fija() {
		return comision_fija;
	}



	public void setComision_fija(double comision_fija) {
		this.comision_fija = comision_fija;
	}



	public double getComision_porcentaje() {
		return comision_porcentaje;
	}



	public void setComision_porcentaje(double comision_porcentaje) {
		this.comision_porcentaje = comision_porcentaje;
	}



	public void setId_account(long id_account) {
		this.id_account = id_account;
	}
	
	public long getId_account() {
		return id_account;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getNombre_establecimiento() {
		return nombre_establecimiento;
	}

	public void setNombre_establecimiento(String nombre_establecimiento) {
		this.nombre_establecimiento = nombre_establecimiento;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public String getRepresentante_legal() {
		return representante_legal;
	}

	public void setRepresentante_legal(String representante_legal) {
		this.representante_legal = representante_legal;
	}

	public String getDireccion_establecimiento() {
		return direccion_establecimiento;
	}

	public void setDireccion_establecimiento(String direccion_establecimiento) {
		this.direccion_establecimiento = direccion_establecimiento;
	}

	public String getEmail_contacto() {
		return email_contacto;
	}

	public void setEmail_contacto(String email_contacto) {
		this.email_contacto = email_contacto;
	}

	public String getTelefono_contacto() {
		return telefono_contacto;
	}

	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}

	public String getId_banco() {
		return id_banco;
	}

	public void setId_banco(String id_banco) {
		this.id_banco = id_banco;
	}

	public String getCuenta_clabe() {
		return cuenta_clabe;
	}

	public void setCuenta_clabe(String cuenta_clabe) {
		this.cuenta_clabe = cuenta_clabe;
	}

	public String getNombre_prop_cuenta() {
		return nombre_prop_cuenta;
	}

	public void setNombre_prop_cuenta(String nombre_prop_cuenta) {
		this.nombre_prop_cuenta = nombre_prop_cuenta;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}



	public String getT_previvale() {
		return t_previvale;
	}



	public void setT_previvale(String t_previvale) {
		this.t_previvale = t_previvale;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getPaterno() {
		return paterno;
	}



	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}



	public String getMaterno() {
		return materno;
	}



	public void setMaterno(String materno) {
		this.materno = materno;
	}



	public String getColonia() {
		return colonia;
	}



	public void setColonia(String colonia) {
		this.colonia = colonia;
	}



	public String getCiudad() {
		return ciudad;
	}



	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}



	public String getCp() {
		return cp;
	}



	public void setCp(String cp) {
		this.cp = cp;
	}





	public String getCurp() {
		return curp;
	}



	public void setCurp(String curp) {
		this.curp = curp;
	}



	public int getId_estado() {
		return id_estado;
	}



	public void setId_estado(int id_estado) {
		this.id_estado = id_estado;
	}



	public String getSmsCode() {
		return smsCode;
	}



	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}



	public String getTipoDocumento() {
		return tipoDocumento;
	}



	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}



	public String getNumeroDocumento() {
		return numeroDocumento;
	}



	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}



	public String getFechaNacimiento() {
		return fechaNacimiento;
	}



	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}



	public String getNacionalidad() {
		return nacionalidad;
	}



	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}



	public String getDistrito() {
		return distrito;
	}



	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}



	public String getProvincia() {
		return provincia;
	}



	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}



	public String getDepartamento() {
		return departamento;
	}



	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}



	public String getCentroLaboral() {
		return centroLaboral;
	}



	public void setCentroLaboral(String centroLaboral) {
		this.centroLaboral = centroLaboral;
	}



	public String getOcupacion() {
		return ocupacion;
	}



	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}



	public String getCargo() {
		return cargo;
	}



	public void setCargo(String cargo) {
		this.cargo = cargo;
	}



	public String getInstitucion() {
		return institucion;
	}



	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}



	public int getIdPais() {
		return idPais;
	}



	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}



	public String getCci() {
		return cci;
	}



	public void setCci(String cci) {
		this.cci = cci;
	}

	public String getJumio_status() {
		return jumio_status;
	}

	public void setJumio_status(String jumio_status) {
		this.jumio_status = jumio_status;
	}

	public String getJumio_reference() {
		return jumio_reference;
	}

	public void setJumio_reference(String jumio_reference) {
		this.jumio_reference = jumio_reference;
	}

	public String getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	

}
