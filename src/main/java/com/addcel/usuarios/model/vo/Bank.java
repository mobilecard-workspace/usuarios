package com.addcel.usuarios.model.vo;

public class Bank {
	
	private long id;
	private String clave;
	private String nombre_corto;
	private String nombre_razon_social;
	private double comision_fija;
	private double comision_porcentaje;
	
	public Bank() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre_corto() {
		return nombre_corto;
	}

	public void setNombre_corto(String nombre_corto) {
		this.nombre_corto = nombre_corto;
	}

	public String getNombre_razon_social() {
		return nombre_razon_social;
	}

	public void setNombre_razon_social(String nombre_razon_social) {
		this.nombre_razon_social = nombre_razon_social;
	}

	public double getComision_fija() {
		return comision_fija;
	}

	public void setComision_fija(double comision_fija) {
		this.comision_fija = comision_fija;
	}

	public double getComision_porcentaje() {
		return comision_porcentaje;
	}

	public void setComision_porcentaje(double comision_porcentaje) {
		this.comision_porcentaje = comision_porcentaje;
	}
	

}
