package com.addcel.usuarios.model.vo;

public class AppCipher {
	
	private Integer id;
	private Integer id_application;
	private String name;
	private String api_key;
	private Integer status;
	
	public AppCipher() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId_application() {
		return id_application;
	}

	public void setId_application(Integer id_application) {
		this.id_application = id_application;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApi_key() {
		return api_key;
	}

	public void setApi_key(String api_key) {
		this.api_key = api_key;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
