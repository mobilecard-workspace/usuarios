package com.addcel.usuarios.model.vo;

public class PreRegistro extends BaseResponse{

	private long id;
	private int status;
	
	public PreRegistro() {
		// TODO Auto-generated constructor stub
	}
	
	public PreRegistro(BaseResponse b) {
		// TODO Auto-generated constructor stub
		super(b.getIdError(), b.getMensajeError());
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
}
