package com.addcel.usuarios.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tag {
	
	 private int tiporecargatag;
     private String etiqueta;
     private String tag;
     private int dv;
     
     public Tag() {
		// TODO Auto-generated constructor stub
	}

	public int getTiporecargatag() {
		return tiporecargatag;
	}

	public void setTiporecargatag(int tiporecargatag) {
		this.tiporecargatag = tiporecargatag;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getDv() {
		return dv;
	}

	public void setDv(int dv) {
		this.dv = dv;
	}
     
     

}
