package com.addcel.usuarios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;



@JsonIgnoreProperties(ignoreUnknown=true)
public class CardRequest {
	
	private boolean	determinada;
	@JsonProperty("isMobilecard")
	private boolean isMobilecard;
	private int	idTarjeta; 
	private long	idUsuario; 
	private String	pan; 
	private int	tipo; 
	private String	vigencia;
	private String codigo;
	private String nombre;
	private String domAmex;
	private String cpAmex;
	private String tipoTarjeta;
	private double balance;
	
	private String representanteLegal;
		
	public boolean isDeterminada() {
		return determinada;
	}
	public void setDeterminada(boolean determinada) {
		this.determinada = determinada;
	}
	public boolean isMobilecard() {
		return isMobilecard;
	}
	public void setMobilecard(boolean isMobilecard) {
		this.isMobilecard = isMobilecard;
	}
	public int getIdTarjeta() {
		return idTarjeta;
	}
	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDomAmex() {
		return domAmex;
	}
	public void setDomAmex(String domAmex) {
		this.domAmex = domAmex;
	}
	public String getCpAmex() {
		return cpAmex;
	}
	public void setCpAmex(String cpAmex) {
		this.cpAmex = cpAmex;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getRepresentanteLegal() {
		return representanteLegal;
	}
	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	
}