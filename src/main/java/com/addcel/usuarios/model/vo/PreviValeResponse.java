package com.addcel.usuarios.model.vo;

public class PreviValeResponse {
	
	private int code;
	
	private String message;
	
	private PreviValeData data;
	
	public PreviValeResponse() {
		// TODO Auto-generated constructor stub
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PreviValeData getData() {
		return data;
	}

	public void setData(PreviValeData data) {
		this.data = data;
	}
	
	

}
