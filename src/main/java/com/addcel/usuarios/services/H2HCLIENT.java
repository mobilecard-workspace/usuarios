package com.addcel.usuarios.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.addcel.billplease.client.model.EnqueueAccountSignUpRequest;
import com.addcel.billplease.client.model.EnqueuePaymentRequest;
import com.addcel.billplease.client.model.EnqueueTransactionResponse;
import com.addcel.billplease.client.model.LoginRequest;
import com.addcel.billplease.client.model.LoginResponse;
import com.addcel.billplease.client.model.LogoutRequest;
import com.addcel.billplease.client.model.LogoutResponse;
import com.addcel.billplease.client.model.TransactionEnquiryRequest;
import com.addcel.billplease.client.model.TransactionEnquiryResponse;
import com.google.gson.Gson;



@Service
public class H2HCLIENT {

	private static final Logger LOGGER = LoggerFactory.getLogger(H2HCLIENT.class);
	private LoginResponse login;
	//private static final String SERVER = "http://192.168.75.53:80/"; //QA
	private static final String SERVER = "http://192.168.75.51:80/"; //prod
	private static final String URL_H2H_LOGIN = SERVER + "H2HPaymentService/v1/login?lan=@es";
	private static final String URL_H2H_LOGOUT = SERVER + "H2HPaymentService/v1/logout?lan=@es";
	private static final String URL_H2H_ENQUEUE_PAYMENT = SERVER + "H2HPaymentService/v1/enqueuePayment?lan=@es";
	private static final String URL_H2H_ENQUEUE_SING_UP = SERVER + "H2HPaymentService/v1/enqueueAccountSignUp?lan=@es";
	private static final String URL_H2H_TRANSACTION_ENQUIRY = SERVER + "H2HPaymentService/v1/transactionEnquiry?lan=@es";
	
	
	private Gson gson = new Gson();
	RestTemplate restTemplate = new RestTemplate();
	
	public LoginResponse login(LoginRequest login, String idioma){
		try{
			
			HttpHeaders headers = new HttpHeaders();
			
			HttpEntity<LoginRequest> request = new HttpEntity<LoginRequest>(login, headers);
			
			LoginResponse responseObject  =  (LoginResponse)  restTemplate.postForObject(URL_H2H_LOGIN.replace("@es", idioma) , request, LoginResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			LOGGER.error("[ERROR AL LOGUEARSE ] ["+login.getLogin()+"]", ex);
			return new LoginResponse();
		}
	}
	
	public LogoutResponse logout(LogoutRequest logout,String idioma){
		try{
			
			HttpHeaders headers = new HttpHeaders();
			
			HttpEntity<LogoutRequest> request = new HttpEntity<LogoutRequest>(logout, headers);
			
			LogoutResponse responseObject  =  (LogoutResponse)  restTemplate.postForObject(URL_H2H_LOGOUT.replace("@es", idioma) , request, LogoutResponse.class);
			return responseObject;
			
		}catch(Exception ex){
			LOGGER.error("[ERROR LOGOUT ] ["+logout.getLogin()+"]", ex);
			return new LogoutResponse();
		}
	}
	
	/*public EnqueueTransactionResponse EnqueuePayment(EnqueuePaymentRequest payment,String idioma){
		EnqueueTransactionResponse responseObject = new EnqueueTransactionResponse();
		try{
			login = login();
			payment.setToken(login.getToken());
			HttpHeaders headers = new HttpHeaders();
			
			HttpEntity<EnqueuePaymentRequest> request = new HttpEntity<EnqueuePaymentRequest>(payment, headers);
			LOGGER.debug("JSON REQUEST: " + gson.toJson(payment));
			responseObject  =  (EnqueueTransactionResponse)  restTemplate.postForObject(URL_H2H_ENQUEUE_PAYMENT.replace("@es", idioma) , request, EnqueueTransactionResponse.class);
			
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getRawStatusCode() == 400 || client.getRawStatusCode() == 401)
				responseObject = gson.fromJson(client.getResponseBodyAsString(), EnqueueTransactionResponse.class);
			//return responseObject;
		}catch(Exception ex){
			LOGGER.error("[ERROR AL ENCOLAR PAGO] ["+payment.getClientReference()+"]", ex);
			//return new EnqueueTransactionResponse();
		}
		logout();
		return responseObject;
	}*/
	
	/*
	 * Encola una transacción de alta de registro de cuenta en la plataforma H2H banorte.
	 */
	public EnqueueTransactionResponse EnqueueAccountSignUp(EnqueueAccountSignUpRequest AccountSignUp,String idioma ){
		EnqueueTransactionResponse responseObject = new EnqueueTransactionResponse();
		try{
			login = login();
			AccountSignUp.setToken(login.getToken());
			HttpHeaders headers = new HttpHeaders();
			//headers.setContentType(MediaType.APPLICATION_JSON);
		//	headers.setAccept((List<MediaType>) MediaType.APPLICATION_JSON);
			
			HttpEntity<EnqueueAccountSignUpRequest> request = new HttpEntity<EnqueueAccountSignUpRequest>(AccountSignUp, headers);
			LOGGER.debug("JSON REQUEST: " + gson.toJson(AccountSignUp));
			responseObject  =  (EnqueueTransactionResponse)  restTemplate.postForObject(URL_H2H_ENQUEUE_SING_UP.replace("@es", idioma) , request, EnqueueTransactionResponse.class);
			
			
		}catch(HttpClientErrorException client){
			LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getStatusCode().value()== 400 || client.getStatusCode().value() == 401)
				responseObject = gson.fromJson(client.getResponseBodyAsString(), EnqueueTransactionResponse.class);
			//return responseObject;
		}catch(Exception ex){
			LOGGER.error("[ERROR EN ALTA DE REGISTRO] ["+AccountSignUp.getClientReference()+"]", ex);
			//return new EnqueueTransactionResponse();
		}
		logout();
		return responseObject;
	}
	
	/*
	 * Realiza la consulta del status de una transacción encolada en el H2H Banorte.
	 */
	/*public TransactionEnquiryResponse TransactionEnquiry(TransactionEnquiryRequest TransactionEnquiry, String idioma){
		TransactionEnquiryResponse responseObject = new TransactionEnquiryResponse();
		try{
			login = login();
			HttpHeaders headers = new HttpHeaders();
			
			HttpEntity<TransactionEnquiryRequest> request = new HttpEntity<TransactionEnquiryRequest>(TransactionEnquiry, headers);
			LOGGER.debug("JSON REQUEST: " + gson.toJson(TransactionEnquiry));
			responseObject  =  (TransactionEnquiryResponse)  restTemplate.postForObject(URL_H2H_TRANSACTION_ENQUIRY.replace("@es", idioma) , request, TransactionEnquiryResponse.class);
			
			
		}catch(HttpClientErrorException client){ 
			LOGGER.debug("Error " + client.getStatusText() + "  " + client.getResponseBodyAsString());
			if(client.getRawStatusCode() == 400 || client.getRawStatusCode() == 401)
				responseObject = gson.fromJson(client.getResponseBodyAsString(), TransactionEnquiryResponse.class);
			//return responseObject;
		} 
		catch(Exception ex){
			LOGGER.error("[ERROR AL CONSULTAR TRANSACCION ] ["+TransactionEnquiry.getTransactionReference()+"]", ex);
			//return new TransactionEnquiryResponse();
		}
		logout();
		return responseObject;
	}*/
	
	private LoginResponse login(){
		try{
			LoginRequest login  = new LoginRequest();
			login.setLogin("addcel_banorte");
			login.setPassword("B4N0rt3%");
			LOGGER.debug("iniciando login");
			LoginResponse response = login(login, "es");
			LOGGER.debug("LOGUIN: "+  response.getToken() + " " + response.getResult().isSuccess() + " " + response.getResult().getMessage());
			return response;
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER LOGIN: ", ex);
			return new  LoginResponse();
		}
	}
	
	private LogoutResponse logout(){
		try{
			LogoutRequest logout = new LogoutRequest();
			logout.setLogin("addcel_banorte");
			logout.setToken(login.getToken());
			LogoutResponse response = logout(logout, "es");
			LOGGER.debug("[RESPUESTA DE LOGOUT]" +  response.getResult().isSuccess());
			return response;
		}catch(Exception ex){
			LOGGER.error("[ERROR EN LOGOUT]", ex);
			return new LogoutResponse();
		}
	}
}
