package com.addcel.usuarios.services;

import com.addcel.usuarios.model.vo.ThreatMetrixRequest;
import com.addcel.usuarios.model.vo.ThreatMetrixResponse;

public interface ThreatMetrixClient {

	public ThreatMetrixResponse investigatePayment(ThreatMetrixRequest request);
	
}