package com.addcel.usuarios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.usuarios.ws.clientes.qr.QR;
import com.addcel.usuarios.ws.clientes.qr.QRClient;
import com.addcel.usuarios.ws.clientes.qr.QRMXRequest;
import com.addcel.usuarios.ws.clientes.qr.QRMXResponse;
import com.addcel.usuarios.ws.clientes.qr.QRequest;
import com.google.gson.Gson;

@Service
public class QRClientImp implements QRClient{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QRClientImp.class);
	
	private final static String GET_QR = "http://localhost/VisaQRWalletService/{idApp}/{idPais}/{idioma}/{idEstablecimiento}/business/generate/qr";
	
	private final static String GENERATE_QR_MX = "http://localhost/CuantoTeDebo/{idApp}/{idioma}/generateQR";
	
	private Gson gson = new Gson();
	
	@Override
	public QR getQRComercio(long idComercio,int idApp , int idPais, String idioma, QRequest qrequest) {
		QR qr = new QR();
		try{
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			
			HttpEntity<QRequest> entity = new HttpEntity<QRequest>(qrequest,headers);
			
			LOGGER.debug("ENVIANDO PETICION GET QR COMERCIO " + gson.toJson(entity));
			qr = restTemplate.postForObject(GET_QR.replace("{idApp}", idApp+"").replace("{idPais}",idPais+"").replace("{idioma}",idioma).replace("{idEstablecimiento}",idComercio+""),entity,QR.class); 
			LOGGER.debug("RESPUESTA QR COMERCIO " + gson.toJson(qr));
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER QR PARA COMERCIO " + idComercio, ex);
		}
		return qr;
	}
	
	@Override
	public QRMXResponse getQRComercioMx(QRMXRequest req, int idApp, String idioma) {
		QRMXResponse response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			
			HttpEntity<QRMXRequest> entity = new HttpEntity<QRMXRequest>(req,headers);
			LOGGER.debug("ENVIANDO PETICION GET QR COMERCIO " + gson.toJson(entity));
			
			response = restTemplate.postForObject(GENERATE_QR_MX.replace("{idApp}", idApp+"").replace("{idioma}",idioma),entity,QRMXResponse.class); 
			LOGGER.debug("RESPUESTA QR MEX {}",gson.toJson(response));
		}catch(Exception ex) {
			LOGGER.error("ERROR AL OBTENER CODIGO QR MX",ex);
		}
		return response;
	}

}
