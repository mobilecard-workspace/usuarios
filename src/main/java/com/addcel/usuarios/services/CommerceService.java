package com.addcel.usuarios.services;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.axis.encoding.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.usuarios.model.mapper.UsuariosServiciosMapper;
import com.addcel.usuarios.model.vo.Bank;
import com.addcel.usuarios.model.vo.BaseResponse;
import com.addcel.usuarios.model.vo.CardCommerce;
import com.addcel.usuarios.model.vo.CardRequest;
import com.addcel.usuarios.model.vo.Establecimiento;
import com.addcel.usuarios.model.vo.MobileCardCard;
import com.addcel.usuarios.model.vo.MobilecardCardMovement;
import com.addcel.usuarios.model.vo.PreviValeResponse;
import com.addcel.usuarios.model.vo.ProjectMC;
import com.addcel.usuarios.model.vo.ResponseSMS;
import com.addcel.usuarios.model.vo.TUsuarios;
import com.addcel.usuarios.model.vo.TemplateCorreo;
import com.addcel.usuarios.model.vo.VerifyResponse;
import com.addcel.usuarios.model.vo.VerifyResponseCommerce;
import com.addcel.usuarios.spring.model.LoginCommerce;
import com.addcel.usuarios.spring.model.NegocioCreateRequest;
import com.addcel.usuarios.spring.model.NegocioCreateResponse;
import com.addcel.usuarios.utils.Business;
import com.addcel.usuarios.utils.Constantes;
import com.addcel.usuarios.utils.UtilsMC;
import com.addcel.usuarios.ws.clientes.qr.QR;
import com.addcel.usuarios.ws.clientes.qr.QRClient;
import com.addcel.usuarios.ws.clientes.qr.QRMXRequest;
import com.addcel.usuarios.ws.clientes.qr.QRMXResponse;
import com.addcel.usuarios.ws.clientes.qr.QRequest;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

import crypto.Crypto;

@Service
public class CommerceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommerceService.class);
	
	@Autowired
	private UsuariosServiciosMapper mapper;
	
	private Gson gson = new Gson();
	
	
	@Autowired
	private QRClient qrClient;
	
	
	
	public ModelAndView userActivate(String codigo, String idioma){
    	ModelAndView view = new ModelAndView();
    	LOGGER.debug("[Activando negocio ] " + codigo);
    	try{
    		String dataHard = new String(Base64.decode(codigo));
    		
    		String[] data = new String(AddcelCrypto.decryptHard(dataHard)).split(":");
    		String id = data[0];
    		String pass = data[1];
    		
    		mapper.commerceActivate(Long.parseLong(id));
    		Establecimiento est = mapper.getEstablecimiento(Long.parseLong(id));
    		view.setViewName("activate");
    		view.addObject("establecimiento",est.getNombre_establecimiento());
    		if(idioma.equalsIgnoreCase("es"))
    			view.addObject("message", "Su Cuenta de correo ha sido verificada exitosamente");
    		else
    			view.addObject("message", "Su Cuenta de correo ha sido verificada exitosamente");

    		return view;
    	}catch(Exception ex){
    		LOGGER.error("[Error al activar usuario] ", ex);
    		ex.printStackTrace();
    		view.setViewName("activate");
    		view.addObject("message", "Error al activar cuenta");
    		return view;
    	}
    }
	
	public BaseResponse sendMailPasswordReset(String admin, String idioma, int idApp){
    	LOGGER.debug("[Enviando correo para reestablecer contraseña]: " + admin );
    	
    	BaseResponse response = new BaseResponse();
    	
    	try{
    		    Establecimiento est = mapper.getEstablecimientoByUser(admin, idApp);
    		
    			if(est != null && !est.getEmail_contacto().isEmpty()){
    				if(est.getEstatus() == 1){
			    			String cadSensitive = java.util.Base64.getEncoder().encodeToString(AddcelCrypto.encryptHard(est.getId()+"").getBytes());
			    			
			    			TemplateCorreo correo = mapper.getTemplateMail("@MENSAJE_RESETPWD_COMMERCE", idioma, idApp);
			    			
		    			
			    			UtilsMC.emailSend(est.getEmail_contacto(),correo.getBody().replace("@GETP", "?cod="+cadSensitive).replace("@IDIOMA", idioma), correo.getAsunto(), est.getRepresentante_legal(), "","", "", new String[]{Constantes.Path_images+"user_update_password.PNG"});
			    			
			    			response.setIdError(0);
			    			if(idioma.equalsIgnoreCase("es"))
			    				response.setMensajeError("Se envio un mensaje a su correo electrónico para continuar con el proceso.");
			    			else
			    				response.setMensajeError("A message was sent to your email to continue with the process.");
    				}
    				else
    				{
    					response.setIdError(600);
		    			if(idioma.equalsIgnoreCase("es"))
		    				response.setMensajeError("Su registro presenta problemas para reestablecer su contraseña, contacte a soporte");
		    			else
		    				response.setMensajeError("Your registration has problems to reestablish your password, contact support");
    				}
    			}else
    			{
    				response.setIdError(90);
    				if(idioma.equalsIgnoreCase("es"))
    					response.setMensajeError("No existe comercio registrado con el dato proporcionado");
    				else
    					response.setMensajeError("There is no registered commerce with the data provided");
    			}
    		
    		
    	}catch(Exception ex){
    		LOGGER.debug("[ERROR AL RESETEAR PASWORD] USER: " + admin, ex);
    		response.setIdError(90);
			if(idioma.equalsIgnoreCase("es"))
				response.setMensajeError("Ocurrio un error, intente mas tarde.");
			else
				response.setMensajeError("An error occurred, please try again.");
    	}
    	LOGGER.debug("[FINALIZANDO RESET DE PASSWORD ] USER: " + admin );
    	return response;
    }
	
	public ModelAndView emailConfirm(String cod, String error, String idioma, int idApp){
    	
    	try{
    		
    		ModelAndView view = new ModelAndView("commerce/confirmEmail");
    		if(error != null && !error.isEmpty())
    			view.addObject("error", error);
    			else
    				view.addObject("error","");
    		view.addObject("cod",cod);
    		view.addObject("idioma",idioma);
    		view.addObject("idApp",idApp);
    		return view;
    		
    	}catch(Exception ex){
    		LOGGER.error("ERROR AL MOSTRAR VISTA *CONFIRMA CORREO", ex);
    		ModelAndView view = new ModelAndView("commerce/confirmEmail");
    		if(idioma.equalsIgnoreCase("es"))
    			view.addObject("error","ERROR INESPERADO, INTENTE MAS TARDE");
    		else
    			view.addObject("error","UNEXPECTED ERROR, TRY LATER");
    		return view;
    		
    	}
    }
	
	
	public ModelAndView validEmail(String cod,String idioma,String email, int idApp){
    	
    	ModelAndView view = null;
    	try{
    		
    		String Cdecryp = new String(java.util.Base64.getDecoder().decode(cod.getBytes()));
    		
    		//TUsuarios user = mapper.getUserInfo(Long.parseLong(AddcelCrypto.decryptHard(Cdecryp)), idApp);
    		Establecimiento est = mapper.getEstablecimiento(Long.parseLong(AddcelCrypto.decryptHard(Cdecryp)));
    		
    		if(est != null && est.getEmail_contacto().equals(email)){
    		view = new ModelAndView("commerce/resetpass");
    			view.addObject("cod",cod);
    			view.addObject("idioma",idioma);
    			view.addObject("idApp",idApp);
    		}
    		else{
    			view = new ModelAndView("commerce/confirmEmail");
    			if(idioma.equalsIgnoreCase("es"))
    				view.addObject("error", "El correo no coincide en el sistema");
    			else
    				view.addObject("error", "Mail does not match in the system");
        		view.addObject("cod",cod);
        		view.addObject("idioma",idioma);
        		view.addObject("idApp",idApp);
    		}
    		
    		
    		return view;
    		
    	}catch(Exception ex){
    		LOGGER.error("ERROR AL VALIDAR CORREO ELECTRONICO", ex);
    		view = new ModelAndView("commerce/confirmEmail");
    		view.addObject("idioma",idioma);
    		view.addObject("idApp",idApp);
    		if(idioma.equalsIgnoreCase("es"))
    			view.addObject("error", "error inexperado, intente mas tarde");
    		else
    			view.addObject("error","unexperienced error, try later");
    		view.addObject("cod",cod);
    		return view;
    	}
    }
	
	
	public ModelAndView resetPass(String cod, String pass, String idioma, int idApp){
    	ModelAndView view = new ModelAndView("commerce/resetPSuccess");
    	view.addObject("idApp",idApp);
    	try{
    		String Cdecryp = new String(java.util.Base64.getDecoder().decode(cod.getBytes()));//AddcelCrypto.decryptSensitive(cod);
			//String passr = UtilsMC.generaSemillaAux(pass);
			String passrc = Business.encode(pass); //Crypto.aesEncrypt(passr, pass);
			Establecimiento est = mapper.getEstablecimiento(Long.parseLong(AddcelCrypto.decryptHard(Cdecryp)));
    		String responseJSON = mapper.resetPasswordCommerce(idApp, est.getId(), passrc, idioma);
    		LOGGER.debug("[RESPUESTA BASE DE DATOS] " + responseJSON);
    		BaseResponse responseBD =  gson.fromJson(responseJSON, BaseResponse.class);
    		view.addObject("message",responseBD.getMensajeError());
    	}catch(Exception ex){
    		LOGGER.error("ERROR AL RESETEAR PASSWORD", ex);
    		if(idioma.equalsIgnoreCase("es"))
    			view.addObject("message", "Ocurrio un error al restaurar su contraseña, intente mas tarde");
    		else
    			view.addObject("message", "An Error occurred while restoring your password, try later");
    	}
    	return view;
    }

	public VerifyResponseCommerce statusVerify(Integer idApp, long id, String idioma) {
		VerifyResponseCommerce response = new VerifyResponseCommerce();
		MobileCardCard card = new MobileCardCard();
		PreviValeService previvale = new PreviValeService();
		card.setMovements(new ArrayList<MobilecardCardMovement>());
		try {
			 LOGGER.debug("VERIFICANDO TARJETA PREVIVALE...");
			 Establecimiento establecimiento = mapper.getEstablecimiento(id);
			 CardCommerce cardC = mapper.getCardCommerce(id, idApp);
			 if(cardC != null ){
				 
				 card.setPan( AddcelCrypto.encryptHard(UtilsMC.getSMS(cardC.getNumero_tarjeta())));
				 card.setVigencia(AddcelCrypto.encryptHard(UtilsMC.getSMS(cardC.getVigencia())));
				 card.setCodigo(AddcelCrypto.encryptHard(UtilsMC.getSMS(cardC.getCt())));
				 card.setNombre(cardC.getNombre_tarjeta());
				 if(establecimiento.getIdPais() == 1) {
					PreviValeResponse cardBalance = previvale.getBalance(id, idApp, 1, idioma, cardC.getNumero_tarjeta());
					card.setBalance(cardBalance == null ? 0.0 : Double.parseDouble(cardBalance.getData().getAuthorization()));
					LOGGER.info("OBTIENENDO CUENTA CLABE ASIGNADA TARJETA: {}", id);
					card.setClabe(mapper.getCuentaClabeMobilecard(id));
					LOGGER.info("CUENTA CLABE ASIGNADA : {}", card.getClabe());
				 }
				
				 response.setIdError(0);
				 response.setMensajeError("");
			 }else
			 {
				 response.setIdError(0);
				 response.setMensajeError("");
				 card = null;
			 }
			 
			 LOGGER.info("NEGOCIO ENCONTRADO - {}", gson.toJson(establecimiento));
			 response.setEmail_contacto(establecimiento.getEmail_contacto());
			 response.setEstatus(establecimiento.getEstatus());
			 response.setId(id);
			 response.setMaterno(establecimiento.getMaterno());
			 response.setNombre(establecimiento.getNombre());
			 response.setPaterno(establecimiento.getPaterno());
			 response.setTelefono_contacto(establecimiento.getTelefono_contacto());
			 response.setCard(card);
			 response.setJumioStatus(establecimiento.getJumio_status());
			 response.setJumioReference(establecimiento.getJumio_reference());
			 response.setIdPais(establecimiento.getIdPais());
			 if("3".equals(establecimiento.getJumio_status())) {
				 response.setIdError(0);
				 response.setMensajeError("Su documentación no pudo ser validada correctamente. Contacte a soporte@addcel.com");
			 } else if ("2".equals(establecimiento.getJumio_status())) {
				 response.setIdError(0);
				 if(idioma.equals("es"))
					 response.setMensajeError("Para poder ingresar a tu aplicación recibiras un sms y un correo, en un máximo de 5 minutos. Tus documentos están siendo validados."); //("Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard");
				 else
					 response.setMensajeError("In order to enter your application you will receive an SMS and an email, in a maximum of 5 minutes. Your documents are being validated.");
			 }
			 
		} catch (Exception ex) {
			LOGGER.error("[ERROR AL VERIFICAR ESTATUS ] [USUARIO " + id + "]", ex);
			getBaseResponse(idioma, response, -1000);
		}
		return response;
	}
	
	private void getBaseResponse(String idioma, BaseResponse response, int cod) {
		response.setIdError(cod);
		if (idioma.equalsIgnoreCase("es"))
			response.setMensajeError("Error inesperado, vuelva a intentarlo");
		else
			response.setMensajeError("Unexpected error, please try again");
	}

	public BaseResponse sendEMailCommerce(Integer idApp, long id, String idioma) {
		Establecimiento esta = null;
		BaseResponse response = new BaseResponse();
		try {
			esta = mapper.getEstablecimiento(id);
			LOGGER.debug("id establecimiento: " + esta.getId());
			 String value =  AddcelCrypto.encryptHard(esta.getId()+":"+idApp);
			 LOGGER.debug("base 64: "+ Base64.encode(value.getBytes()));
			 String param = "?codigo="+Base64.encode(value.getBytes());
			 ProjectMC projects = mapper.getProjectMC("LCPFServices", "adminActivate");
			 String url =  projects.getUrl().replace("{idioma}", idioma)+param;//Constantes.ACTIVATION_URL.replace("{idioma}", idioma)+param;
			 LOGGER.debug("[ACTIVATE URL] " + url);
			if(idioma.equalsIgnoreCase("es"))
			    UtilsMC.emailSend(esta.getEmail_contacto(), mapper.getParameter("@MENSAJE_ACTIVATE_LCPF_ES"), 
			    		mapper.getParameter("@ASUNTO_ACTIVATE_LCPF_ES"), esta.getEmail_contacto(), 
			    		url, "", esta.getRepresentante_legal(),new String[]{Constantes.Path_images+"user_registry.PNG"});
			 else
				 UtilsMC.emailSend(esta.getEmail_contacto(), mapper.getParameter("@MENSAJE_ACTIVATE_LCPF_EN"), 
						 mapper.getParameter("@ASUNTO_ACTIVATE_LCPF_EN"), esta.getEmail_contacto(), url,"",  
						 esta.getRepresentante_legal(),new String[]{Constantes.Path_images+"user_registry.PNG"}); 
			response.setMensajeError("Correo ha sido enviado exitosamente.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setIdError(-4000);
			response.setMensajeError("Ocurrio un error al re enviar el correo de activacion.");
		}
		return response;
	}

	public BaseResponse updateReferenceCommerce(Integer idApp, Integer idPais, String idioma, String login,
			String scanReference) {
		BaseResponse response = new BaseResponse();
		try {
			LOGGER.info("ACTUALIZANDO REFERENCIA DE NEGOCIO - {} - {}", login, scanReference);
			mapper.updateCommerceReference(login, scanReference);
			response.setIdError(0);
			response.setMensajeError("Para poder ingresar a tu aplicación recibiras un sms y un correo, en un máximo de 5 minutos. Tus documentos están siendo validados.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError("Ocurrio un error al actualizar");
		}
		return response;
	}
	
	public NegocioCreateResponse CommerceRegister(NegocioCreateRequest req, int idApp, int idPais, String idioma) {
		NegocioCreateResponse response = new NegocioCreateResponse();
		Establecimiento comercio = new Establecimiento();
		Establecimiento establecimiento = null;
		TUsuarios user = new TUsuarios();
		String responseJson = "";
		try {
			
			LOGGER.debug("INICIANDO PROCESO DE ALTA COMERCIO MEXICO");
			LOGGER.debug("OBJETO REQUEST: "+ gson.toJson(req));
			
			 user.setUsrPwd(req.getPassword());
		     user.setUsrLogin(req.getEmail());
		     user.setIdioma(idioma);
		     user.setIdApp(idApp);
		     user.setTipoUsuario("NEGOCIO");
		     
		     int exists = mapper.existsCommerce(req.getEmail(), req.getCelular(), idPais, idApp); 
		     
			if(exists == 0) {
						comercio.setId_aplicacion(idApp);
						comercio.setEstatus(1);
						comercio.setJumio_status("1");
						comercio.setCiudad(req.getCiudad());
						comercio.setColonia(req.getColonia());
						comercio.setCp(req.getCp());
						comercio.setEmail_contacto(req.getEmail());
						comercio.setDireccion(req.getDireccion());
						comercio.setDireccion_establecimiento(req.getDireccion());
						comercio.setIdPais(idPais);
						comercio.setPass(req.getPassword());
						comercio.setUsuario(req.getEmail());
						comercio.setTelefono_contacto(req.getCelular());
						comercio.setNombre_establecimiento(req.getNombreEstablecimiento());
						comercio.setId_estado(req.getIdEstado());
						comercio.setRepresentante_legal(req.getNombre()+ " " + req.getPaterno() + " " + req.getMaterno());
						comercio.setCci(req.getClabe());
						comercio.setCuenta_clabe(req.getClabe());
						comercio.setCuenta_tipo("CLABE");
						
						comercio.setId_banco(req.getIdBanco()+"");
						Bank bank =  mapper.getBancoBillPocket(Long.parseLong(req.getIdBanco()+""));
						comercio.setCodigo_banco(bank != null ? bank.getClave(): "0");
						comercio.setComision_fija(bank != null ? bank.getComision_fija() : 0.0);
						comercio.setComision_porcentaje(0.04);//bank != null ? bank.getComision_porcentaje() : 0
						comercio.setNombre(req.getNombre());
						comercio.setMaterno(req.getMaterno());
						comercio.setPaterno(req.getPaterno());
						comercio.setRazon_social(req.getNombreEstablecimiento());
						comercio.setCurp(req.getCurp());
						comercio.setRfc(req.getRfc());
						comercio.setNombre_prop_cuenta(req.getNombre()+ " " + req.getPaterno() + " " + req.getMaterno());
						
						LOGGER.debug("INSERTANDO COMERCIO ...." + gson.toJson(comercio));
						int cuenta_fav = req.getClabe() == null || req.getClabe().isEmpty() ? 2 : 1;
						mapper.insertEstablecimiento(comercio, null, null, cuenta_fav);
						
						if(comercio.getId() > 0 ) {
							
							LOGGER.debug("COMERCIO INSERTADO CON EXITO");
							
							if(req.getClabe()!= null && req.getClabe().isEmpty()) {
								LOGGER.debug("COMERCIO SIN CLABE BANCARIA, SE ASIGNARA TARJETA PREVIVALE");
								ResponseSMS respPrevi = new ResponseSMS();
								PreviValeService previvale = new PreviValeService();
								try {
								respPrevi = previvale.registrarNegocioSinTarjeta(comercio.getId(), idApp, idPais, idioma);
								}catch(Exception ex) {
									LOGGER.error("error al asignar tarjeta ", ex);
								}
								if(respPrevi != null && respPrevi.getCode() == 1000) {
									LOGGER.debug("Tarjeta asignada correctamente a comercio {}", comercio.getId());
									response.setMensajeError("¡Felicidades! Tu tarjeta MobileCard ha sido creada exitosamente, ve a la sección Cartera para mas información");
									response.setCardPrevivale(true);
								}else {
									LOGGER.debug("ERROR AL REGISTRAR COMERCIO EN PREVIVALE");
									response.setCardPrevivale(false);
									response.setIdError(100l);
									response.setMensajeError("su registro fue exitoso, pero hay problemas con su cuenta MobileCard");
								}
									
							}else {
								response.setCardPrevivale(false);
							}
							
							 /***************ACTUALIZANDO QR**************/
						    updateQRComercio(comercio.getId(), idApp,idPais, idioma, req.getNombreEstablecimiento());
							
						    
						    responseJson = mapper.login(user);
							LOGGER.info("RESPONSE FROM DB: {}", responseJson.replaceAll("\"photo\":\"((([^\"]){10}).*)\",", "\"photo\":\"$2\","));
							establecimiento = gson.fromJson(responseJson, Establecimiento.class);
							BaseResponse msj = gson.fromJson(responseJson, BaseResponse.class);
							LOGGER.info("RESPUESTA DE LOGIN: {COMERCIO}", gson.toJson(establecimiento));
							
							response.setComisionFija(establecimiento.getComision_fija());
							response.setComisionPorcentaje(establecimiento.getComision_porcentaje());
							response.setCuentaClabe(establecimiento.getCuenta_clabe());
							response.setDireccionEstablecimiento(establecimiento.getDireccion_establecimiento());
							response.setEmail(establecimiento.getEmail_contacto());
							response.setIdAccount(establecimiento.getId_account());
							response.setIdBanco(establecimiento.getId_banco());
							response.setIdError(Long.parseLong(msj.getIdError()+""));
							response.setMensajeError(response.getMensajeError() == null || response.getMensajeError().isEmpty() ? msj.getMensajeError(): response.getMensajeError());
							response.setIdEstablecimiento(establecimiento.getId());
							response.setIdUsrStatus(Long.parseLong(establecimiento.getEstatus()+""));
							response.setNegocio(true);
							response.setNombreEstablecimiento(establecimiento.getNombre_establecimiento());
							response.setNombrePropCuenta(establecimiento.getNombre_prop_cuenta());
							response.setRazonSocial(establecimiento.getRazon_social());
							response.setRepresentanteLegal(establecimiento.getRepresentante_legal());
							response.setTelefono(establecimiento.getTelefono_contacto());
							response.setUrlLogo(establecimiento.getUrlLogo());
							response.setJumioStatus(establecimiento.getJumio_status());
							response.setScanReference(establecimiento.getJumio_reference());
							response.setIdPais(establecimiento.getIdPais());
							response.setQrTag(establecimiento.getQrTag());
							response.setQrBase64(establecimiento.getQrBase64());
							response.setTipo_persona(establecimiento.getTipo_persona());
							response.setCard(null);
							
							
							LOGGER.debug("ALTA COMERCIO MEXICO FINALIZADO CON EXITO...");
					}else {
						LOGGER.debug("ERROR AL INSERTAR REGISTRO DE COMERCCIO MEXICO EN BASE DE DATOS");
						response.setIdError(300l);
						response.setMensajeError("Error al procesar peticion, contacte a soporte");
					}
			}else {
				LOGGER.debug(" SE ENCONTRO UN COMERCIO CON EL EMAIL Y NUMERO DE TELEFONO YA REGISTRADO");
				response.setIdError(700l);
				response.setMensajeError("El email ó número telefónico ya se encuentra registrado");
			}
				
		}catch(Exception ex) {
			LOGGER.debug("ERROR AL REGISTRAR COMERCIO ", ex);
			response.setIdError(400l);
			response.setMensajeError("ERROR AL REGISTRAR COMERCIO, CONTACTE A SOPORTE");
		}finally {
			quitNulls(response);
			if(  (response.getIdEstablecimiento() == null || response.getIdEstablecimiento()==0) && response.getIdError() == 0l ) {
				LOGGER.debug("ERROR ALTA COMERCIO MEXICO ");
				response.setIdError(600l);
				response.setMensajeError("Su registro presenta problemas, contacte a soporte");
			}
		}
		LOGGER.debug("RESPUESTA RETORNADA (ALTA COMERCIO): " +gson.toJson(response));
		return response;
	}
	
	private void quitNulls(LoginCommerce commerce){
		try{
			if(commerce.getNegocio() == null)
				commerce.setNegocio(false);
			if(commerce.getCard() == null)
				commerce.setCard(new CardRequest());
			if(commerce.getCard().getCodigo() == null)
				commerce.getCard().setCodigo("");
			if(commerce.getCard().getCpAmex() == null)
				commerce.getCard().setCpAmex("");
			if(commerce.getCard().getDomAmex() == null)
				commerce.getCard().setDomAmex("");
			if(commerce.getCard().getNombre() == null)
				commerce.getCard().setNombre("");
			if(commerce.getCard().getPan() == null)
				commerce.getCard().setPan("");
			if(commerce.getCard().getRepresentanteLegal() == null)
				commerce.getCard().setRepresentanteLegal("");
			if(commerce.getCard().getTipoTarjeta() == null)
				commerce.getCard().setTipoTarjeta("");
			if(commerce.getCard().getVigencia() == null)
				commerce.getCard().setVigencia("");
				
			if(commerce.getComisionFija() == null)
				commerce.setComisionFija(0.0);
			if(commerce.getComisionPorcentaje() == null)
				commerce.setComisionPorcentaje(0.0);
			if(commerce.getCuentaClabe() == null)
				commerce.setCuentaClabe("");
			if(commerce.getDireccionEstablecimiento() == null)
				commerce.setDireccionEstablecimiento("");
			if(commerce.getEmail() == null)
				commerce.setEmail("");
			if(commerce.getIdAccount() == null)
				commerce.setIdAccount(0l);
			if(commerce.getIdBanco() == null)
				commerce.setIdBanco("");
			if(commerce.getIdEstablecimiento() == null)
				commerce.setIdEstablecimiento(0l);
			if(commerce.getIdPais() == null)
				commerce.setIdPais(0);
			if(commerce.getIdUsrStatus() == null)
				commerce.setIdUsrStatus(0l);
			if(commerce.getJumioStatus() == null)
				commerce.setJumioStatus("");
			if(commerce.getNombreEstablecimiento() == null)
				commerce.setNombreEstablecimiento("");
			if(commerce.getNombrePropCuenta() == null)
				commerce.setNombrePropCuenta("");
			if(commerce.getRazonSocial()== null)
				commerce.setRazonSocial("");
			if(commerce.getRepresentanteLegal() == null)
				commerce.setRepresentanteLegal("");
			if(commerce.getScanReference() == null)
				commerce.setScanReference("");
			if(commerce.getTelefono() == null)
				commerce.setTelefono("");
			if(commerce.getUrlLogo() == null)
				commerce.setUrlLogo("");
				
		}catch(Exception ex){
			LOGGER.error("ERROR AL QUITAR NULLS", ex);
		}
	}
	
	private void updateQRComercio(long idcomercio, int idApp, int idPais, String idioma, String nombreComercio){
		try{
			
			 Calendar calendar = Calendar.getInstance();
			 calendar.setTime(new Date());
			 calendar.add(Calendar.YEAR, 3);
			 String FORMAT = "yyyy-MM-dd";
			 SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
			 String fechaFormat = sdf.format(calendar.getTime());
			 String qrTag = null;
			 String qrBase64 = null;
			 
			 switch (idPais) {
			case 1:
				 	QRMXRequest req = new QRMXRequest();
				 	req.setIdEstablecimiento(idcomercio);
				 	req.setIdPais(idPais);
				 	req.setMonto(0.0);
				 	req.setTypeQR("ESTATICO");
				 	QRMXResponse res =  qrClient.getQRComercioMx(req, idApp, idioma);
				 	if(res != null && res.getIdError() == 0) {
				 		qrTag = "";
				 		qrBase64 = res.getQrBase64();
				 	}else{
						 LOGGER.debug("NO SE PUDO OBTENER QR COMERCIO {}", idcomercio);
					 }
				break;

			default:
				LOGGER.debug("QR NO DISPONOBLE PARA PAIS {}",idPais);
				break;
			}
			
			 if(qrBase64 != null && !qrBase64.isEmpty()) 
				 mapper.updateCommerceQR(idcomercio, qrTag, qrBase64);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTUALIZAR QR COMERCIO ", ex);
		}
	}

	public Integer isPeru() {
		int result = 0;
		try {
			result = mapper.isPeru();
			LOGGER.debug("id Perú: "+result);
		} catch (Exception e) {
			LOGGER.error("ERROR AL OBTENER EL ID DE PERÚ  " +e.getLocalizedMessage());
		}
		return result;
	}
	
}
