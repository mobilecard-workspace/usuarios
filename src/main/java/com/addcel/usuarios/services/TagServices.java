package com.addcel.usuarios.services;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.usuarios.model.mapper.UsuariosServiciosMapper;
import com.addcel.usuarios.model.vo.BalanceResponse;
import com.addcel.usuarios.model.vo.BaseResponse;
import com.addcel.usuarios.model.vo.ProcessTagsVO;
import com.addcel.usuarios.model.vo.Tag;
import com.addcel.usuarios.model.vo.TagResponse;
import com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoapProxy;
import com.google.gson.Gson;

@Service
public class TagServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TagServices.class);
	
	@Autowired
	private UsuariosServiciosMapper mapper;
    private Gson gson = new Gson();
    
    
    public TagResponse getTag(Integer idApp, long idUsuario, String idioma, int provider){
    	TagResponse response = null;
    	try{
    		
    		 int process = provider == -1 ? 1 : 2; // process = 1 all tags, process 2 by provider
    		ProcessTagsVO processTags = new ProcessTagsVO(idApp, idUsuario, "", provider, "", 0, idioma, process);
    		String resultJSON = mapper.processTags(processTags);
    		LOGGER.debug("[RESPUESTA BD ] " + resultJSON);
    		response = gson.fromJson(resultJSON, TagResponse.class);
    		
    	}catch(Exception  ex){
    		LOGGER.error("[ERROR AL OBTENER TAG ][" + idUsuario +"]", ex);
    		response = new TagResponse();
    		response.setIdError(99);
    		if(idioma.equalsIgnoreCase("es"))
    				response.setMensajeError("error inesperado, vuelva a intentarlo");
    		else
    			response.setMensajeError("Unexpected error, please try again");
    	}
    	
    	return response;
    	
    }
    
    
    public BaseResponse deleteTag(Integer idApp, String idioma, long idUsuario,String idTag){
    	BaseResponse response = null;
    	try{
    		ProcessTagsVO processTags = new ProcessTagsVO(idApp, idUsuario, idTag, 0, "", 0, idioma, 5);
    		String resultJSON = mapper.processTags(processTags);
    		response = gson.fromJson(resultJSON, BaseResponse.class);
    		
    	}catch(Exception ex){
    		LOGGER.error("[ERROR AL BORRAR TAG ][ " + idUsuario +"]", ex);
    		response = new TagResponse();
    		response.setIdError(99);
    		if(idioma.equalsIgnoreCase("es"))
    				response.setMensajeError("error inesperado, vuelva a intentarlo");
    		else
    			response.setMensajeError("Unexpected error, please try again");
    	}
    	return response;
    }
    
    
    public TagResponse deleteTagByProvider(Integer idApp, String idioma, long idUsuario,String idTag, int provider){
    	TagResponse response = null;
    	BaseResponse responseDelete = null;
    	try{
    		
    		responseDelete = deleteTag(idApp, idioma, idUsuario, idTag);
    		response = getTag(idApp, idUsuario, idioma, provider);
    		response.setIdError(responseDelete.getIdError());
    		response.setMensajeError(responseDelete.getMensajeError());
    		
    	}catch(Exception ex){
    		LOGGER.error("[ERROR AL BORRAR TAG POR PROVEDOR ][ " + idUsuario +"]", ex);
    		response = new TagResponse();
    		response.setIdError(99);
    		if(idioma.equalsIgnoreCase("es"))
    				response.setMensajeError("error inesperado, vuelva a intentarlo");
    		else
    			response.setMensajeError("Unexpected error, please try again");
    	}
    	return response;
    }
    
    public TagResponse addTag(Integer idApp, String idioma, long idUsuario,Tag tag, int provider){
    	TagResponse response = null;
    	BaseResponse responseInsert = null;
    	BalanceResponse balance = null;
    	ProcessTagsVO processTags = null;
    	try{
    		balance = checkBalanceTag(idApp, idioma, idUsuario, tag);
    		if(balance.getBalance() != -1) {
    			processTags = new ProcessTagsVO(idApp, idUsuario, tag.getTag(), tag.getTiporecargatag(), tag.getEtiqueta(), tag.getDv(), idioma, 3);
        		String resultJSON = mapper.processTags(processTags);
        		responseInsert = gson.fromJson(resultJSON, BaseResponse.class);
        		response = getTag(idApp, idUsuario, idioma, provider);
        		response.setIdError(responseInsert.getIdError());
        		response.setMensajeError(responseInsert.getMensajeError());
    		} else {
    			processTags = new ProcessTagsVO(idApp, idUsuario, tag.getTag(), tag.getTiporecargatag(), tag.getEtiqueta(), tag.getDv(), idioma, 3);
    			response = getTag(idApp, idUsuario, idioma, provider);
    			response.setIdError(-1);
    			if(idioma.equalsIgnoreCase("es")) {
    				response.setMensajeError("Tag no valido.");
    			} else {
            		response.setMensajeError("Tag not valid.");
    			}
    		}
    	}catch(Exception ex){
    		LOGGER.error("[ERROR AL AÑADIR TAG POR PROVEDOR ][ " + idUsuario +"]", ex);
    		response = new TagResponse();
    		response.setIdError(99);
    		if(idioma.equalsIgnoreCase("es"))
    				response.setMensajeError("error inesperado, vuelva a intentarlo");
    		else
    			response.setMensajeError("Unexpected error, please try again");
    	}
    	return response;
    }
    
    public TagResponse updateTag(Integer idApp, String idioma, long idUsuario,Tag tag, int provider){
    	TagResponse response = null;
    	BaseResponse responseUpdate = null;
    	try{
    		ProcessTagsVO processTags = new ProcessTagsVO(idApp, idUsuario, tag.getTag(), tag.getTiporecargatag(), tag.getEtiqueta(), tag.getDv(), idioma, 4);
    		String resultJSON = mapper.processTags(processTags);
    		responseUpdate = gson.fromJson(resultJSON, BaseResponse.class);
    		
    		response = getTag(idApp, idUsuario, idioma, provider);
    		response.setIdError(responseUpdate.getIdError());
    		response.setMensajeError(responseUpdate.getMensajeError());
    		
    	}catch(Exception ex){
    		LOGGER.error("[ERROR AL AÑADIR TAG POR PROVEDOR ][ " + idUsuario +"]", ex);
    		response = new TagResponse();
    		response.setIdError(99);
    		if(idioma.equalsIgnoreCase("es"))
    				response.setMensajeError("error inesperado, vuelva a intentarlo");
    		else
    			response.setMensajeError("Unexpected error, please try again");
    	}
    	return response;
    }


	public BalanceResponse checkBalanceTag(Integer idApp, String idioma, long idUsuario, Tag tag) {
		BalanceResponse balance = new BalanceResponse();
		BigDecimal response = null;
		try {
			LOGGER.info("CONSULTANDO SALDO TAG - {} - USUARIO - {}", tag.getTag(), idUsuario);
			CheckBalanceSoapProxy proxy = new CheckBalanceSoapProxy();
			response = proxy.consultarSaldoTag(tag.getTag());
			if(response.doubleValue() != -1) {
				balance.setBalance(response.doubleValue());
				balance.setMensajeError("Success");
			} else {
				balance.setIdError(-1);
				balance.setMensajeError("Tag no valido.");
				balance.setBalance(response.doubleValue());
			}
			LOGGER.info("CONSULTANDO SALDO TAG - {} - SALDO - {}", tag.getTag(), balance.getBalance());
		} catch (Exception e) {
			e.printStackTrace();
			balance.setIdError(-1);
			balance.setMensajeError("No se puedo realizar la consulta de saldo. ");
			LOGGER.error("ERROR AL CONSULTAR SALDO TAG - {} - USUARIO - {}", tag.getTag(), idUsuario);
		}
		return balance;
	}
    

}
