package com.addcel.usuarios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.addcel.usuarios.ws.clientes.jumio.JumioClient;
import com.addcel.usuarios.ws.clientes.jumio.JumioVerifyRes;
import com.google.gson.Gson;

@Service
public class JumioClientImp implements JumioClient{

	private static final Logger LOGGER = LoggerFactory.getLogger(JumioClientImp.class);
	
	private final static String PATH_VERIFY_JUMIO_REFERENCE = "http://localhost/Jumio/api/{idApp}/{idPais}/{idioma}/verificar/{jumioReference}";
	
	private Gson GSON = new Gson();
	
	@Override
	public JumioVerifyRes verifcarReferencia(String referencia,int idApp,int idPais, String idioma) {
		JumioVerifyRes response = null;
		try{
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			
			//UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(PATH_VERIFY_JUMIO_REFERENCE.replace("{idApp}",idApp+"").replace("{idPais}",idPais+"").replace("{idioma}", idioma).replace("{jumioReference}", referencia));
			HttpEntity<?> request = new HttpEntity<>(headers);
			
			LOGGER.debug("URL: " + PATH_VERIFY_JUMIO_REFERENCE.replace("{idApp}",idApp+"").replace("{idPais}",idPais+"").replace("{idioma}", idioma).replace("{jumioReference}", referencia));
			LOGGER.debug("ENVIANDO PETICION JUMIO ");
			ResponseEntity<String> respJum = restTemplate.exchange(PATH_VERIFY_JUMIO_REFERENCE.replace("{idApp}",idApp+"").replace("{idPais}",idPais+"").replace("{idioma}", idioma).replace("{jumioReference}", referencia),HttpMethod.GET, request, String.class);
			
			LOGGER.debug("RESPUESTA JUMIO: " + respJum.getBody());
			response = GSON.fromJson(respJum.getBody(), JumioVerifyRes.class);
		}catch(Exception ex){
			LOGGER.debug("ERROR AL VERIFICAR REFERENCIA JUMIO", ex);
		}
		return response;
	}
	
	
}
