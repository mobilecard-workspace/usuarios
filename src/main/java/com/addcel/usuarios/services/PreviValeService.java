package com.addcel.usuarios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.usuarios.model.vo.PreviValeResponse;
import com.addcel.usuarios.model.vo.ResponseSMS;
import com.google.gson.Gson;


public class PreviValeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PreviValeService.class);
	
	private static  String SERVER = "http://localhost:80/"; // prod
	private static final String URL_GET_BALANCE = SERVER + "PreviVale/api/@idApp/@idPais/@idioma/creditoTarjeta";
	private static final String URL_REGISTRAR_CON_TARJETA = SERVER + "PreviVale/api/@idApp/@idPais/@idioma/registrarNegocioConTarjeta";
	private static final String URL_REGISTRAR_USUARIO_SIN_TARJETA = SERVER + "PreviVale/api/@idApp/@idPais/@idioma/registrarCliente";
	private static final String URL_REGISTRAR_NEGOCIO_SIN_TARJETA = SERVER + "PreviVale/api/@idApp/@idPais/@idioma/registrarNegocioSinTarjeta";
	
	
	private Gson gson = new Gson();
	RestTemplate restTemplate = new RestTemplate();
	
	public PreviValeService() {
		// TODO Auto-generated constructor stub
	}
	
	public PreviValeService(String server) {
		this.SERVER = server;
	}
	

public PreviValeResponse registrarConTarjeta(long idComercio, String pan ,int idApp,int idPais,String idioma){
		
	PreviValeResponse sms = null;
		ResponseEntity<PreviValeResponse> response = null;
		try{
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			
			headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
			HttpEntity<String> entity = new HttpEntity<String>("{\"idEstablecimiento\": "
					+idComercio+",\"tarjeta\": \""+pan+"\"}", headers);
			
			response = restTemplate.exchange(URL_REGISTRAR_CON_TARJETA.replace("@idApp", idApp+"").replace("@idPais", idPais+"").replace("@idioma", idioma), //"http://localhost/PreviVale/api/3/1/es/registrarNegocioConTarjeta" 
					HttpMethod.POST, entity, PreviValeResponse.class);	
			sms = response.getBody();
			LOGGER.info("RESPUESTA DE ACTIVACION TARJETA NEGOCIO - ID {} - RESP {}", idComercio, gson.toJson(sms));
		}catch(Exception ex){
			LOGGER.error("ERROR AL REGISTRAR TARJETA COMERCIO PREVIVALE ENVIANDO TARJETA", ex);
		}
		return sms;
	}
	
public PreviValeResponse getBalance(long idComercio, int idApp, int idPais, String idioma , String tarjeta){
	
	ResponseEntity<String> responseEntity = null;
	PreviValeResponse sms = null;
	try{
		
		RestTemplate rest = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
		HttpEntity<String> entity = new HttpEntity<String>("{\"idEstablecimiento\": "+ idComercio+",\"tarjeta\":\""+ tarjeta+"\""+"}", headers);
		
		responseEntity = rest.exchange(URL_GET_BALANCE.replace("@idApp", idApp+"").replace("@idPais", idPais+"").replace("@idioma", idioma), HttpMethod.POST, entity, String.class);
		LOGGER.info("RESPUESTA DE CREACION DE TARJETA PREVIVALE - {} - {}", responseEntity.getBody());
		sms =  gson.fromJson(responseEntity.getBody(), PreviValeResponse.class);
	}catch(Exception ex){
		LOGGER.error("ERROR AL REGISTRAR USUARIO CON TARJETA", ex);
	}
	return sms;
}



public ResponseSMS registrarUsuarioSinTarjeta(long idUsuario, int idApp, int idPais, String idioma ){
	
	ResponseEntity<String> responseEntity = null;
	ResponseSMS sms = null;
	try{
		
		RestTemplate rest = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
		HttpEntity<String> entity = new HttpEntity<String>("{\"idUsuario\": "+ idUsuario+"}", headers);
		LOGGER.debug("ENVIANDO PETICION DE ASIGNACION TARJETA PREVIVALE .... {} ", idUsuario);
		responseEntity = rest.exchange(URL_REGISTRAR_USUARIO_SIN_TARJETA.replace("@idApp", idApp+"").replace("@idPais", idPais+"").replace("@idioma", idioma), HttpMethod.POST, entity, String.class);
//		previvaleResp = rest.postForEntity(PREVIVALE_URL_REGISTRO, previvaleCard, McResponse.class);
		LOGGER.info("RESPUESTA DE CREACION DE TARJETA PREVIVALE - {} - {}", responseEntity.getBody());
		//response = gson.fromJson(responseEntity.getBody(), MobilecardCardResponse.class);
		sms =  gson.fromJson(responseEntity.getBody(), ResponseSMS.class);
		
	}catch(Exception ex){
		LOGGER.error("ERROR AL REGISTRAR USUARIO CON TARJETA", ex);
	}
	return sms;
}

public ResponseSMS registrarNegocioSinTarjeta(long idComercio, int idApp,int idPais,String idioma){
	ResponseSMS sms = null;
	ResponseEntity<ResponseSMS> response = null;
	try{
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		
		headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
		HttpEntity<String> entity = new HttpEntity<String>("{\"idEstablecimiento\": "+idComercio+"}", headers);
		
		response = restTemplate.exchange(URL_REGISTRAR_NEGOCIO_SIN_TARJETA.replace("@idApp", idApp+"").replace("@idPais", idPais+"").replace("@idioma", idioma), //("http://localhost/PreviVale/api/3/1/es/registrarNegocioConTarjeta", 
				HttpMethod.POST, entity, ResponseSMS.class);	
		sms = response.getBody();
		LOGGER.info("RESPUESTA DE ACTIVACION TARJETA NEGOCIO - ID {} - RESP {}", idComercio, gson.toJson(sms));
	}catch(Exception ex){
		LOGGER.error("ERROR AL INTENTAR REGISTRAR TARJETA NEGOCIO SIN TARJETA", ex);
	}
	return sms;
}
	
}
