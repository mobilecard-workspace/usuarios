package com.addcel.usuarios.services;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.axis.encoding.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.billplease.client.model.AccountRecord;
import com.addcel.billplease.client.model.EnqueueAccountSignUpRequest;
import com.addcel.billplease.client.model.EnqueueTransactionResponse;
import com.addcel.usuarios.model.mapper.UsuariosServiciosMapper;
import com.addcel.usuarios.model.vo.AbstractVO;
import com.addcel.usuarios.model.vo.AppCipher;
import com.addcel.usuarios.model.vo.Bank;
import com.addcel.usuarios.model.vo.BaseResponse;
import com.addcel.usuarios.model.vo.CardRequest;
import com.addcel.usuarios.model.vo.Establecimiento;
import com.addcel.usuarios.model.vo.ExperianUserVO;
import com.addcel.usuarios.model.vo.ImgProfile;
import com.addcel.usuarios.model.vo.InsertRequest;
import com.addcel.usuarios.model.vo.LoginRequest;
import com.addcel.usuarios.model.vo.PreRegistro;
import com.addcel.usuarios.model.vo.ProjectMC;
import com.addcel.usuarios.model.vo.ResponseSMS;
import com.addcel.usuarios.model.vo.TUsuarios;
import com.addcel.usuarios.model.vo.TemplateCorreo;
import com.addcel.usuarios.model.vo.TermResponse;
import com.addcel.usuarios.model.vo.ThreatMetrixRequest;
import com.addcel.usuarios.model.vo.ThreatMetrixResponse;
import com.addcel.usuarios.model.vo.UpdateResponse;
import com.addcel.usuarios.model.vo.UserPhotoReq;
import com.addcel.usuarios.model.vo.VerifyResponse;
import com.addcel.usuarios.spring.model.BankCodesResponse;
import com.addcel.usuarios.spring.model.LoginCommerce;
import com.addcel.usuarios.spring.model.UpdateCommerceRes;
import com.addcel.usuarios.utils.Business;
import com.addcel.usuarios.utils.Constantes;
import com.addcel.usuarios.utils.MailSender;
import com.addcel.usuarios.utils.UtilsMC;
import com.addcel.usuarios.ws.clientes.datacredit.DataCreditClient;
import com.addcel.usuarios.ws.clientes.emaildomains.EmailDomains;
import com.addcel.usuarios.ws.clientes.jumio.JumioClient;
import com.addcel.usuarios.ws.clientes.jumio.JumioVerifyRes;
import com.addcel.usuarios.ws.clientes.qr.QR;
import com.addcel.usuarios.ws.clientes.qr.QRClient;
import com.addcel.usuarios.ws.clientes.qr.QRMXRequest;
import com.addcel.usuarios.ws.clientes.qr.QRMXResponse;
import com.addcel.usuarios.ws.clientes.qr.QRequest;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

import crypto.Crypto;

@Service
public class UserServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServices.class);
	
	private static final String USUARIO = "usuario";
	
	private static final String NEGOCIO = "negocio";

	private H2HCLIENT h2hclient = new H2HCLIENT();
	
	@Autowired
	private UsuariosServiciosMapper mapper;
	
	@Autowired
	private EmailDomains emailDisposable;
	
	@Autowired
	private ThreatMetrixClientImpl tmClient;
	
	@Autowired
	private JumioClient jumioClient;
	
	@Autowired
	private QRClient qrClient;
	

	private Gson gson = new Gson();

	public TUsuarios UserInsert(Integer idApp, InsertRequest insertRequest, String idioma, String sessionId) {
		TUsuarios response = new TUsuarios();
		TUsuarios loginInsert = new TUsuarios();
		ThreatMetrixResponse validate = null;
		AppCipher appInfo = null;
		try {
			LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO");
			LOGGER.debug("Json: " + gson.toJson(insertRequest));
			
			
			if(emailDisposable.isDisposable(insertRequest.getEmail().split("@")[1])){
				response.setIdError(2000);
				if(idioma.equalsIgnoreCase("es"))
					response.setMensajeError("Tu correo es desechable, para crear un usuario deberas ingresar un correo válido");
				else
					response.setMensajeError("Your email is disposable, to create a user you must enter a valid email");
				return response;
			}
			appInfo = mapper.getAppCipher(idApp); 
			
			
			String pass_decrypt=null;
			TUsuarios usuario = new TUsuarios();
			usuario.seteMail(insertRequest.getEmail());
			
			//pass_decrypt = idApp > 3 ? Business.decryptApp(appInfo.getApi_key(),insertRequest.getPassword()) :  AddcelCrypto.decryptHard(insertRequest.getPassword());

			if(idApp <= 3)
				pass_decrypt = AddcelCrypto.decryptHard(insertRequest.getPassword());
			else
				if(idApp == 4)
					pass_decrypt = insertRequest.getPassword();
				else
					if(idApp > 4 )
						pass_decrypt = Business.decryptApp(appInfo.getApi_key(),insertRequest.getPassword());
		
			String passmx = UtilsMC.generaSemillaAux(pass_decrypt);
			String pass_AES = Crypto.aesEncrypt(passmx, pass_decrypt);

			usuario.setUsrLogin(insertRequest.getEmail());
			usuario.setUsrPwd(pass_AES);
			usuario.setUsrNombre(insertRequest.getFirstName());
			usuario.setUsrApellido(insertRequest.getLastName());
			usuario.setUsrTelefono(insertRequest.getPhone());
			usuario.setIdPais(insertRequest.getCountry());
			usuario.setImei(insertRequest.getImei());
			usuario.setSoftware(insertRequest.getOs());
			usuario.setModelo(insertRequest.getPlatform());
			usuario.setIdUsrStatus(idApp == 1 ? 1 : idApp > 3 ? 1 : 100);
			usuario.setIdioma(idioma);
			usuario.setCedula(insertRequest.getCedula() == null ? "" : insertRequest.getCedula());
			usuario.setTipoCedula(insertRequest.getTipoCedula() == null ? 0 : insertRequest.getTipoCedula());
			usuario.setProveedor(insertRequest.getProveedor());
			usuario.setIdApp(idApp);
			usuario.setTipo(insertRequest.getManufacturer());
			usuario.setUsrNacionalidad(insertRequest.getNacionalidad() == null ? "" : insertRequest.getNacionalidad());
			usuario.setUsrDistrito(insertRequest.getDistrict() == null ? "" : insertRequest.getDistrict());
			usuario.setUsrProvincia(insertRequest.getProvince() == null ? "" : insertRequest.getProvince());
			usuario.setUsrDepartamento(insertRequest.getDepartment() == null ? "" : insertRequest.getDepartment());
			usuario.setUsrCentroLaboral(insertRequest.getWork() == null ? "" : insertRequest.getWork());
			usuario.setUsrOcupacion(insertRequest.getJob() == null ? "" : insertRequest.getJob());
			usuario.setUsrCargoPublico(insertRequest.getPublicOffice() == null ? "" : insertRequest.getPublicOffice());
			usuario.setUsrInstitucion(insertRequest.getInstitution() == null ? "" : insertRequest.getInstitution());
			usuario.setUsr_jumio("1");
			usuario.setUsrTerminos("0");
			
//			if(StringUtils.isNotEmpty(sessionId)) {
//				validate = validateUser(usuario, sessionId);
//				if("high".equals(validate.getTmxRisk())) {
//					response.setMensajeError("Se ha detectado actividad inusual en la transaccion. No se puede realizar el pago.");
//					response.setIdError(900);
//					return response;
//				}
//			}
			
			if (insertRequest.getCard() != null && insertRequest.getCard().getPan() != null) {
				usuario.setUsrTdcNumero(UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getPan())));
				usuario.setUsrTdcVigencia(
						UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getVigencia())));
				usuario.setUsrTdcCodigo(UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getCodigo())));
			}

			usuario = (TUsuarios) UtilsMC.validarCampos(usuario);
			LOGGER.debug("DATOS DE USUARIO A GUARDAR: " + gson.toJson(usuario));
			String responseDB = mapper.guardarUsuario(usuario);

			LOGGER.debug("[RESPONSE USER INSERT] " + responseDB);

			response = gson.fromJson(responseDB, TUsuarios.class);

			// Enviar email para confirmar cuenta
			/*if (response.getIdError() != 0) {
				loginInsert = response;
				return loginInsert;
			} */

			// insertar tarjeta
			if (response.getIdError() == 0 && insertRequest.getCard() != null) {
				LOGGER.debug("[SE DETECTO TARJETA. INSERTANDO ...]");
				
				boolean validDC = (insertRequest.getCountry() == 2 ? true : false);
				boolean insertcard = true;
				if(validDC){
					DataCreditClient DClient = new DataCreditClient();
					ExperianUserVO user = new ExperianUserVO();
					//{"tipoIdUsuario":"1","idUsuario":"900374820","nitUsuario":"900374820","clave":"50AVJ","tipoIdCliente":"1"}
					
					user.setClave("50AVJ");
					user.setIdAplicacion("1");
					user.setIdUsuario("900374820");
					user.setNitUsuario("900374820");
					user.setRespuesta("");
					user.setTipoIdCliente("1");
					user.setTipoIdUsuario("1");
					LOGGER.debug("Respuesta Restemplate: " + DClient.valCuenta(insertRequest, user));
					boolean res = DClient.valCuentastub(insertRequest, user);
					if(!res){
						insertcard = false;
						LOGGER.debug("No paso validacion datacredit...");
					}
				}
				
				if(insertcard){
					TUsuarios login = gson.fromJson(mapper.loginUsuario(usuario), TUsuarios.class);
					HashMap parameters = new HashMap<>();
					parameters.put("nusuario", login.getIdeUsuario());
					parameters.put("idapp", idApp);
					parameters.put("nnumtarjeta", UtilsMC.getSMS(usuario.getUsrTdcNumero()));
					parameters.put("nnumtarjetacrip", usuario.getUsrTdcNumero());
					parameters.put("nvigencia", usuario.getUsrTdcVigencia());
					parameters.put("nproceso", 1);
					parameters.put("nidtarjetausuario", 1);
					parameters.put("nct", usuario.getUsrTdcCodigo());
					parameters.put("ntarjeta",
							insertRequest.getFirstName().trim() + " " + insertRequest.getLastName().trim());
					parameters.put("domamex", insertRequest.getCard().getDomAmex());
					parameters.put("cpamex", insertRequest.getCard().getCpAmex());
					parameters.put("idioma", idioma);
					parameters.put("mobilecard", 0);
					parameters.put("tipotarjeta", insertRequest.getCard().getTipoTarjeta() == null ? "CREDITO"
							: insertRequest.getCard().getTipoTarjeta().toUpperCase());
	
					String responseAddCard = mapper.ProcessCard(parameters);
					LOGGER.debug("[RESPONSE INSERT CARD] " + responseAddCard);
				}
				/*LoginRequest loginR = new LoginRequest();
				loginR.setIdApp(idApp);
				loginR.setImei(insertRequest.getImei());
				loginR.setLogin(insertRequest.getEmail());
				loginR.setManufacturer(insertRequest.getManufacturer());
				loginR.setOs(insertRequest.getOs());
				loginR.setPassword(insertRequest.getPassword());
				loginR.setUsrLoginOrEmail(insertRequest.getEmail());
				loginR.setUsrPwd(insertRequest.getPassword());
				loginR.setTipoUsuario("USUARIO");*/
				
				 loginInsert =gson.fromJson(mapper.loginUsuario(usuario), TUsuarios.class); // (TUsuarios)login( idApp,  insertRequest.getCountry(),  loginR,  idioma);//
				
				response.setIdUsuario(loginInsert.getIdeUsuario());
				response.setIdPais(insertRequest.getCountry());
			} else {
				if (response.getIdError() == 0) {
					LOGGER.debug("No se detecto tarjeta");
					/*LoginRequest loginR = new LoginRequest();
					loginR.setIdApp(idApp);
					loginR.setImei(insertRequest.getImei());
					loginR.setLogin(insertRequest.getEmail());
					loginR.setManufacturer(insertRequest.getManufacturer());
					loginR.setOs(insertRequest.getOs());
					loginR.setPassword(insertRequest.getPassword());
					loginR.setUsrLoginOrEmail(insertRequest.getEmail());
					loginR.setUsrPwd(insertRequest.getPassword());
					loginR.setTipoUsuario("USUARIO");*/
					
					 loginInsert = gson.fromJson(mapper.loginUsuario(usuario), TUsuarios.class); //(TUsuarios)login( idApp,  insertRequest.getCountry(),  loginR,  idioma); //
					 
					response.setIdUsuario(loginInsert.getIdeUsuario());
					response.setIdPais(insertRequest.getCountry());
					if (insertRequest.getCountry() == 1) {
						response.setIdError(90);
						if (idioma.equals("es"))
							response.setMensajeError("Desea obtener tarjeta Mobilecard");
						else
							response.setMensajeError("Want to get Mobilecard card");
					}
				}
			}
			LOGGER.debug("Retornando respuesta..."+ gson.toJson(response));
			return response;

		} catch (Exception ex) {
			LOGGER.error("Error al insertar usuario", ex);
			ex.printStackTrace();
			response.setIdError(101);
			response.setMensajeError("An error occurred, please try again.");
			return response;
		}

	}
	
	
	public TUsuarios UserInsert_v2Nuevo(Integer idApp, InsertRequest insertRequest, String idioma, String sessionId) {
		TUsuarios response = new TUsuarios();
		TUsuarios loginInsert = new TUsuarios();
		ThreatMetrixResponse validate = null;
		AppCipher appInfo = null;
		try {
			LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO");
			LOGGER.debug("Json: " + gson.toJson(insertRequest));
			
			
			if(emailDisposable.isDisposable(insertRequest.getEmail().split("@")[1])){
				response.setIdError(2000);
				if(idioma.equalsIgnoreCase("es"))
					response.setMensajeError("Tu correo es desechable, para crear un usuario deberas ingresar un correo válido");
				else
					response.setMensajeError("Your email is disposable, to create a user you must enter a valid email");
				return response;
			}
			appInfo = mapper.getAppCipher(idApp); 
			
			
			String pass_decrypt=null;
			TUsuarios usuario = new TUsuarios();
			usuario.seteMail(insertRequest.getEmail());
			
			//pass_decrypt = idApp > 3 ? Business.decryptApp(appInfo.getApi_key(),insertRequest.getPassword()) :  AddcelCrypto.decryptHard(insertRequest.getPassword());

			if(idApp <= 3)
				pass_decrypt = AddcelCrypto.decryptHard(insertRequest.getPassword());
			else
				if(idApp == 4)
					pass_decrypt = insertRequest.getPassword();
				else
					if(idApp > 4 )
						pass_decrypt = Business.decryptApp(appInfo.getApi_key(),insertRequest.getPassword());
		
			String passmx = UtilsMC.generaSemillaAux(pass_decrypt);
			String pass_AES = Crypto.aesEncrypt(passmx, pass_decrypt);

			usuario.setUsrLogin(insertRequest.getEmail());
			usuario.setUsrPwd(pass_AES);
			usuario.setUsrNombre(insertRequest.getFirstName());
			usuario.setUsrApellido(insertRequest.getLastName());
			usuario.setUsrTelefono(insertRequest.getPhone());
			usuario.setIdPais(insertRequest.getCountry());
			usuario.setImei(insertRequest.getImei());
			usuario.setSoftware(insertRequest.getOs());
			usuario.setModelo(insertRequest.getPlatform());
			usuario.setIdUsrStatus(idApp == 1 ? 1 : idApp > 3 ? 1 : 100);
			usuario.setIdioma(idioma);
			usuario.setCedula(insertRequest.getCedula() == null ? "" : insertRequest.getCedula());
			usuario.setTipoCedula(insertRequest.getTipoCedula() == null ? 0 : insertRequest.getTipoCedula());
			usuario.setProveedor(insertRequest.getProveedor());
			usuario.setIdApp(idApp);
			usuario.setTipo(insertRequest.getManufacturer());
			usuario.setUsrNacionalidad(insertRequest.getNacionalidad() == null ? "" : insertRequest.getNacionalidad());
			usuario.setUsrDistrito(insertRequest.getDistrict() == null ? "" : insertRequest.getDistrict());
			usuario.setUsrProvincia(insertRequest.getProvince() == null ? "" : insertRequest.getProvince());
			usuario.setUsrDepartamento(insertRequest.getDepartment() == null ? "" : insertRequest.getDepartment());
			usuario.setUsrCentroLaboral(insertRequest.getWork() == null ? "" : insertRequest.getWork());
			usuario.setUsrOcupacion(insertRequest.getJob() == null ? "" : insertRequest.getJob());
			usuario.setUsrCargoPublico(insertRequest.getPublicOffice() == null ? "" : insertRequest.getPublicOffice());
			usuario.setUsrInstitucion(insertRequest.getInstitution() == null ? "" : insertRequest.getInstitution());
			usuario.setUsr_jumio("1");
			usuario.setUsrTerminos("1");
//			if(StringUtils.isNotEmpty(sessionId)) {
//				validate = validateUser(usuario, sessionId);
//				if("high".equals(validate.getTmxRisk())) {
//					response.setMensajeError("Se ha detectado actividad inusual en la transaccion. No se puede realizar el pago.");
//					response.setIdError(900);
//					return response;
//				}
//			}
			
			if (insertRequest.getCard() != null && insertRequest.getCard().getPan() != null) {
				usuario.setUsrTdcNumero(UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getPan())));
				usuario.setUsrTdcVigencia(
						UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getVigencia())));
				usuario.setUsrTdcCodigo(UtilsMC.setSMS(AddcelCrypto.decryptHard(insertRequest.getCard().getCodigo())));
			}

			usuario = (TUsuarios) UtilsMC.validarCampos(usuario);
			LOGGER.debug("DATOS DE USUARIO A GUARDAR: " + gson.toJson(usuario));
			String responseDB = mapper.guardarUsuario(usuario);

			LOGGER.debug("[RESPONSE USER INSERT] " + responseDB);

			response = gson.fromJson(responseDB, TUsuarios.class);

			// Enviar email para confirmar cuenta
			if (response.getIdError() != 0) {
				loginInsert = response;
				return loginInsert;
			} 

			// insertar tarjeta
			if (response.getIdError() == 0 && insertRequest.getCard() != null) {
				LOGGER.debug("[SE DETECTO TARJETA. INSERTANDO ...]");
				
				boolean validDC = (insertRequest.getCountry() == 2 ? true : false);
				boolean insertcard = true;
				if(validDC){
					DataCreditClient DClient = new DataCreditClient();
					ExperianUserVO user = new ExperianUserVO();
					//{"tipoIdUsuario":"1","idUsuario":"900374820","nitUsuario":"900374820","clave":"50AVJ","tipoIdCliente":"1"}
					
					user.setClave("50AVJ");
					user.setIdAplicacion("1");
					user.setIdUsuario("900374820");
					user.setNitUsuario("900374820");
					user.setRespuesta("");
					user.setTipoIdCliente("1");
					user.setTipoIdUsuario("1");
					LOGGER.debug("Respuesta Restemplate: " + DClient.valCuenta(insertRequest, user));
					boolean res = DClient.valCuentastub(insertRequest, user);
					if(!res){
						insertcard = false;
						LOGGER.debug("No paso validacion datacredit...");
					}
				}
				
				if(insertcard){
					TUsuarios login = gson.fromJson(mapper.loginUsuario(usuario), TUsuarios.class);
					HashMap parameters = new HashMap<>();
					parameters.put("nusuario", login.getIdeUsuario());
					parameters.put("idapp", idApp);
					parameters.put("nnumtarjeta", UtilsMC.getSMS(usuario.getUsrTdcNumero()));
					parameters.put("nnumtarjetacrip", usuario.getUsrTdcNumero());
					parameters.put("nvigencia", usuario.getUsrTdcVigencia());
					parameters.put("nproceso", 1);
					parameters.put("nidtarjetausuario", 1);
					parameters.put("nct", usuario.getUsrTdcCodigo());
					parameters.put("ntarjeta",
							insertRequest.getFirstName().trim() + " " + insertRequest.getLastName().trim());
					parameters.put("domamex", insertRequest.getCard().getDomAmex());
					parameters.put("cpamex", insertRequest.getCard().getCpAmex());
					parameters.put("idioma", idioma);
					parameters.put("mobilecard", 0);
					parameters.put("tipotarjeta", insertRequest.getCard().getTipoTarjeta() == null ? "CREDITO"
							: insertRequest.getCard().getTipoTarjeta().toUpperCase());
	
					String responseAddCard = mapper.ProcessCard(parameters);
					LOGGER.debug("[RESPONSE INSERT CARD] " + responseAddCard);
				}
				LoginRequest loginR = new LoginRequest();
				loginR.setIdApp(idApp);
				loginR.setImei(insertRequest.getImei());
				loginR.setLogin(insertRequest.getEmail());
				loginR.setManufacturer(insertRequest.getManufacturer());
				loginR.setOs(insertRequest.getOs());
				loginR.setPassword(insertRequest.getPassword());
				loginR.setUsrLoginOrEmail(insertRequest.getEmail());
				loginR.setUsrPwd(insertRequest.getPassword());
				loginR.setTipoUsuario("USUARIO");
				
				 loginInsert = (TUsuarios)login( idApp,  insertRequest.getCountry(),  loginR,  idioma);//gson.fromJson(mapper.loginUsuario(usuario), TUsuarios.class);
				
				response.setIdUsuario(loginInsert.getIdeUsuario());
				response.setIdPais(insertRequest.getCountry());
			} else {
				if (response.getIdError() == 0) {
					LOGGER.debug("No se detecto tarjeta");
					LoginRequest loginR = new LoginRequest();
					loginR.setIdApp(idApp);
					loginR.setImei(insertRequest.getImei());
					loginR.setLogin(insertRequest.getEmail());
					loginR.setManufacturer(insertRequest.getManufacturer());
					loginR.setOs(insertRequest.getOs());
					loginR.setPassword(insertRequest.getPassword());
					loginR.setUsrLoginOrEmail(insertRequest.getEmail());
					loginR.setUsrPwd(insertRequest.getPassword());
					loginR.setTipoUsuario("USUARIO");
					
					 loginInsert = (TUsuarios)login( idApp,  insertRequest.getCountry(),  loginR,  idioma); //gson.fromJson(mapper.loginUsuario(usuario), TUsuarios.class);
					 
					response.setIdUsuario(loginInsert.getIdeUsuario());
					response.setIdPais(insertRequest.getCountry());
					if (insertRequest.getCountry() == 1) {
						response.setIdError(90);
						if (idioma.equals("es"))
							response.setMensajeError("Desea obtener tarjeta Mobilecard");
						else
							response.setMensajeError("Want to get Mobilecard card");
					}
				}
			}
			LOGGER.debug("Retornando respuesta...");
			return loginInsert;

		} catch (Exception ex) {
			LOGGER.error("Error al insertar usuario", ex);
			ex.printStackTrace();
			response.setIdError(101);
			response.setMensajeError("An error occurred, please try again.");
			return loginInsert;
		}

	}
	
	

	
	public Object addUserOrComercio(Integer idApp, Object obj, String idioma, String sessionId) {
		TUsuarios responseUser = null;
		Establecimiento responseCommerce = null;
		try {
			LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO / COMERCIO");
			LOGGER.debug("OBJETO REQUEST: "+ gson.toJson(obj));
			//String js = gson.toJson(obj);
			//LOGGER.debug("Json: " + js.su);
			LOGGER.debug("tipo objeto: " + obj.getClass().getName() + " " + obj.getClass().getCanonicalName());
			LOGGER.debug("isInserRequest: "+ obj.getClass().isInstance(InsertRequest.class));
			InsertRequest aux = gson.fromJson( gson.toJson(obj), InsertRequest.class); 
			if(aux != null && aux.getEmail() != null && !aux.getEmail().isEmpty()) { //obj.getClass().isInstance(InsertRequest.class)
				responseUser = UserInsert(idApp, aux, idioma, sessionId);
				LOGGER.debug("Retornando respuesta - {}", gson.toJson(responseUser));
				return responseUser;
			} else {
				Establecimiento est = gson.fromJson(gson.toJson(obj),Establecimiento.class);
				LOGGER.debug("ALTA NEGOCIO: "+ gson.toJson(est));
				if( (est.getCuenta_clabe() == null || est.getCuenta_clabe().isEmpty()) && est.getCci() != null && !est.getCci().isEmpty()  )
					est.setCuenta_clabe(est.getCci());
				
				if( (est.getCci() == null || est.getCci().isEmpty()) && est.getCuenta_clabe() != null && !est.getCuenta_clabe().isEmpty())
					est.setCci(est.getCuenta_clabe());
				
				if( (est.getT_previvale() == null || est.getT_previvale().isEmpty()) && (est.getCuenta_clabe() == null || est.getCuenta_clabe().isEmpty()) ){
					responseCommerce = new Establecimiento();
					responseCommerce.setIdError(3000);
					if(idioma.equals("es"))
						responseCommerce.setMensajeError("Ingrese una cuenta clabe o tarjeta fisica");
					else
						responseCommerce.setMensajeError("Enter a clabe account or physical card");
					return responseCommerce;
				}
				
				
				if(StringUtils.isNotEmpty(est.getT_previvale())) {
					int count = mapper.validaTarjetaPrevivale(est.getT_previvale());
					if(count == 0) {
						responseCommerce = new Establecimiento();
						responseCommerce.setIdError(-1900);
						responseCommerce.setMensajeError("La tarjeta ingresada no es valida.");
						return responseCommerce;
					} 
				}
				responseCommerce = AddCommerce(est, idApp, idioma,1);
				
			/*	LoginRequest loginR = new LoginRequest();
				loginR.setIdApp(idApp);
				loginR.setImei("");
				loginR.setLogin(est.getUsuario());
				loginR.setManufacturer("");
				loginR.setOs("");
				loginR.setPassword(est.getPass());
				loginR.setUsrLoginOrEmail(est.getUsuario());
				loginR.setUsrPwd(est.getPass());
				loginR.setTipoUsuario("NEGOCIO");
				LoginCommerce  resp = null;*/
				
				/*if(responseCommerce.getIdError() == 0) {
					  resp =	(LoginCommerce)login( idApp,  est.getIdPais(),  loginR,  idioma);
				}else {
					resp = gson.fromJson(gson.toJson(responseCommerce), LoginCommerce.class);
				}*/
				
				LOGGER.debug("Retornando respuesta insert- {}", gson.toJson(responseCommerce));
				//LOGGER.debug("Retornando respuesta login- {}", gson.toJson(resp));
				return responseCommerce; //resp;//
			}
		} catch (Exception ex) {
			LOGGER.error("Error al insertar usuario", ex);
			AbstractVO error = new AbstractVO();
			ex.printStackTrace();
			error.setIdError(101);
			error.setMensajeError("An error occurred, please try again.");
			return error;
		}
	}
	
	
	public Object addUserOrComercioV2Nuevo(Integer idApp, Object obj, String idioma, String sessionId) {
		TUsuarios responseUser = null;
		Establecimiento responseCommerce = null;
		try {
			LOGGER.debug("INICIANDO PROCESO DE ALTA USUARIO / COMERCIO");
			LOGGER.debug("OBJETO REQUEST: "+ gson.toJson(obj));
			//String js = gson.toJson(obj);
			//LOGGER.debug("Json: " + js.su);
			LOGGER.debug("tipo objeto: " + obj.getClass().getName() + " " + obj.getClass().getCanonicalName());
			LOGGER.debug("isInserRequest: "+ obj.getClass().isInstance(InsertRequest.class));
			InsertRequest aux = gson.fromJson( gson.toJson(obj), InsertRequest.class);
			if(aux != null && aux.getEmail() != null && !aux.getEmail().isEmpty()) { //obj.getClass().isInstance(InsertRequest.class)
				if(aux.getCountry() == mapper.isPeru()) {
					throw new Exception();
				}else {
					responseUser = UserInsert(idApp, (InsertRequest) obj, idioma, sessionId);
					LOGGER.debug("Retornando respuesta - {}", gson.toJson(responseUser));
					return responseUser;
				}
			} else {
				Establecimiento est = gson.fromJson(gson.toJson(obj),Establecimiento.class);
				LOGGER.debug("ALTA NEGOCIO: "+ gson.toJson(est));
				if( (est.getCuenta_clabe() == null || est.getCuenta_clabe().isEmpty()) && est.getCci() != null && !est.getCci().isEmpty()  )
					est.setCuenta_clabe(est.getCci());
				
				if( (est.getCci() == null || est.getCci().isEmpty()) && est.getCuenta_clabe() != null && !est.getCuenta_clabe().isEmpty())
					est.setCci(est.getCuenta_clabe());
				
				if( (est.getT_previvale() == null || est.getT_previvale().isEmpty()) && (est.getCuenta_clabe() == null || est.getCuenta_clabe().isEmpty()) ){
					responseCommerce = new Establecimiento();
					responseCommerce.setIdError(3000);
					if(idioma.equals("es"))
						responseCommerce.setMensajeError("Ingrese una cuenta clabe o tarjeta fisica");
					else
						responseCommerce.setMensajeError("Enter a clabe account or physical card");
					return responseCommerce;
				}
				
				
				if(StringUtils.isNotEmpty(est.getT_previvale())) {
					int count = mapper.validaTarjetaPrevivale(est.getT_previvale());
					if(count == 0) {
						responseCommerce = new Establecimiento();
						responseCommerce.setIdError(-1900);
						responseCommerce.setMensajeError("La tarjeta ingresada no es valida.");
						return responseCommerce;
					} 
				}
				
				//Valida el pais para el registro
				if(est.getIdPais() == mapper.isPeru()) {
					throw new Exception("");
				}else {
					responseCommerce = AddCommerce(est, idApp, idioma,2);
					
					LoginRequest loginR = new LoginRequest();
					loginR.setIdApp(idApp);
					loginR.setImei("");
					loginR.setLogin(est.getUsuario());
					loginR.setManufacturer("");
					loginR.setOs("");
					loginR.setPassword(est.getPass());
					loginR.setUsrLoginOrEmail(est.getUsuario());
					loginR.setUsrPwd(est.getPass());
					loginR.setTipoUsuario("NEGOCIO");
					LoginCommerce  resp = null;
					
					if(responseCommerce.getIdError() == 0) {
						  resp =	(LoginCommerce)login( idApp,  est.getIdPais(),  loginR,  idioma);
					}else {
						resp = gson.fromJson(gson.toJson(responseCommerce), LoginCommerce.class);
					}
					
					LOGGER.debug("Retornando respuesta insert- {}", gson.toJson(responseCommerce));
					LOGGER.debug("Retornando respuesta login- {}", gson.toJson(resp));
					return resp;//responseCommerce;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error al insertar usuario", ex);
			AbstractVO error = new AbstractVO();
			ex.printStackTrace();
			error.setIdError(101);
			error.setMensajeError("An error occurred, please try again.");
			return error;
		}
	}
	
	
	
	public ModelAndView userActivate(Integer idApp, String codigo, String idioma) {
		ModelAndView view = new ModelAndView();
		LOGGER.debug("[Activando usuario ] " + codigo);
		try {
			String[] data = new String(Base64.decode(codigo)).split(":");
			String user = AddcelCrypto.decryptHard(data[0]);
			String pass = AddcelCrypto.decryptHard(data[1]);
			LOGGER.debug("user = " + user);
			LOGGER.debug("pass = " + pass);
			// mapper.userActivate(user, pass);

			//String pass_decrypt = AddcelCrypto.decryptHard(pass);
			//String passmx = UtilsMC.generaSemillaAux(pass_decrypt);
			String pass_AES = pass;//Crypto.aesEncrypt(passmx, pass_decrypt);
			
			/*String responseJSON = mapper.userActivate(user, pass, idioma, idApp);
			BaseResponse responseDB = gson.fromJson(responseJSON, BaseResponse.class);*/
			mapper.updateConfirmEmail(user, pass_AES, "1", idApp);

			view.setViewName("activate");

			if(idioma.equals("es"))
				view.addObject("message", "Su Cuenta de correo ha sido verificada exitosamente");
			else
				view.addObject("message", "Su Cuenta de correo ha sido verificada exitosamente");
			
			return view;
		} catch (Exception ex) {
			LOGGER.error("[Error al activar usuario] ", ex);
			view.setViewName("activate_error");
			view.addObject("message", "Error al activar cuenta");
			return view;
		}
	}
	
	
	public ModelAndView userCommerceActivate(Integer idApp, Integer idPais,String tipoUsuario, String codigo, String idioma) {
		ModelAndView view = new ModelAndView();
		LOGGER.debug("[Activando usuario ] {} {}" , codigo, tipoUsuario);
		try {
			String[] data = new String(Base64.decode(codigo)).split(":");
			String user = AddcelCrypto.decryptHard(data[0]);
			String id = AddcelCrypto.decryptHard(data[1]);
			LOGGER.debug("user = " + user);
			LOGGER.debug("pass = " + id);
			long idUser = Long.parseLong(id);
			
			if(tipoUsuario.toUpperCase().equals("USUARIO"))
				mapper.preRegistro(idUser, idApp, idPais, tipoUsuario, idioma, 2, "","","");
			else if(tipoUsuario.toUpperCase().equals("COMERCIO"))
				mapper.preRegistro(idUser, idApp, idPais, tipoUsuario, idioma, 4, "","","");

			view.setViewName("activate");

			if(idioma.equals("es"))
				view.addObject("message", "Tu cuenta de correo ha sido habilitada, regresa a la aplicacion y presiona continuar para finalizar tu registro");
			else
				view.addObject("message", "Your email account has been enabled, return to the application and press continue to finish your registration");
			
			return view;
		} catch (Exception ex) {
			LOGGER.error("[Error al activar usuario] ", ex);
			view.setViewName("activate");
			view.addObject("message", "Error al activar cuenta");
			return view;
		}
	}

	public BaseResponse ActivationSendMail(Integer idApp, String idioma, long idUsuario) {

		BaseResponse response = new BaseResponse();
		response.setIdError(0);
		TUsuarios user = null;
		try {
			user = mapper.getUserInfo(idUsuario, idApp);
			// Enviar email para confirmar cuenta
			if (user.geteMail() != null && !user.geteMail().isEmpty()) {
				String value = AddcelCrypto.encryptHard(user.getUsrLogin()) + ":"
						+ AddcelCrypto.encryptHard(user.getUsrPwd());
				String param = "?codigo=" + Base64.encode(value.getBytes());
				String url = Constantes.ACTIVATION_URL.replace("{idioma}", idioma);
				url = url.replace("{idApp}", String.valueOf(idApp)) + param;
				LOGGER.debug("[ACTIVATE URL] " + url);
				TemplateCorreo correo = mapper.getTemplateMail("@MENSAJE_ACTIVATE", idioma, idApp);
				UtilsMC.emailSend(user.geteMail(), correo.getBody(),
						correo.getAsunto(), user.getUsrLogin(), url, "",
						user.getUsrNombre() + " " + user.getUsrApellido(),
						new String[] { Constantes.Path_images + "user_registry.PNG" });
			}

		} catch (Exception ex) {
			LOGGER.error("[ERROR AL ENVIAR CORREO DE ACTIVACION ] " + idUsuario, ex);
			response.setIdError(101);
			if (idioma.equalsIgnoreCase("es"))
				response.setMensajeError("Error inesperado, intente de nuevo");
			else
				response.setMensajeError("Unexpected error, try again");
		}

		if (idioma.equalsIgnoreCase("es"))
			response.setMensajeError("Se envió un correo para confirmar su cuenta");
		else
			response.setMensajeError("An email was sent to confirm your account");

		return response;

	}


	public TUsuarios userLogin(Integer idApp, LoginRequest request, String idioma) {
		LOGGER.debug("[INICIANDO LOGIN USUARIO] " + gson.toJson(request));
		TUsuarios user = new TUsuarios();
		AppCipher appInfo = null;
		try {
			appInfo = mapper.getAppCipher(idApp);
			
			String passdecrypt = idApp > 3 ? Business.decryptApp(appInfo.getApi_key(), request.getUsrPwd()) : AddcelCrypto.decryptHard(request.getUsrPwd());
			String passmx = UtilsMC.generaSemillaAux(passdecrypt);
			user.setUsrLogin(request.getUsrLoginOrEmail());
			user.setUsrPwd(Crypto.aesEncrypt(passmx, passdecrypt));
			user.setIdioma(idioma);
			user.setIdApp(idApp);

			String responseJson = mapper.loginUsuario(user);

			user = gson.fromJson(responseJson, TUsuarios.class);

			if (user.getIdError() == 0) {
				if (user.getUsrTdcNumero() != null && !user.getUsrTdcNumero().isEmpty()) {
					String card = UtilsMC.getSMS(user.getUsrTdcNumero());
					user.setUsrTdcNumero(UtilsMC.maskCard(card, "*"));
				}
				user.setUsrPwd("");
				
				TUsuarios taux = mapper.getUserInfo(user.getId(), idApp);
				if(taux.getIdUsrStatus() == 99 || taux.getIdUsrStatus()  == 100)
					if(!taux.getUsr_email().equals("1") || !taux.getUsr_sms().equals("1")){
						user.setIdError(3000);
						if(idioma.equals("es"))
							user.setMensajeError("Active su cuenta vía correo y código SMS, los cuales fueron enviados al registrase");
						else
							user.setMensajeError("Activate your account via email and SMS code, which were sent when you registered");
					}
				
				LOGGER.debug("ESTATUS JUMIO " + user.getUsr_jumio());
				if(user.getUsr_jumio() != null && user.getUsr_jumio().equals("2")){
					JumioVerifyRes jum = jumioClient.verifcarReferencia(user.getScanReference(),idApp,1,idioma);
					if(jum != null){
						user.setIdError(jum.getCode());
						user.setMensajeError(jum.getMessage());
						user.setUsr_jumio(jum.getJumioStatus()+"");
						if(jum.getJumioStatus() == 2)
							if(idioma.equals("es"))
								user.setMensajeError("Para poder ingresar a tu aplicación recibiras un sms y un correo, en un máximo de 5 minutos. Tus documentos están siendo validados.");
							else
								user.setMensajeError("In order to enter your application you will receive an SMS and an email, in a maximum of 5 minutes. Your documents are being validated.");
					}
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.debug("[ERROR AL LOGUEAR USUARIO] ", ex);
			user.setIdError(101);
			if (idioma.equalsIgnoreCase("es"))
				user.setMensajeError("Error inesperado, intente de nuevo");
			else
				user.setMensajeError("Unexpected error, try again");
		}
		LOGGER.debug("[FINALIZANDO LOGIN USUARIO] {}", gson.toJson(user));
		return user;
	}

	public Object login(Integer idApp, Integer idPais, LoginRequest request, String idioma) {
		LOGGER.debug("[INICIANDO LOGIN USUARIO]_V2 " + gson.toJson(request));
		TUsuarios user = new TUsuarios();
		AppCipher appInfo = null;
		Establecimiento establecimiento = null;
		try {
				
			appInfo = mapper.getAppCipher(idApp);
			
			if(!"NEGOCIO".equals(request.getTipoUsuario())) {
				//String passdecrypt = idApp > 3 ? Business.decryptApp(appInfo.getApi_key(), request.getUsrPwd()) : AddcelCrypto.decryptHard(request.getUsrPwd());
				String passdecrypt = null;
				if(idApp <= 3)
					passdecrypt = AddcelCrypto.decryptHard(request.getUsrPwd());
				else
					if(idApp == 4)
						passdecrypt = request.getUsrPwd();
					else
					   if(idApp > 4 )
						   passdecrypt = Business.decryptApp(appInfo.getApi_key(),request.getUsrPwd());
				String passmx = UtilsMC.generaSemillaAux(passdecrypt);
				
				user.setUsrPwd(Crypto.aesEncrypt(passmx, passdecrypt));
			} else {
				user.setUsrPwd(request.getUsrPwd());
			}
			user.setUsrLogin(request.getUsrLoginOrEmail());
			user.setIdioma(idioma);
			user.setIdApp(idApp);
			user.setTipoUsuario(request.getTipoUsuario());
			String responseJson = mapper.login(user);
			LOGGER.info("RESPONSE FROM DB: {}", responseJson.replaceAll("\"photo\":\"((([^\"]){10}).*)\",", "\"photo\":\"$2\","));
			if("NEGOCIO".equals(request.getTipoUsuario())) {
				establecimiento = gson.fromJson(responseJson, Establecimiento.class);
				BaseResponse msj = gson.fromJson(responseJson, BaseResponse.class);
				establecimiento.setNegocio(true);
				LOGGER.info("RESPUESTA DE LOGIN: {COMERCIO}", gson.toJson(establecimiento));
				LoginCommerce commerce = new LoginCommerce();
				commerce.setComisionFija(establecimiento.getComision_fija());
				commerce.setComisionPorcentaje(establecimiento.getComision_porcentaje());
				commerce.setCuentaClabe(establecimiento.getCuenta_clabe());
				commerce.setDireccionEstablecimiento(establecimiento.getDireccion_establecimiento());
				commerce.setEmail(establecimiento.getEmail_contacto());
				commerce.setIdAccount(establecimiento.getId_account());
				commerce.setIdBanco(establecimiento.getId_banco());
				commerce.setIdError(Long.parseLong(msj.getIdError()+""));
				commerce.setMensajeError(msj.getMensajeError());
				commerce.setIdEstablecimiento(establecimiento.getId());
				commerce.setIdUsrStatus(Long.parseLong(establecimiento.getEstatus()+""));
				commerce.setNegocio(true);
				commerce.setNombreEstablecimiento(establecimiento.getNombre_establecimiento());
				commerce.setNombrePropCuenta(establecimiento.getNombre_prop_cuenta());
				commerce.setRazonSocial(establecimiento.getRazon_social());
				commerce.setRepresentanteLegal(establecimiento.getRepresentante_legal());
				commerce.setTelefono(establecimiento.getTelefono_contacto());
				commerce.setUrlLogo(establecimiento.getUrlLogo());
				commerce.setJumioStatus(establecimiento.getJumio_status());
				commerce.setScanReference(establecimiento.getJumio_reference());
				commerce.setIdPais(establecimiento.getIdPais());
				if(commerce == null || commerce.getIdPais() != 4 )
					commerce.setIdPais(1);
				commerce.setQrTag(establecimiento.getQrTag());
				commerce.setQrBase64(establecimiento.getQrBase64());
				commerce.setTipo_persona(establecimiento.getTipo_persona());
				CardRequest card = getEstablecimientoCard(commerce.getIdEstablecimiento());
				if(card != null) {
					commerce.setCard(card);
				}
				
				quitNulls(commerce);
				
				if(establecimiento.getIdPais() != 0 && establecimiento.getIdPais() != idPais) {
					commerce.setIdUsrStatus(401l);
					commerce.setIdError(401l);
					commerce.setMensajeError("La cuenta con la que estas accediendo no pertenece al país seleccionado");
					
				}
				
			    //ACTIUALIZAR QR COMERCIO MEXICO EN CASO DE NO TENER
				try {
					if(commerce.getIdPais() == 1)
						if(mapper.hasStaticQr(establecimiento.getId(), commerce.getIdPais(), idApp) == 0) {
							updateQRComercio(establecimiento.getId(), idApp, commerce.getIdPais(), idioma, "");
							LOGGER.debug("QR ESTATICO ACTUALIZADO DESDE LOGIN COMERCIO {}", establecimiento.getId());
						}
				}catch(Exception ex) {
					LOGGER.debug("NO SE PUDO ACTUALIZAR QR ESTATICO COMERcio {}",establecimiento.getId());
				}
				
				
				if(commerce.getJumioStatus().equals("2"))
				{
					commerce.setIdError(100l);
					if(idioma.equals("es"))
						commerce.setMensajeError("Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard");
					else
						commerce.setMensajeError("Your documentation is being analyzed, in a moment you will be ready to access MobileCard");
				}
				
				return commerce;
			} else {
				
				user.setNegocio(false);
				user = gson.fromJson(responseJson, TUsuarios.class);
				
				try {
					user.setIdPais(idPais);
					mapper.updateUserCountry(user.getIdeUsuario(), idPais);
					LOGGER.debug("PAIS ACTUALIZADO  {} {}", user.getIdeUsuario(),idPais);
				}catch(Exception ex) {
					LOGGER.debug("NO FUE POSIBLE ACTUALIZAR EL PAIS {} {}", user.getIdeUsuario(),idPais);
				}
				
				if (user.getIdError() == 0) {
					if (user.getUsrTdcNumero() != null && !user.getUsrTdcNumero().isEmpty()) {
						String card = UtilsMC.getSMS(user.getUsrTdcNumero());
						user.setUsrTdcNumero(UtilsMC.maskCard(card, "*"));
					}
					user.setUsrPwd("");
					
					//******obteniendo img profile*******/
					ImgProfile img = mapper.getUserPhoto(user.getIdeUsuario());
					if(img == null || img.getImg() == null){
						user.setImg("");
					}else{
						user.setImg(new String(img.getImg()));
					}
					
					/**********checando si esta en el whitelist de promociones************/
					if(user.getIdError() == 0)
						verificaPromo(user.getIdeUsuario(), user.getIdUsrStatus(), user.getUsr_jumio(), idPais, idApp, idioma);
					
					
				}
				try {
					LOGGER.info("VALIDANDO ESTATUS DEL USUARIO {}", user.getIdUsrStatus());
					/*if(idApp < 3)
						if(100 == user.getIdUsrStatus()) {
							resendSMS(idApp, idPais, idioma, "usuario", user.getIdeUsuario());
							LOGGER.info("RE ENVIANDO MSM AL USUARIO {} - ", user.getIdeUsuario());
						}*/
					LOGGER.debug("ESTATUS JUMIO " + user.getUsr_jumio());
					/*if(user.getUsr_jumio() != null && !user.getUsr_jumio().equals("1")){
						JumioVerifyRes jum = jumioClient.verifcarReferencia(user.getScanReference(),idApp,idPais,idioma);
						if(jum != null){
							user.setIdError(jum.getCode());
							user.setMensajeError(jum.getMessage());
							user.setUsr_jumio(jum.getJumioStatus()+"");
						}
					}*/
					
					if(user.getUsr_jumio() != null && user.getUsr_jumio().equals("2")){
						user.setIdError(100);
						if(idioma.equals("es"))
							user.setMensajeError("Tu documentación esta siendo analizada, en un momento mas estarás listo para acceder a MobileCard");
						else
							user.setMensajeError("Your documentation is being analyzed, in a moment you will be ready to access MobileCard");
					}
					
					if(user.getIdUsrStatus() == 99){
						//GOOD BYE IRON MAN :( 
						//user.setIdError(3000);
						if(idioma.equals("es"))
							user.setMensajeError("Active su cuenta vía correo y código SMS, los cuales fueron enviados al registrase");
						else
							user.setMensajeError("Activate your account via email and SMS code, which were sent when you registered");
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				LOGGER.info("RESPUESTA DE LOGIN: {}", gson.toJson(user).replaceAll("\"photo\":\"((([^\"]){10}).*)\",", "\"photo\":\"$2\","));
				return user;
			}

		} catch (Exception ex) {
			LOGGER.debug("[ERROR AL LOGUEAR USUARIO] ", ex);
			user.setIdError(101);
			if (idioma.equalsIgnoreCase("es"))
				user.setMensajeError("Error inesperado, intente de nuevo");
			else
				user.setMensajeError("Unexpected error, try again");
		}
		LOGGER.debug("[FINALIZANDO LOGIN USUARIO] ");
		return user;
	}
	
	private void verificaPromo(long idUsuario, int estatus, String estatusJum,int idPais,int idApp, String idioma) {
		try {
			String band = mapper.getParameter("@PROMOTAG");
			if( band!= null && band.equals("1")) {
					if(idPais == 1 && estatusJum.equals("1") && estatus == 1) {
						int iswhitelist = mapper.iswhitelistuser(idUsuario);
						LOGGER.debug("CHECANDO WHITELISTPROMO {} {}",idUsuario, idPais);
						if(iswhitelist > 0) {
							LOGGER.debug("USUARIO ENCONTRADO EN WHITELIST PROMO...");
								if(mapper.hasPrevivale(idUsuario) == 0) {
									LOGGER.debug("EL USUARIO NO TIENE TARJETA PREVIVALE, SE PROCEDE A ASIGNARLE UNA");
										PreviValeService prevService = new PreviValeService();
										ResponseSMS respPrevi = prevService.registrarUsuarioSinTarjeta(idUsuario, idApp, idPais, idioma);
										if(respPrevi != null && respPrevi.getCode()==1000) {
											LOGGER.debug("TARJETA PREVIVALE ASIGANDA CON EXITO... {} ",idUsuario );
											LOGGER.debug("SE PROCEDE A OCULTAR SUS TARJETAS ...");
											mapper.DesactivatecardPromoAct(idUsuario, idApp);
											mapper.DesactivatecardPromoDes(idUsuario, idApp);
											LOGGER.debug("TARJETAS DESACTIVADAS....");
										}else
											LOGGER.debug("OCURRIO UN ERROR AL ASIGANR TARJETA PREVIVALE {}",idUsuario);
											if(mapper.hasPrevivale(idUsuario) == 1) {
												LOGGER.debug("SE OCULTARAN TARJETAS DESPUES DE ERROR, SI TIENE PREVIVALE");
												mapper.DesactivatecardPromoAct(idUsuario, idApp);
												mapper.DesactivatecardPromoDes(idUsuario, idApp);
												LOGGER.debug("TARJETAS DESACTIVADAS....");
											}
								}else {
									// ya cueenta con tarjetas previvale
									LOGGER.debug("EL USUARIO YA CUENTA CON TARJETA PREVIVALE, SE PROCEDE A OCULTAR LAS DEMAS");
									mapper.DesactivatecardPromoAct(idUsuario, idApp);
									mapper.DesactivatecardPromoDes(idUsuario, idApp);
									LOGGER.debug("TARJETAS DESACTIVADAS....");
								}
						}
					}
			}else {
				LOGGER.debug("PROMOTAG DESACTIVADA");
			}
			
		}catch(Exception ex) {
			LOGGER.error("ERROR AL REVISAR WHITELIST PROMO", ex);
		}
	}

	private void quitNulls(LoginCommerce commerce){
		try{
			if(commerce.getNegocio() == null)
				commerce.setNegocio(false);
			if(commerce.getCard() == null)
				commerce.setCard(new CardRequest());
			if(commerce.getCard().getCodigo() == null)
				commerce.getCard().setCodigo("");
			if(commerce.getCard().getCpAmex() == null)
				commerce.getCard().setCpAmex("");
			if(commerce.getCard().getDomAmex() == null)
				commerce.getCard().setDomAmex("");
			if(commerce.getCard().getNombre() == null)
				commerce.getCard().setNombre("");
			if(commerce.getCard().getPan() == null)
				commerce.getCard().setPan("");
			if(commerce.getCard().getRepresentanteLegal() == null)
				commerce.getCard().setRepresentanteLegal("");
			if(commerce.getCard().getTipoTarjeta() == null)
				commerce.getCard().setTipoTarjeta("");
			if(commerce.getCard().getVigencia() == null)
				commerce.getCard().setVigencia("");
				
			if(commerce.getComisionFija() == null)
				commerce.setComisionFija(0.0);
			if(commerce.getComisionPorcentaje() == null)
				commerce.setComisionPorcentaje(0.0);
			if(commerce.getCuentaClabe() == null)
				commerce.setCuentaClabe("");
			if(commerce.getDireccionEstablecimiento() == null)
				commerce.setDireccionEstablecimiento("");
			if(commerce.getEmail() == null)
				commerce.setEmail("");
			if(commerce.getIdAccount() == null)
				commerce.setIdAccount(0l);
			if(commerce.getIdBanco() == null)
				commerce.setIdBanco("");
			if(commerce.getIdEstablecimiento() == null)
				commerce.setIdEstablecimiento(0l);
			if(commerce.getIdPais() == null)
				commerce.setIdPais(0);
			if(commerce.getIdUsrStatus() == null)
				commerce.setIdUsrStatus(0l);
			if(commerce.getJumioStatus() == null)
				commerce.setJumioStatus("");
			if(commerce.getNombreEstablecimiento() == null)
				commerce.setNombreEstablecimiento("");
			if(commerce.getNombrePropCuenta() == null)
				commerce.setNombrePropCuenta("");
			if(commerce.getRazonSocial()== null)
				commerce.setRazonSocial("");
			if(commerce.getRepresentanteLegal() == null)
				commerce.setRepresentanteLegal("");
			if(commerce.getScanReference() == null)
				commerce.setScanReference("");
			if(commerce.getTelefono() == null)
				commerce.setTelefono("");
			if(commerce.getUrlLogo() == null)
				commerce.setUrlLogo("");
				
		}catch(Exception ex){
			LOGGER.error("ERROR AL QUITAR NULLS", ex);
		}
	}
	
	public VerifyResponse statusVerify(Integer idApp, long idUsuario, String idioma) {
		VerifyResponse response = new VerifyResponse();
		try {
			LOGGER.debug("INICIANDO PROCESO DE VERIFICACION..");
			TUsuarios user = mapper.getUserInfo(idUsuario, idApp);

			LOGGER.debug("ESTATUS JUMIO " + user.getUsr_jumio());
			response.setId_usr_status(user.getIdUsrStatus());
			response.setIdpais(user.getIdPais());
			response.setUsr_apellido(user.getUsrApellido());
			response.setUsr_nombre(user.getUsrNombre());
			response.setUsr_telefono(user.getUsrTelefono());
			response.setIdError(0);
			response.setMensajeError("");
			response.setUsr_sms(user.getUsr_sms() == null ? "" : user.getUsr_sms());
			response.setUsr_email(user.getUsr_email() == null ? "" : user.getUsr_email());
			response.setScanReference(user.getScanReference());
			response.setUsr_jumio(user.getUsr_jumio());
			
			if(user.getUsr_jumio() != null &&  user.getScanReference() != null && !user.getScanReference().isEmpty() )
			{
				if((user.getUsr_jumio().equals("2") || user.getUsr_jumio().equals("0")) && user.getIdUsrStatus() == 1 ){
					JumioVerifyRes jum = jumioClient.verifcarReferencia(user.getScanReference(),idApp,1,idioma);
					if(jum != null){
						user.setIdError(jum.getCode());
						user.setMensajeError(jum.getMessage());
						user.setUsr_jumio(jum.getJumioStatus()+"");
					}
				}
				if(user.getUsr_jumio().equals("2")){
					if(idioma.equals("es"))
						response.setMensajeError("Para poder ingresar a tu aplicación recibiras un sms y un correo, en un máximo de 5 minutos. Tus documentos están siendo validados.");
					else
						response.setMensajeError("In order to enter your application you will receive an SMS and an email, in a maximum of 5 minutes. Your documents are being validated.");
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.error("[ERROR AL VERIFICAR ESTATUS ] [USUARIO " + idUsuario + "]", ex);
			getBaseResponse(idioma, response, 99);
		}
		return response;
	}

	public BaseResponse passwordUpdate(Integer idApp, String idioma, String idUsuario, String pass_old, String pass_new) {

		LOGGER.debug("[INICIANDO ACATUALIZACION DE PASSWORD ] USER: " + idUsuario);
		TUsuarios user = new TUsuarios();
		BaseResponse response = new BaseResponse();
		try {

			String password = AddcelCrypto.decryptHard(pass_new);
			String passaux = UtilsMC.generaSemillaAux(password);
			String passnew = Crypto.aesEncrypt(passaux, password);
			user.setNewPassword(passnew);
			user.setIdeUsuario(Long.parseLong(idUsuario));
			user.setIdioma(idioma);
			user.setIdApp(idApp);

			String responseJSON = mapper.cambiarPasswordUsuario(user);
			LOGGER.debug("[RESPUESTA DE BASE DE DATOS: ] " + responseJSON);

			user = gson.fromJson(responseJSON, TUsuarios.class);
			response.setIdError(user.getIdError());
			response.setMensajeError(user.getMensajeError());

			if (user.getIdError() == 0) {
				TemplateCorreo correo = mapper.getTemplateMail("@MENSAJE_RECOVERYPWD", idioma, idApp);
				UtilsMC.emailSend(user.geteMail(), correo.getBody(),
						correo.getAsunto(), user.getUsrLogin(), "", password,
						user.getUsrNombre() + " " + user.getUsrApellido(),
						new String[] { Constantes.Path_images + "user_update_password.PNG" });
			}

		} catch (Exception ex) {
			LOGGER.debug("[ERROR AL ACTUALIZAR PASWORD] ", ex);
			getBaseResponse(idioma, response, 101);
		}
		LOGGER.debug("[FINALIZANDO ACTUALIZACION DE PASSWORD ] USER: " + idUsuario);
		return response;
	}

	public BaseResponse passwordReset(Integer idApp, String userOrEmail, String idioma) {
		LOGGER.debug("[INICIANDO RESET DE PASSWORD ] login: " + userOrEmail);
		TUsuarios user = new TUsuarios();
		BaseResponse response = new BaseResponse();

		try {

			String password = UtilsMC.generatePassword(8);
			String passr = UtilsMC.generaSemillaAux(password);
			String passrc = Crypto.aesEncrypt(passr, password);

			user.setUsrPwd(passrc);
			user.setUsrLogin(userOrEmail);
			user.setIdioma(idioma);
			user.setIdApp(idApp);

			String responseJSON = mapper.resetPasswordUsuario(user);
			LOGGER.debug("[RESPUESTA BASE DE DATOS] "
					+ responseJSON.replace("email", "eMail").replace("nombres", "usrNombre"));

			user = gson.fromJson(responseJSON.replace("email", "eMail").replace("nombres", "usrNombre"),
					TUsuarios.class);
			response.setIdError(user.getIdError());
			response.setMensajeError(user.getMensajeError());

			if (user.getIdError() == 0) {
				TemplateCorreo correo = mapper.getTemplateMail("@MENSAJE_RECOVERYPWD", idioma, idApp);
				LOGGER.info("DATOS DE CORREO A ENVIAR - {} - {} ", user.geteMail(), correo.getBody());
				UtilsMC.emailSend(user.geteMail(), correo.getBody(),
						correo.getAsunto(), userOrEmail, "", password,
						user.getUsrNombre() + " " + user.getUsrApellido(),
						new String[] { Constantes.Path_images + "user_update_password.PNG" });
			}

		} catch (Exception ex) {
			LOGGER.debug("[ERROR AL RESETEAR PASWORD] USER: " + userOrEmail, ex);
			getBaseResponse(idioma, response, 101);
		}
		LOGGER.debug("[FINALIZANDO RESET DE PASSWORD ] USER: " + userOrEmail);
		return response;
	}
	
	 public BaseResponse sendMailPasswordReset(String userOrEmail, String idioma, int idApp){
	    	LOGGER.debug("[Enviando correo para reestablecer contraseña]: " + userOrEmail );
	    	
	    	BaseResponse response = new BaseResponse();
	    	
	    	try{
	    			TUsuarios user = mapper.getUserInfoByLogin(userOrEmail,idApp);
	    			if(user != null && !user.getUsrLogin().isEmpty()){
	    				if(user.getIdUsrStatus()==1){
				    			String cadSensitive = java.util.Base64.getEncoder().encodeToString(AddcelCrypto.encryptHard(user.getIdeUsuario()+"").getBytes());
				    			//ProjectMC projects = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
				    			TemplateCorreo correo = mapper.getTemplateMail("@MENSAJE_RESETPWD", idioma, idApp);
				    			
			    			
				    			UtilsMC.emailSend(user.geteMail(),correo.getBody().replace("@GETP", "?cod="+cadSensitive).replace("@IDIOMA", idioma), correo.getAsunto(), user.getUsrNombre(), "","", "", new String[]{Constantes.Path_images+"user_update_password.PNG"});
				    			
				    			response.setIdError(0);
				    			if(idioma.equalsIgnoreCase("es"))
				    				response.setMensajeError("Se envio un mensaje a su correo electrónico para continuar con el proceso.");
				    			else
				    				response.setMensajeError("A message was sent to your email to continue with the process.");
	    				}
	    				else
	    				{
	    					response.setIdError(600);
			    			if(idioma.equalsIgnoreCase("es"))
			    				response.setMensajeError("Su registro presenta problemas para reestablecer su contraseña, contacte a soporte");
			    			else
			    				response.setMensajeError("Your registration has problems to reestablish your password, contact support");
	    				}
	    			}else
	    			{
	    				response.setIdError(90);
	    				if(idioma.equalsIgnoreCase("es"))
	    					response.setMensajeError("No existe usuario registrado con el dato proporcionado");
	    				else
	    					response.setMensajeError("There is no registered user with the data provided");
	    			}
	    		
	    		
	    	}catch(Exception ex){
	    		LOGGER.debug("[ERROR AL RESETEAR PASWORD] USER: " + userOrEmail, ex);
	    		getBaseResponse(idioma, response, 101);
	    	}
	    	LOGGER.debug("[FINALIZANDO RESET DE PASSWORD ENVIO DE CORREO] USER: " + userOrEmail );
	    	return response;
	    }
	 
	 public ModelAndView emailConfirm(String cod, String error, String idioma, int idApp){
	    	
	    	try{
	    		
	    		ModelAndView view = new ModelAndView("user/confirmEmail");
	    		if(error != null && !error.isEmpty())
	    			view.addObject("error", error);
	    			else
	    				view.addObject("error","");
	    		view.addObject("cod",cod);
	    		view.addObject("idioma",idioma);
	    		view.addObject("idApp",idApp);
	    		return view;
	    		
	    	}catch(Exception ex){
	    		LOGGER.error("ERROR AL MOSTRAR VISTA *CONFIRMA CORREO", ex);
	    		ModelAndView view = new ModelAndView("/"+idioma+"/pass/confirm");
	    		if(idioma.equalsIgnoreCase("es"))
	    			view.addObject("error","ERROR INESPERADO, INTENTE MAS TARDE");
	    		else
	    			view.addObject("error","UNEXPECTED ERROR, TRY LATER");
	    		return view;
	    		
	    	}
	}
	 
	 public ModelAndView validEmail(String cod,String idioma,String email, int idApp){
	    	
	    	ModelAndView view = null;
	    	try{
	    		
	    		String Cdecryp = new String(java.util.Base64.getDecoder().decode(cod.getBytes()));
	    		
	    		TUsuarios user = mapper.getUserInfo(Long.parseLong(AddcelCrypto.decryptHard(Cdecryp)), idApp);
	    		
	    		if(user != null && user.geteMail().equals(email)){
	    		view = new ModelAndView("user/resetpass");
	    			view.addObject("cod",cod);
	    			view.addObject("idioma",idioma);
	    			view.addObject("idApp",idApp);
	    		}
	    		else{
	    			view = new ModelAndView("user/confirmEmail");
	    			if(idioma.equalsIgnoreCase("es"))
	    				view.addObject("error", "El correo no coincide en el sistema");
	    			else
	    				view.addObject("error", "Mail does not match in the system");
	        		view.addObject("cod",cod);
	        		view.addObject("idioma",idioma);
	        		view.addObject("idApp",idApp);
	    		}
	    		
	    		
	    		return view;
	    		
	    	}catch(Exception ex){
	    		LOGGER.error("ERROR AL VALIDAR CORREO ELECTRONICO", ex);
	    		view = new ModelAndView("user/confirmEmail");
	    		view.addObject("idioma",idioma);
	    		view.addObject("idApp",idApp);
	    		if(idioma.equalsIgnoreCase("es"))
	    			view.addObject("error", "error inexperado, intente mas tarde");
	    		else
	    			view.addObject("error","unexperienced error, try later");
	    		view.addObject("cod",cod);
	    		return view;
	    	}
	 }
	 
	 
	 public ModelAndView resetPass(String cod, String pass, String idioma, int idApp){
	    	
	    	ModelAndView view = new ModelAndView("user/resetPSuccess");
	    	view.addObject("idApp",idApp);
	    	try{
	    		String Cdecryp = new String(java.util.Base64.getDecoder().decode(cod.getBytes()));//AddcelCrypto.decryptSensitive(cod);
	    		
	    		
				String passr = UtilsMC.generaSemillaAux(pass);
				String passrc = Crypto.aesEncrypt(passr, pass);
	    		
	    		TUsuarios user = mapper.getUserInfo(Long.parseLong(AddcelCrypto.decryptHard(Cdecryp)),idApp);
	    		user.setIdioma(idioma);
	    		user.setUsrPwd(passrc);
				user.setUsrLogin(user.getUsrLogin());
				user.setIdApp(idApp);
	    		String responseJSON = mapper.resetPasswordUsuario(user);
	    		LOGGER.debug("[RESPUESTA BASE DE DATOS] " + responseJSON.replace("email", "eMail").replace("nombres", "usrNombre"));
	    		
	    		user =  gson.fromJson(responseJSON.replace("email", "eMail").replace("nombres", "usrNombre"), TUsuarios.class);
	    		view.addObject("message",user.getMensajeError());
	    		
	    		
	    	}catch(Exception ex){
	    		LOGGER.error("ERROR AL RESETEAR PASSWORD", ex);
	    		if(idioma.equalsIgnoreCase("es"))
	    			view.addObject("message", "Ocurrio un error al restaurar su contraseña, intente mas tarde");
	    		else
	    			view.addObject("message", "An Error occurred while restoring your password, try later");
	    	}
	    	
	    	return view;
	}
	

	public UpdateResponse userUpdateInfo(Integer idApp, String idioma, String name, long idUsuario, String login, String value) {
		LOGGER.debug("[INICIANDO PROCESO DE ACTUALIZACION ] [" + name + "] " + idUsuario);
		UpdateResponse response = new UpdateResponse();
		try {

			String responseJSON = mapper.updateUserInfo(idioma, name, value, idUsuario,login, idApp);
			LOGGER.debug("[RESPUESTA BD] " + responseJSON);
			response = gson.fromJson(responseJSON, UpdateResponse.class);
			if(response.getIdError() == 0){
				if(idioma.equals("es"))
					response.setMensajeError("Para poder ingresar a tu aplicación recibiras un sms y un correo, en un máximo de 5 minutos. Tus documentos están siendo validados.");
				else
					response.setMensajeError("In order to enter your application you will receive an SMS and an email, in a maximum of 5 minutes. Your documents are being validated.");
			}

		} catch (Exception ex) {
			LOGGER.debug("[ERROR AL ACTUALIZAR DATOS] USER: " + idUsuario, ex);
			getBaseResponse(idioma, response, 101);
		}
		LOGGER.debug("[FINALIZANDO PROCESO DE ACTUALIZACION ] [" + name + "] " + idUsuario);
		return response;
	}

	public TermResponse getTerms(Integer idApp, String idioma, int idpais, String pass) {

		TermResponse response = new TermResponse();
		response.setIdError(0);
		response.setMensajeError("");

		String hash_android = UtilsMC.getSHA256("mobilecardandroid");
		String hash_ios = UtilsMC.getSHA256("mobilecardios");

		LOGGER.debug("android: " + pass + "\t " + hash_android);
		LOGGER.debug("ios: " + pass + "\t " + hash_ios);
		try {
			//if (pass.equals(hash_ios) || pass.equals(hash_android)) {

				String term = mapper.getTerminos(idpais, idioma, idApp);
				response.setTermino(term);
				if (term == null || term.isEmpty()) {
					getBaseResponse(idioma, response, 90);
				}
			/*} else {
				response.setIdError(91);
				if (idioma.equalsIgnoreCase("es"))
					response.setMensajeError("Acceso denegado");
				else
					response.setMensajeError("Access denied");
			}*/
		} catch (Exception ex) {
			getBaseResponse(idioma, response, 92);
			LOGGER.error("ERROR AL OBTENER TERMINOS Y CONDICIONES", ex);
		}
		return response;
	}

	public TermResponse getPrivacy(String idioma, int idpais, String pass) {

		TermResponse response = new TermResponse();
		response.setIdError(0);
		response.setMensajeError("");

		String hash_android = UtilsMC.getSHA256("mobilecardandroid");
		String hash_ios = UtilsMC.getSHA256("mobilecardios");

		LOGGER.debug("android: " + pass + "\t " + hash_android);
		LOGGER.debug("ios: " + pass + "\t " + hash_ios);
		try {
			//if (pass.equals(hash_ios) || pass.equals(hash_android)) {

				String term = mapper.getPrivacidad(idpais, idioma, 2);// 1= terminos 2 = privacidad
				response.setTermino(term);
				if (term == null || term.isEmpty()) {
					getBaseResponse(idioma, response, 90);
				}
		//	} 
			/*else {
				response.setIdError(91);
				if (idioma.equalsIgnoreCase("es"))
					response.setMensajeError("Acceso denegado");
				else
					response.setMensajeError("Access denied");
			}*/
		} catch (Exception ex) {
			getBaseResponse(idioma, response, 92);
			LOGGER.error("ERROR AL OBTENER PRIVACIDAD", ex);
		}
		return response;
	}
	
	private void getBaseResponse(String idioma, AbstractVO response, int cod) {
		response.setIdError(cod);
		if (idioma.equalsIgnoreCase("es"))
			response.setMensajeError("Error inesperado, vuelva a intentarlo");
		else
			response.setMensajeError("Unexpected error, please try again");
	}
	
	private void getBaseResponse(String idioma, BaseResponse response, int cod) {
		response.setIdError(cod);
		if (idioma.equalsIgnoreCase("es"))
			response.setMensajeError("Error inesperado, vuelva a intentarlo");
		else
			response.setMensajeError("Unexpected error, please try again");
	}
	
	
	
	public Establecimiento AddCommerce(Establecimiento est , int idApp, String idioma,int version){
		Establecimiento response = new Establecimiento();
		try{
			
			 LOGGER.debug("[ALTA ESTABLECIMIENTO]" + est.getNombre_establecimiento());
			 
			 est.setIdPais(est.getIdPais() == 0 ? 1 : est.getIdPais());
			 est.setEstatus(1);
			 est.setId_aplicacion(idApp);
			 est.setUsuario(est.getUsuario().trim());
			 est.setTipo_persona(est.getTipo_persona() == null ? "" : est.getTipo_persona());
			 int cont_user = mapper.searchEstab(est.getUsuario(),idApp);
			 if(est.getUsr_institucion() == null || est.getUsr_institucion().isEmpty())
				 est.setUsr_institucion(est.getInstitucion() != null ? est.getInstitucion() : "");
			 
			
			 if(cont_user == 0){
				 Bank bank = null;
				 if(est.getIdPais() == 4) {
					 bank =  mapper.getBancoPeru(Long.parseLong(est.getId_banco()));
				 } else {
					 bank =  mapper.getBancoBillPocket(Long.parseLong(est.getId_banco()));//mapper.getBanco(Long.parseLong(est.getId_banco()));
				 }
				 
				 if(bank != null){
					 LOGGER.debug("seteando valores de comision...");
					 est.setComision_porcentaje(bank.getComision_porcentaje());
					 est.setComision_fija(bank.getComision_fija());
					 est.setCodigo_banco(bank.getClave());
				 }
				 
				 if(StringUtils.isNotEmpty(est.getCuenta_clabe())) {
					 	LOGGER.debug("entrando no vacio cuenta clabe...");
					    String act_type = "CLABE";
						est.setId_account(0);
						est.setComision_fija(bank.getComision_fija());
						est.setComision_porcentaje(bank.getComision_porcentaje());
						est.setUrlLogo(mapper.getProjectMC("LCPFServices", "getDefaultLogo").getUrl());
					    est.setCodigo_banco(bank.getClave());
					    est.setCuenta_tipo(act_type);
				 }
					 byte[] img_ine= est.getImg_ine().getBytes();
					 byte[] img_dom = est.getImg_domicilio().getBytes(); 
					 
					 if(StringUtils.isEmpty(est.getRepresentante_legal())) {
						 est.setRepresentante_legal(est.getNombre() + " "+ est.getPaterno()+ " " + est.getMaterno());
					 }
					 if(StringUtils.isEmpty(est.getPaterno())) {
						 est.setNombre(est.getRepresentante_legal());
						 est.setPaterno("-");
						 est.setMaterno("-");
						 est.setCurp("-");
						 est.setDireccion_establecimiento("Gustavo Baz Prada 98");
						 est.setColonia("Naucalpan de Juárez");
						 est.setCp("53370");
						 est.setCiudad("Naucalpan de Juárez");
						 est.setId_estado(15);
					 }
					 
					 if(version == 1)
						 est.setJumio_status("0");
					 else
						 est.setJumio_status("1");
					 
					 if(est.getId() != 0) {
						 mapper.updateEstab(est);
					 } else {
						 LOGGER.debug("NEGOCIO A GUARDAR: " + gson.toJson(est));
						 mapper.insertEstablecimiento(est,img_ine,img_dom,1); 
					 }
				    
				    
				    
					 if(est.getT_previvale() != null && !est.getT_previvale().isEmpty()) {
						 try {
							 LOGGER.debug("[ACTIVANDO TARJETA DEL NEGOCIO ] {}", est.getId());
							 PreviValeService previvale = new PreviValeService();
							 previvale.registrarConTarjeta(est.getId(), UtilsMC.setSMS(AddcelCrypto.decryptHard(est.getT_previvale())), idApp,1, idioma);
							 //activatePrevivaleCard(est.getId(), est.getT_previvale());
						} catch (Exception e) {
							e.printStackTrace();
						}
					 }
					 
					 
					 /***************ACTUALIZANDO QR**************/
				     updateQRComercio(est.getId(), idApp, est.getIdPais(), idioma, est.getNombre_establecimiento());
					 
					 
					 /****************ENVIANDO CONTRATO MOBILECARD COMERCIO PERU****************/
					 if(est.getIdPais() == 4)
						 SendAgreement(est.getEmail_contacto(), est.getNombre(), idApp);
					 
					 response.setIdError(0);
					 response.setId(est.getId());
					 if(idioma.equalsIgnoreCase("es"))
						 response.setMensajeError("Para poder ingresar a tu aplicación recibiras un sms y un correo, en un máximo de 5 minutos. Tus documentos están siendo validados.");
					 else
						 response.setMensajeError("In order to enter your application you will receive an SMS and an email, in a maximum of 5 minutes. Your documents are being validated.");
					
			 }else{
				 
				 response.setIdError(98);
				 if(idioma.equalsIgnoreCase("es"))
					 response.setMensajeError("Existe un usuario con el email introducido");
				 else
					 response.setMensajeError("There is already a user with the email entered.");
			 }
			 
		}catch(Exception ex){
			LOGGER.error("error al agregar comercio... ", ex);
			getBaseResponse(idioma, response, 3000);
		}
		return response;
	}
	
	private void SendAgreement (String  sMail,String nombre, int idApp){
		try{ 
			
			 //TEMPLATE_EMAIL template = templateEmail.findOne("@MENSAJE_CONTRATO_TEBCA");
			TemplateCorreo template =  mapper.getTemplateMail("@MENSAJE_CONTRATO_MC_PERU", "es", idApp);
			 MailSender correo = new MailSender();
		     String from = "no-reply@addcel.com";
		     String[] to = {sMail};
		     correo.setCc(new String[] {});
		     correo.setBody(template.getBody().replace("@NOMBRE", nombre));
		     correo.setFrom(from);
		     correo.setSubject(template.getAsunto());
		     correo.setTo(to);
		     correo.setCid(new String[] {  "/usr/java/resources/images/Addcel/user_registry.PNG" });
		     correo.setAttachments(new String[] {"/usr/java/resources/contratos/ContratoMCPeru.pdf"});
		     LOGGER.debug("ENVIANDO CONTRATO MOBILECARD {}",nombre) ;
		     EmailSenderClient client = new EmailSenderClient();
		     client.SendMail(correo);
		}catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR CONTRATO TEBCA "+ nombre, ex);
		}
	}
	
	private void updateQRComercio(long idcomercio, int idApp, int idPais, String idioma, String nombreComercio){
		try{
			
			 Calendar calendar = Calendar.getInstance();
			 calendar.setTime(new Date());
			 calendar.add(Calendar.YEAR, 3);
			 String FORMAT = "yyyy-MM-dd";
			 SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
			 String fechaFormat = sdf.format(calendar.getTime());
			 String qrTag = null;
			 String qrBase64 = null;
			 
			 switch (idPais) {
			case 1:
				 	QRMXRequest req = new QRMXRequest();
				 	req.setIdEstablecimiento(idcomercio);
				 	req.setIdPais(idPais);
				 	req.setMonto(0.0);
				 	req.setTypeQR("ESTATICO");
				 	QRMXResponse res =  qrClient.getQRComercioMx(req, idApp, idioma);
				 	if(res != null && res.getIdError() == 0) {
				 		qrTag = "";
				 		qrBase64 = res.getQrBase64();
				 	}else{
						 LOGGER.debug("NO SE PUDO OBTENER QR COMERCIO {}", idcomercio);
					 }
				break;
			case 4:
					 QRequest qrequest = new QRequest();
					 //qrequest.setAmount("0");
					 qrequest.setDescription(nombreComercio);
					 qrequest.setExpirationDate(fechaFormat);
					 QR qr = qrClient.getQRComercio(idcomercio, idApp, idPais, idioma, qrequest);
					 
					 if(qr != null && qr.getCode() == 0){
						 qrTag = qr.getQrTag();
						 qrBase64 = qr.getQrBase64();
					 }else{
						 LOGGER.debug("NO SE PUDO OBTENER QR COMERCIO {}", idcomercio);
					 }
				break;

			default:
				LOGGER.debug("QR NO DISPONOBLE PARA PAIS {}",idPais);
				break;
			}
			
			 if(qrBase64 != null && !qrBase64.isEmpty()) 
				 mapper.updateCommerceQR(idcomercio, qrTag, qrBase64);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTUALIZAR QR COMERCIO ", ex);
		}
	}
	
	public UpdateCommerceRes AdminUpdate(Establecimiento establecimiento, String idioma, int idApp){
		UpdateCommerceRes response = new UpdateCommerceRes();
		 try{
			 
			 LOGGER.debug("[INICIANDO ACTUALIZACIÓN ESTABLECIMIENTO]" + establecimiento.getId());
			 establecimiento.setId_aplicacion(idApp);
			 mapper.updateEstab(establecimiento);
			 
			 establecimiento = mapper.getEstablecimiento(establecimiento.getId());
			 response = gson.fromJson(gson.toJson(establecimiento),UpdateCommerceRes.class);
			 
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Datos Actualizados");
			 else
				 response.setMensajeError("Updated data");
			 
			}catch(Exception ex){
			 LOGGER.error("[ERROR UPDATE ESTABLECIMIENTO]"+ establecimiento.getId(), ex);
			 response.setIdError(99);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, vuelva a intentarlo");
			 else
				 response.setMensajeError("Unexpected error, try again");
		 }
		 return response;
	 }
	
	
 public BaseResponse adminPassUpdate(String idioma, long idAdmin, String newPass){
		 
		 BaseResponse response = new BaseResponse();
		 try{
			 LOGGER.debug("[ACTUALIZANDO PASS]");
			 Establecimiento est = mapper.getEstablecimiento(idAdmin);
			 //String pass = AddcelCrypto.decryptHard(newPass);
			// newPass = Business.encode(pass);
			 mapper.AdminPassUpdate(idAdmin, newPass,1);
			 response.setIdError(0);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Contraseña actualizada");
			 else
				 response.setMensajeError("Updated password");
			 HashMap< String, String> datos = new HashMap<String, String>();
			 datos.put("@USUARIO", est.getUsuario());
			 datos.put("@PASSWORD", "");
			 ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
			 
			 Business.SendMailMicrosoft(est.getEmail_contacto(), mapper.getParameter("@MESSAGE_UPDATE_EMAIL_LCPF_ADMIN_ES"), "La cuenta por favor", datos,project.getUrl());
		 }catch(Exception ex){
			 LOGGER.error("[ERROR AL ACTUALIZAR PASSWORD]" + idAdmin, ex);
			 response.setIdError(98);
			 if(idioma.equalsIgnoreCase("es"))
				 response.setMensajeError("Error inesperado, intente mas tarde");
			 else
				 response.setMensajeError("Unexpected error, try later");
		 }
		 return response;
	 }
 
 public BaseResponse AdminResetPass(String idioma, String admin, int idApp){
	 BaseResponse response = new BaseResponse();
	 try{
		 LOGGER.debug("[RESET PASS]");
		 
		 Establecimiento est = mapper.getEstablecimientoByUser(admin, idApp);
		 long idAdmin = est.getId();
		 String newpass = UtilsMC.generatePassword(8);
		 String passEncoder = Business.encode(newpass);
		 mapper.AdminPassUpdate(idAdmin, passEncoder,98);
		 response.setIdError(0);
		 if(idioma.equalsIgnoreCase("es"))
			 response.setMensajeError("Se ha enviado a su correo una contraseña provisional");
		 else
			 response.setMensajeError("A temporary password has been sent to your email");
		 HashMap< String, String> datos = new HashMap<String, String>();
		 datos.put("@USUARIO", est.getUsuario());
		 datos.put("@PASSWORD", newpass);
		 //LOGGER.debug("pass funcion: " + newpass);
		 ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
		 
		 Business.SendMailMicrosoft(est.getEmail_contacto(), mapper.getParameter("@MESSAGE_RESET_EMAIL_LCPF_ADMIN_ES"), "La cuenta por favor", datos, project.getUrl());
	 }catch(Exception ex){
		 LOGGER.error("[ERROR RESET PASS]", ex);
		 response.setIdError(98);
		 if(idioma.equalsIgnoreCase("es"))
			 response.setMensajeError("Error inesperado, intente mas tarde");
		 else
			 response.setMensajeError("Unexpected error, try later");
	 }
	 return response;
 }
 
 public BankCodesResponse getBankCodes(Integer idPais){
		BankCodesResponse response = new BankCodesResponse();
		try{
			LOGGER.debug("[INICIANDO OBTENCION DE CATALOGO DE BANCOS] - PAIS - {}", idPais);
			if(idPais == 4) {
				response.setBanks( mapper.getBankcodesPeru());
			} else {
				response.setBanks( mapper.getBankcodesBillPocket());
			}
			LOGGER.debug("despues de base");
			response.setIdError(0);
			response.setMensajeError("");
		}catch(Exception ex){
			LOGGER.error("[ERROR AL OBTENER BNANCOS]", ex);
			response.setIdError(99);
			response.setMensajeError("Error inesperado, vuelva a intentarlo");
		}
		LOGGER.debug("retornando reswpuesta");
		return response;
	}
 
 	public Object validateSMS(Integer idApp, Integer idPais, String codigo, String idioma, 
 			String tipoUsuario, long id) {
 		AbstractVO response = null;
		LOGGER.debug("[VALIDANDO SMS] - SMS: {} - ID USUARIO: {}", codigo, id);
		try {
			if(USUARIO.equalsIgnoreCase(tipoUsuario)) {
				TUsuarios usuario = validateSMSUser(id, idApp, codigo, idioma);
				response = usuario;
 			} else if (NEGOCIO.equalsIgnoreCase(tipoUsuario)) {
 				Establecimiento establecimiento = validateSMSCommerce(id, idApp, codigo, idioma);
 				response = establecimiento;
 			}
		} catch (Exception ex) {
			LOGGER.error("[Error al activar usuario] ", ex);
			response.setIdError(-1);
			response.setMensajeError("No se puedo realizar la validacion de su SMS.");
		}
		return response;
	}
 	
 	private TUsuarios validateSMSUser(long idUsuario, Integer idApp, String codigo, String idioma) {
 		TUsuarios usuario = null;
 		try {
 			usuario = mapper.getUserInfo(idUsuario, idApp);
 			if(codigo.equals(usuario.getSmsCode())) {
				/*String value = AddcelCrypto.encryptHard(usuario.getUsrLogin()) + ":"
						+ AddcelCrypto.encryptHard(usuario.getUsrPwd());
				String param = "?codigo=" + Base64.encode(value.getBytes());
				String url = Constantes.ACTIVATION_URL.replace("{idioma}", idioma) + param;
				url = url.replace("{idApp}", String.valueOf(idApp));
				LOGGER.debug("[ACTIVATE URL] " + url);
				TemplateCorreo correo = mapper.getTemplateMail("@MENSAJE_ACTIVATE", idioma, idApp);
				UtilsMC.emailSend(usuario.geteMail(), correo.getBody(),
						correo.getAsunto(), usuario.getUsrLogin(), url, "",
						usuario.getUsrNombre() + " " + usuario.getUsrApellido(),
						new String[] { Constantes.Path_images + "user_registry.PNG" });*/
				mapper.updateStatusSMS(idUsuario,"1");
				usuario.setIdUsrStatus(99);
			} else {
				usuario.setIdError(-1);
				usuario.setMensajeError("SMS no valido.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
 		return usuario;
 	}
 	
 	private Establecimiento validateSMSCommerce(long id, Integer idApp, String codigo, String idioma) {
 		Establecimiento esta = null;
 		try {
 			esta = mapper.getEstablecimiento(id);
 			if(codigo.equals(esta.getSmsCode())) {				
				 LOGGER.debug("id establecimiento: " + esta.getId());
				 String value =  AddcelCrypto.encryptHard(esta.getId()+":"+idApp);
				 LOGGER.debug("base 64: "+ Base64.encode(value.getBytes()));
				 String param = "?codigo="+Base64.encode(value.getBytes());
				 ProjectMC projects = mapper.getProjectMC("LCPFServices", "adminActivate");
				 String url =  Constantes.ACTIVATION_URL_COMMERCE.replace("{idioma}", idioma)+param;//Constantes.ACTIVATION_URL.replace("{idioma}", idioma)+param;
				 LOGGER.debug("[ACTIVATE URL] " + url);
				
				if(idioma.equalsIgnoreCase("es"))
	 			    UtilsMC.emailSend(esta.getEmail_contacto(), mapper.getParameter("@MENSAJE_ACTIVATE_LCPF_ES"), 
	 			    		mapper.getParameter("@ASUNTO_ACTIVATE_LCPF_ES"), esta.getEmail_contacto(), 
	 			    		url, "", esta.getRepresentante_legal(),new String[]{Constantes.Path_images+"user_registry.PNG"});
	 			 else
	 				 UtilsMC.emailSend(esta.getEmail_contacto(), mapper.getParameter("@MENSAJE_ACTIVATE_LCPF_EN"), 
	 						 mapper.getParameter("@ASUNTO_ACTIVATE_LCPF_EN"), esta.getEmail_contacto(), url,"",  
	 						 esta.getRepresentante_legal(),new String[]{Constantes.Path_images+"user_registry.PNG"}); 
				
				mapper.updateStatusCommerce(id, 99);
				esta.setEstatus(99);
			} else {
				esta.setIdError(-1);
				esta.setMensajeError("SMS no valido.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
 		return esta;
 	}
 
 	private boolean sendSMS(Integer idApp, Integer idPais, String idioma, String numero,
 			String codigo, long idUsuario) {
 		boolean resp = false;
 		ResponseSMS sms = null;
 		ResponseEntity<ResponseSMS> response = null;
 		String prefijoNumero = null;
 		try {
 			LOGGER.info("ENVIANDO SMS - NUMERO {} - CODIGO {}", numero, codigo);
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			if(idPais == 1) {
				prefijoNumero = "+521";
			} else if (idPais == 2) {
				prefijoNumero = "+57";
			} else if (idPais == 3) {
				prefijoNumero = "+1";
			}else if(idPais == 4){
				prefijoNumero = "+51";
			}
			HttpEntity<String> entity = new HttpEntity<String>("{\n" + 
					"    \"idUsuario\": \""+idUsuario+"\",\n" + 
					"    \"idMensaje\": \"1\",\n" + 
					"    \"numeroCelular\": \""+prefijoNumero+numero+"\",\n" + 
					"    \"params\": {\n" + 
					"        \"<nip>\": \""+codigo+"\"\n" + 
					"    }\n" + 
					"}", headers);
			response = rest.exchange("http://localhost/sms/1/MX/ESP/send", HttpMethod.POST, entity, ResponseSMS.class);	
			sms = response.getBody();
			LOGGER.info("RESPUESTA DE ENVIO SMS - NUMERO {} - RESP {}", prefijoNumero+numero, gson.toJson(sms));
			if(sms.getCode() == 1) {
				resp = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR AL ENVIAR SMS - NUMERO {} - ERROR {}", prefijoNumero+numero, e.getLocalizedMessage());
		}
 		return resp;
 	}
 	
 	private void activatePrevivaleCard(long id, String t_previvale) {
 		ResponseSMS sms = null;
 		ResponseEntity<ResponseSMS> response = null;
 		try {
 			LOGGER.info("ACTIVANDO TARJETA PAR NEGOCIO - {} - TARJETA {}", id, t_previvale);
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			t_previvale = AddcelCrypto.decryptHard(t_previvale);
			headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
			HttpEntity<String> entity = new HttpEntity<String>("{\"idEstablecimiento\": "
					+id+",\"tarjeta\": \""+t_previvale+"\"}", headers);
			response = rest.exchange("http://localhost/PreviVale/api/3/1/es/registrarNegocioConTarjeta", 
					HttpMethod.POST, entity, ResponseSMS.class);	
			sms = response.getBody();
			LOGGER.info("RESPUESTA DE ACTIVACION TARJETA NEGOCIO - ID {} - RESP {}", id, gson.toJson(sms));
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR AL ACTIVACION TARJETA NEGOCIO - ID {} - ERROR {}", id, e.getLocalizedMessage());
		}		
	}
 	
 	private CardRequest getEstablecimientoCard(Long idEstablecimiento) {
 		CardRequest card = null;
 		ResponseEntity<CardRequest> response = null;
 		try {
 			LOGGER.info("CONSULTANDO TARJETA DE NEGOCIO - TARJETA {}",  idEstablecimiento);
			RestTemplate rest = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
			HttpEntity<String> entity = new HttpEntity<String>("{\"idEstablecimiento\": "
					+idEstablecimiento+"}", headers);
			response = rest.exchange("http://localhost/PreviVale/api/3/1/es/registrarNegocioConTarjeta", 
					HttpMethod.POST, entity, CardRequest.class);	
			card = response.getBody();
			LOGGER.info("CONSULTANDO TARJETA DE NEGOCIO - ID {} - RESP {}", idEstablecimiento, gson.toJson(card));
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR CONSULTAR TARJETA DE NEGOCIO - ID {} - ERROR {}", idEstablecimiento, e.getLocalizedMessage());
		}
 		return card;
	}


	public BaseResponse resendSMS(Integer idApp, Integer idPais, String idioma, String tipoUsuario, long id) {
		BaseResponse response = new BaseResponse();
		String telefono = null;
		try {
			LOGGER.info("RE ENVIANDO SMS AL USUARIO - ID - {} - TIPO USUARIO - {}", id, 
					tipoUsuario + " - APP - "+idApp+" - PAIS - "+idPais+" - IDIOMA - "+idioma);
			SecureRandom random = new SecureRandom();
			int num = random.nextInt(100000);
			String formatted = String.format("%06d", num);
			if(USUARIO.equalsIgnoreCase(tipoUsuario)) {
				TUsuarios login = mapper.getUserInfo(id, idApp);
				LOGGER.info("CODIGO RE GENERADO: {} - IDUSUARIO: {}", formatted,
						id + ", TELEFONO: " + login.getUsrTelefono() + ", PAIS: " + idPais);
				telefono = login.getUsrTelefono();
				mapper.updateUserSMSCode(id, formatted);
 			} else if (NEGOCIO.equalsIgnoreCase(tipoUsuario)) {
 				Establecimiento establecimiento = mapper.getEstablecimiento(id);
 				telefono = establecimiento.getTelefono_contacto();
 				LOGGER.info("CODIGO RE GENERADO: {} - ID NEGOCIO: {}", formatted,
						id + ", TELEFONO: " + telefono + ", PAIS: " + idPais);
 				mapper.updateCommerceSmsCode(id, formatted);
 			}
			if (sendSMS(idApp, idPais, idioma, telefono, formatted, id)) {
				
				response.setMensajeError("Mensaje enviado con exito.");
				LOGGER.info("RE ENVIANDO SMS AL USUARIO EXITOSAMENTE - {} - {}", id, formatted);
			} else {
				response.setIdError(-1);
				response.setMensajeError("Ocurrio un error al enviar el mensaje.");
				LOGGER.info("NO SE ENVIO CORRECTAMENTE EL MENSAJE - {} - {}", id, formatted);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError("Ocurrio un error al enviar el mensaje.");
			LOGGER.info("OCURRIO UN ERROR AL ENVIAR EL MENSAJE - {}", id);
		}
		return response;
	}
	
	private ThreatMetrixResponse validateUser(TUsuarios usuario, String sessionId) {
		ThreatMetrixRequest request = new ThreatMetrixRequest();
		ThreatMetrixResponse validate = null;
		try {
			LOGGER.info("DATOS  DE USUARIO: "+usuario.getUsrNombre() + " "+ usuario.getUsrApellido()+
					", CORREO: "+usuario.getUsrLogin()+", TELEFONO: "+usuario.getUsrTelefono()+
					", USERNAME: "+usuario.getUsrLogin());
			request.setApplicationName("mobilecardmx");
			request.setCorreo(usuario.getUsrLogin());
			request.setNombre(usuario.getUsrNombre() + " "+ usuario.getUsrApellido());
			request.setSessionId(sessionId);
			request.setTelefono(usuario.getUsrTelefono());
			request.setUsername(usuario.getUsrLogin());
			validate = tmClient.investigatePayment(request);
		} catch (Exception e) {
			e.printStackTrace();
			validate = new ThreatMetrixResponse();
			validate.setIdError(500);
			validate.setMensajeError("No se puedo realizar la validacion, intente de nuevo. ");
		}
		LOGGER.info("RESPONSE VALIDATE USER THREATMETRIX - {}", gson.toJson(validate));
		return validate;
	}
	
	
	public BaseResponse addUserPhoto(int idPais, int idApp, String idioma, UserPhotoReq req){
		BaseResponse response = new BaseResponse();
		try{
			LOGGER.debug("GUARDANDO FOTO DE USUARIO {}",req.getIdUser());
			byte[] photo = req.getPhoto().getBytes();
			mapper.addUserPhoto(req.getIdUser(), idApp, idPais, photo);
			LOGGER.debug("FOTO GUARDAD CON EXITO");
			
			response.setIdError(0);
			if(idioma.equals("es"))
				response.setMensajeError("LA IMAGEN SE GUARDÓ CORRECTAMENTE");
			else
				response.setMensajeError("THE IMAGE WAS KEPT CORRECTLY");
			
		}catch(Exception e){
			LOGGER.error("ERROR AL GUARDAR FOTO " + req.getIdUser(), e);
			response.setIdError(300);
			if(idioma.equals("es"))
				response.setMensajeError("ERROR AL GUARDAR IMAGEN, INTENTE DE NUEVO");
			else
				response.setMensajeError("ERROR WHEN SAVING IMAGE, TRY AGAIN");
		}
		return response;
	
	}
	public BaseResponse updateUserPhoto(int idPais, int idApp, String idioma, UserPhotoReq req){
		BaseResponse response = new BaseResponse();
		try{
			LOGGER.debug("ACTUALIZANDO FOTO DE USUARIO {}",req.getIdUser());
			byte[] photo = req.getPhoto().getBytes();
			mapper.updateUserPhoto(req.getIdUser(), idApp, idPais, photo);
			LOGGER.debug("FOTO ACTUALIZADA CON EXITO");
			
			response.setIdError(0);
			if(idioma.equals("es"))
				response.setMensajeError("LA IMAGEN SE ACTUALIZÓ CORRECTAMENTE");
			else
				response.setMensajeError("THE IMAGE WAS UPDATED CORRECTLY");
			
		}catch(Exception e){
			LOGGER.error("ERROR AL GUARDAR FOTO " + req.getIdUser(), e);
			response.setIdError(300);
			if(idioma.equals("es"))
				response.setMensajeError("ERROR AL ACTUALIZAR IMAGEN, INTENTE DE NUEVO");
			else
				response.setMensajeError("ERROR UPDATING IMAGE, TRY AGAIN");
		}
		return response;
		
	}
	
	public PreRegistro preRegistro(int idApp, int idPais, String tipoUsuario, String idioma, String email,String imei, String telefono){
		PreRegistro response = new PreRegistro();
		String json = "";
		try{
			LOGGER.debug("INICIANDO PROCESO DE ENVIO DE VERIFICACION DE CORREO {} {}",tipoUsuario, email  );
			
			 if(tipoUsuario.toUpperCase().equals("USUARIO"))
				 json = mapper.preRegistro(0l, idApp, idPais, tipoUsuario, idioma, 1, email,imei,telefono);
			 else if(tipoUsuario.toUpperCase().equals("COMERCIO"))
				 json = mapper.preRegistro(0l, idApp, idPais, tipoUsuario, idioma, 3, email,imei,telefono);
			 
			LOGGER.debug("RESPUESTA DE BASE DE DATOS : "+ json);
			response = gson.fromJson(json, PreRegistro.class);
			
			if(response.getIdError() == 0){
				
				String value = AddcelCrypto.encryptHard(email) + ":" + AddcelCrypto.encryptHard(response.getId()+"");
				String param = "?codigo=" + Base64.encode(value.getBytes());
				/*String url = "https://www.mobilecard.mx/Usuarios/{idApp}/{idPais}/{idioma}/{tipoUsuario}/activate".replace("{idioma}", idioma)
						      .replace("{idApp}", idApp+"").replace("{idPais}", idPais+"").replace("{tipoUsuario}", tipoUsuario) + param;*/
				String url = Constantes.ACTIVATION_URL.replace("{idioma}", idioma)
					      .replace("{idApp}", idApp+"").replace("{idPais}", idPais+"").replace("{tipoUsuario}", tipoUsuario) + param;
				url = url.replace("{idApp}", String.valueOf(idApp));
				LOGGER.debug("[ACTIVATE URL] " + url);
				
				TemplateCorreo correo = mapper.getTemplateMail("@MENSAJE_PRE_REGISTRO", idioma, idApp);	
				UtilsMC.emailSend(email, correo.getBody(),correo.getAsunto(), email, url, "", "", new String[] { Constantes.Path_images + "user_registry.PNG" });
				//UtilsMC.emailSend(email, correo.getBody(),correo.getAsunto(), email, "", "","",new String[] { Constantes.Path_images + "user_registry.PNG" });
				
			}else{
				LOGGER.debug("NO SE PUDO ENVIAR EL CORREO PRE REGISTRO");
			}
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR EMAIL PRE-REGISTRO",ex);
			response.setIdError(1000);
			response.setMensajeError("ERROR AL ENVIAR CORREO, CONTACTE A SOPORTE");
		}
		return response;
	}
	
	public PreRegistro verifyUserCommerce(int idApp, int idPais, String tipoUsuario, String idioma, long id){
		PreRegistro response = new PreRegistro();
		try{
			LOGGER.debug("INICIANDO PROCESO DE VERIFICACION {} {}" , tipoUsuario, id);
			String json = mapper.preRegistro(id, idApp, idPais, tipoUsuario, idioma, 5, "","","");
			LOGGER.debug("RESPUESTA BASE DE DATOS " + json);
			response = gson.fromJson(json, PreRegistro.class);
		}catch(Exception ex){
			LOGGER.error("ERROR AL VERIFICAR CUENTA  " +tipoUsuario + " " + id ,ex);
			response.setIdError(1000);
			response.setMensajeError("ERROR AL VERIFICAR CUENTA USUARIO, CONTACTE A SOPORTE");
		}
		return response;
	}
	
	public boolean isActive(Integer idApp){
		boolean active = true;
		String band = null;
		//if(idApp == 4){
		band = mapper.isAppActive(idApp);
		if(band != null)
			if(band.equals("T"))
				active = true;
			else
				active = false;
		//}
		if(active)
			LOGGER.debug("SERVICIO AUTORIZADO PARA ID {}", idApp);
		else
			LOGGER.debug("SERVICIO DENEGADO PARA ID {}", idApp);
		return active;
	}
	
	public Integer isPeru() {
		int result = 0;
		try {
			result = mapper.isPeru();
			LOGGER.debug("id Perú: "+result);
		} catch (Exception e) {
			LOGGER.error("ERROR AL OBTENER EL ID DE PERÚ  " +e.getLocalizedMessage());
		}
		return result;
	}
	
}