package com.addcel.usuarios.spring.model;


import com.addcel.usuarios.model.vo.CardRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginCommerce{

	private Double comisionFija;
	private Double comisionPorcentaje;
	private String cuentaClabe;
	private String direccionEstablecimiento;
	private String email;
	private Long idAccount;
	private String idBanco;
	private Long idError;
	private Long idEstablecimiento;
	private Long idUsrStatus;
	private String mensajeError;
	private Boolean negocio;
	private String nombreEstablecimiento;
	private String nombrePropCuenta;
	private String razonSocial;
	private String representanteLegal;
	private String telefono;
	private String urlLogo;
	private CardRequest card;
	private String jumioStatus;
	private String scanReference;
	private Integer idPais;
	private String qrTag;
	private String qrBase64;
	private String tipo_persona;
	
	
	public LoginCommerce() {
		// TODO Auto-generated constructor stub
	}
	
	public String getQrTag() {
		return qrTag;
	}



	public void setQrTag(String qrTag) {
		this.qrTag = qrTag;
	}



	public String getQrBase64() {
		return qrBase64;
	}



	public void setQrBase64(String qrBase64) {
		this.qrBase64 = qrBase64;
	}



	public String getTipo_persona() {
		return tipo_persona;
	}



	public void setTipo_persona(String tipo_persona) {
		this.tipo_persona = tipo_persona;
	}



	public Double getComisionFija() {
		return comisionFija;
	}
	public void setComisionFija(Double comisionFija) {
		this.comisionFija = comisionFija;
	}
	public Double getComisionPorcentaje() {
		return comisionPorcentaje;
	}
	public void setComisionPorcentaje(Double comisionPorcentaje) {
		this.comisionPorcentaje = comisionPorcentaje;
	}
	public String getCuentaClabe() {
		return cuentaClabe;
	}
	public void setCuentaClabe(String cuentaClabe) {
		this.cuentaClabe = cuentaClabe;
	}
	public String getDireccionEstablecimiento() {
		return direccionEstablecimiento;
	}
	public void setDireccionEstablecimiento(String direccionEstablecimiento) {
		this.direccionEstablecimiento = direccionEstablecimiento;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getIdAccount() {
		return idAccount;
	}
	public void setIdAccount(Long idAccount) {
		this.idAccount = idAccount;
	}
	public String getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(String idBanco) {
		this.idBanco = idBanco;
	}
	public Long getIdError() {
		return idError;
	}
	public void setIdError(Long idError) {
		this.idError = idError;
	}
	public Long getIdEstablecimiento() {
		return idEstablecimiento;
	}
	public void setIdEstablecimiento(Long idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}
	public Long getIdUsrStatus() {
		return idUsrStatus;
	}
	public void setIdUsrStatus(Long idUsrStatus) {
		this.idUsrStatus = idUsrStatus;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public Boolean getNegocio() {
		return negocio;
	}
	public void setNegocio(Boolean negocio) {
		this.negocio = negocio;
	}
	public String getNombreEstablecimiento() {
		return nombreEstablecimiento;
	}
	public void setNombreEstablecimiento(String nombreEstablecimiento) {
		this.nombreEstablecimiento = nombreEstablecimiento;
	}
	public String getNombrePropCuenta() {
		return nombrePropCuenta;
	}
	public void setNombrePropCuenta(String nombrePropCuenta) {
		this.nombrePropCuenta = nombrePropCuenta;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getRepresentanteLegal() {
		return representanteLegal;
	}
	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getUrlLogo() {
		return urlLogo;
	}
	public void setUrlLogo(String urlLogo) {
		this.urlLogo = urlLogo;
	}

	public CardRequest getCard() {
		return card;
	}

	public void setCard(CardRequest card) {
		this.card = card;
	}

	public String getJumioStatus() {
		return jumioStatus;
	}

	public void setJumioStatus(String jumioStatus) {
		this.jumioStatus = jumioStatus;
	}

	public String getScanReference() {
		return scanReference;
	}

	public void setScanReference(String scanReference) {
		this.scanReference = scanReference;
	}

	public Integer getIdPais() {
		return idPais;
	}

	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}
	
}
