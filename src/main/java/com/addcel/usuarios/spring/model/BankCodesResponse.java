package com.addcel.usuarios.spring.model;

import java.util.List;

import com.addcel.usuarios.model.vo.BankCodes;
import com.addcel.usuarios.model.vo.BaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BankCodesResponse extends BaseResponse{

	private List<BankCodes> banks;
	
	public BankCodesResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public void setBanks(List<BankCodes> banks) {
		this.banks = banks;
	}
	
	public List<BankCodes> getBanks() {
		return banks;
	}
	
}
