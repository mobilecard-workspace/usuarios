package com.addcel.usuarios.spring.model;

import com.addcel.usuarios.model.vo.Establecimiento;

public class UpdateCommerceRes extends Establecimiento{

	private int idError ;
	private String mensajeError;
	
	public UpdateCommerceRes() {
		// TODO Auto-generated constructor stub
	}
	
	public UpdateCommerceRes(int idError,String mensajeError) {
		// TODO Auto-generated constructor stub
		this.idError = idError;
		this.mensajeError = mensajeError;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	
	
}
