package com.addcel.usuarios.spring.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class NegocioCreateRequest {
	
	private String celular;
	private String nombreEstablecimiento;
	private String email;
	private String password;
	private String direccion;
	private String colonia;
	private String ciudad;
	private Integer idEstado;
	private String cp;
	private String  nombre;
	private String paterno;
	private String materno;
	private String clabe;
	private Integer idBanco;
	private String rfc;
	private String curp;
	
	
	//DATOS PARA COMERCIO COLOMBIA
	
	
	
	public NegocioCreateRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}




	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getColonia() {
		return colonia;
	}


	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public String getCp() {
		return cp;
	}


	public void setCp(String cp) {
		this.cp = cp;
	}


	public String getNombreEstablecimiento() {
		return nombreEstablecimiento;
	}


	public void setNombreEstablecimiento(String nombreEstablecimiento) {
		this.nombreEstablecimiento = nombreEstablecimiento;
	}


	public Integer getIdEstado() {
		return idEstado;
	}


	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getPaterno() {
		return paterno;
	}


	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}


	public String getMaterno() {
		return materno;
	}


	public void setMaterno(String materno) {
		this.materno = materno;
	}


	public String getClabe() {
		return clabe;
	}


	public void setClabe(String clabe) {
		this.clabe = clabe;
	}


	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	
	public Integer getIdBanco() {
		return idBanco;
	}
	
	

}
