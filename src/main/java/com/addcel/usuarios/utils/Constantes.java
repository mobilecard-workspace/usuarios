package com.addcel.usuarios.utils;

public class Constantes {
        
        public static final String FORMATO_ENCRIPT = "ddhhmmssSSS";
        
	public static final String LOG_APP = "[GESTION USUARIOS] - ";

	public static final String LOG_RESPUESTA = "[RESPUESTA]";
	
	public static final String LOG_ERROR = "[ERROR]";

	public static final String LOG_SERVICE = "[SERVICIO]";

	public static final String LOG_PROCESO_TESTING = "[TESTING SERVICES]";

	public static final String LOG_PROCESO_ALTA_USUARIO = "[ALTA USUARIO]";

	public static final String LOG_PROCESO_ACTUALIZA_USUARIO = "[ACTUALIZA USUARIO]";

	public static final String LOG_PROCESO_LOGIN_USUARIO = "[LOGIN USUARIO]";
	
	public static final String LOG_PROCESO_BORRAR_USUARIO = "[BORRADO LOGICO DE USUARIO]";
	
	public static final String LOG_PROCESO_CAMBIO_PASSWORD = "[CAMBIO DE PASSWORD]";
	
	public static final String LOG_PROCESO_RESET_PASSWORD = "[RESET DE PASSWORD]";
	
	public static final String LOG_PROCESO_ACTUALIZACION_TARJETA = "[ACTUALIZACION DE TARJETA]";

	public static final String JSON_VACIO = "{\"idError\":99,\"mensajeError\":\"El JSON no puede venir vacio.\"}";
	
	public static final String ERROR = "[ERROR]";

	public static final String ERROR_INESPERADO_ALTA_USUARIO = "{\"idError\":100,\"mensajeError\":\"Ocurrio un error inesperado al crear el usuario. Por favor, intente nuevamente\"}";

	public static final String ERROR_RESPUESTA_URL_MODULO_SIN_REGISTROS = "{\"idError\":200,\"mensajeError\":\"No existen registros para esa aplicacion y modulo.\"}";
	
	public static final String ERROR_INESPERADO_ACTUALIZANDO_USUARIO = "{\"idError\":300,\"mensajeError\":\"Ocurrio un error inesperado al actualizar los datos de usuario. Por favor, intente nuevamente\"}";
	
	public static final String ERROR_LOGIN_USUARIO = "{\"idError\":400,\"mensajeError\":\"Usuario no atenticado\"}";

	public static final String ERROR_INESPERADO_LOGIN_USUARIO = "{\"idError\":500,\"mensajeError\":\"Ocurrio un error inesperado al realizar la autenticacion del usuario. Por favor, intente nuevamente\"}";

	public static final String ERROR_BORRAR_USUARIO = "{\"idError\":600,\"mensajeError\":\"Usuario no borrado\"}";

	public static final String ERROR_INESPERADO_BORRAR_USUARIO = "{\"idError\":700,\"mensajeError\":\"Ocurrio un error inesperado al realizar la autenticacion del usuario. Por favor, intente nuevamente\"}";

	public static final String ERROR_INESPERADO_CAMBIO_PASSWORD = "{\"idError\":800,\"mensajeError\":\"Ocurrio un error inesperado al realizar la actualizacion del password. Por favor, intente nuevamente\"}";
	
	public static final String ERROR_CAMBIO_PASSWORD = "{\"idError\":900,\"mensajeError\":\"Password no actualizado. Por favor, intente nuevamente\"}";
	
	public static final String ERROR_INESPERADO_RESET_PASSWORD = "{\"idError\":800,\"mensajeError\":\"Ocurrio un error inesperado al realizar el reseteo de password. Por favor, intente nuevamente\"}";;
	
	public static final String ERROR_RESET_PASSWORD = "{\"idError\":900,\"mensajeError\":\"No se pudo realizar el reseteo de su password. Por favor, intente nuevamente\"}";
	
	public static final String ERROR_ACTUALIZAR_TARJETA = "El número de tarjeta esta duplicado no se puede registrar.";
	
	public static final String FORMATO_FECHA_ENCRIPT = "ddhhmmssSSS";
	
	//public static final String ACTIVATION_URL = "http://199.231.160.203/Usuarios/{idApp}/{idPais}/{idioma}/{tipoUsuario}/activate";
	
	public static final String ACTIVATION_URL = "https://www.mobilecard.mx/Usuarios/{idApp}/{idPais}/{idioma}/{tipoUsuario}/activate";
	
	public static final String ACTIVATION_URL_COMMERCE = "https://www.mobilecard.mx/Usuarios/{idioma}/commerce/activate";
	//public static final String ACTIVATION_URL_COMMERCE = "http://199.231.160.203/Usuarios/{idioma}/commerce/activate";
	
	
	public static final String URL_SEND_MAIL = "http://localhost/MailSenderAddcel/enviaCorreoAddcel";  
	
	public static final String Path_images ="/usr/java/resources/images/Addcel/";
	
	public static final String PATH_PREVIVALE_WS = "http://192.168.75.51/PreviVale/api/3/1/es/";
	
	public static final String PREVIVALE_WS_ESTATUS_TARJETA = "estatusTarjetaNegocio";
	
	
}