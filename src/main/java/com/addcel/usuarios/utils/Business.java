package com.addcel.usuarios.utils;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;

import com.addcel.usuarios.services.EmailSenderClient;
import com.google.gson.Gson;

import crypto.Crypto;

public class Business {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Business.class);

    private static final String TelefonoServicio = "5525963513";
	//private DAO _DAO ;
	
	
	public static String digest(String text) {
		String digest = "";
		BigInteger bigIntDgst = null;
		
		try {
			LOGGER.debug("DIGEST cadena: " + text);
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(), 0, text.length());
			bigIntDgst = new BigInteger(1, md.digest());
			//digest = bigIntDgst.toString(16);
			digest = String.format("%040x", bigIntDgst);
		} catch (NoSuchAlgorithmException e) {
			LOGGER.debug("Error al encriptar - digest", e);
		}
		LOGGER.debug("DIGEST calculado: " + digest);
		return digest;
	}
	
	public static String formatoMontoProsa(String monto) {
		String varTotal = "000";
		String pesos = null;
		String centavos = null;
		if (monto.contains(".")) {
			pesos = monto.substring(0, monto.indexOf("."));
			centavos = monto.substring(monto.indexOf(".") + 1, monto.length());
			if (centavos.length() < 2) {
				centavos = centavos.concat("0");
			} else {
				centavos = centavos.substring(0, 2);
			}
			varTotal = pesos + centavos;
		} else {
			varTotal = monto.concat("00");
		}
		LOGGER.info("Monto a cobrar 3dSecure: " + varTotal);
		return varTotal;
	}
	
	 public static String getSMS(String Telefono) {
	        return Crypto.aesDecrypt(parsePass(TelefonoServicio), Telefono);
	    }
	 
	 public static String parsePass(String pass) {
	        int len = pass.length();
	        String key = "";

	        for (int i = 0; i < 32 / len; i++) {
	            key += pass;
	        }

	        int carry = 0;
	        while (key.length() < 32) {
	            key += pass.charAt(carry);
	            carry++;
	        }
	        return key;
	    }
	 
	 public static String formatoMontoPayworks(String monto){
			String varTotal = "000";
			String pesos = null;
	        String centavos = null;
			if(monto.contains(".")){
	            pesos=monto.substring(0, monto.indexOf("."));
	            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
	            if(centavos.length()<2){
	                centavos = centavos.concat("0");
	            }else{
	                centavos=centavos.substring(0, 2);
	            }            
	            varTotal=pesos+ "." + centavos;
	        }else{
	            varTotal=monto.concat(".00");
	        } 
			return varTotal;		
		}
	 
	 
	 public static boolean SendMailMicrosoft(String sMail, String Cadena, String wAsunto,HashMap<String, String> datos, String urlEmailSender) {
 	    boolean flag = false;
 	   
 	    try {

 	    	if(datos.containsKey("NOMBRE"))
 	    		Cadena = Cadena.replace("#NOMBRE#", datos.get("NOMBRE"));
 	    	if(datos.containsKey("FECHA"))
 	    		Cadena = Cadena.replace("#FECHA#", datos.get("FECHA"));
 	    	if(datos.containsKey("AUTBAN"))
 	    		Cadena = Cadena.replace("#AUTBAN#", datos.get("AUTBAN"));
 	    	if(datos.containsKey("IMPORTE"))
 	    		Cadena = Cadena.replace("#IMPORTE#", datos.get("IMPORTE"));
 	    	if(datos.containsKey("COMISION"))
 	    		Cadena = Cadena.replace("#COMISION#", datos.get("COMISION"));
 	    	if(datos.containsKey("TOTAL"))
 	    		Cadena = Cadena.replace("#TOTAL#", datos.get("TOTAL"));
 	    	if(datos.containsKey("PROPINA"))
 	    		Cadena = Cadena.replace("#PROPINA#", datos.get("PROPINA"));
 	    	if(datos.containsKey("@USUARIO"))
 	    		Cadena = Cadena.replace("@USUARIO", datos.get("@USUARIO"));
 	    	if(datos.containsKey("@PASSWORD")){
 	    		//LOGGER.debug("pass enviado: " + datos.get("@PASSWORD"));
 	    		Cadena = Cadena.replace("@PASSWORD", datos.get("@PASSWORD"));
 	    	}
 	      
 	    	
 	      
 	      
 	      MailSender correo = new MailSender();
 	      String from = "no-reply@addcel.com";
 	      String[] to = {sMail};
 	      correo.setCc(new String[] {});
 	      correo.setBody(Cadena);
 	      correo.setFrom(from);
 	      correo.setSubject(wAsunto);
 	      correo.setTo(to);
 	      correo.setCid(new String[]{Constantes.Path_images+"BanorteH2H.PNG"});
 	      
 	      Gson gson = new Gson();
 	      String json = gson.toJson(correo);
 	      LOGGER.info("antes de enviar mail");
 	     EmailSenderClient clientRest = new EmailSenderClient();
	      clientRest.SendMail(correo);
 	      //MailSender(json);
 	     // enviarCorreoMicrosoft(correo);
 	      LOGGER.info("despues de enviar mail");
 	      LOGGER.debug("mensaje hotmail enviado : " + sMail);
 	    } catch (Exception ex) {
 	    	LOGGER.error("Error al enviar email ", ex);
 	      LOGGER.error("Ocurrio un error al enviar email hotmail {}:  {}", sMail, ex);
 	    }
 	    return flag;
 	  }
 
 public static void MailSender(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			LOGGER.info("Iniciando proceso de envio email Microsoft. ");
			URL url = new URL(Constantes.URL_SEND_MAIL);
			LOGGER.info("Conectando con " + Constantes.URL_SEND_MAIL);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			LOGGER.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			LOGGER.info("Respuesta del servidor(envio email Microsoft) " + sb.toString());
		} catch (Exception ex) {
			LOGGER.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
 
 
 public static String removeAcentos(String input) {
	    // Cadena de caracteres original a sustituir.
	    String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
	    // Cadena de caracteres ASCII que reemplazarán los originales.
	    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
	    String output = input;
	    for (int i=0; i<original.length(); i++) {
	        // Reemplazamos los caracteres especiales.
	        output = output.replace(original.charAt(i), ascii.charAt(i));
	    }//for i
	    return output;
	}
 
 public static String encode(final String clearText) throws NoSuchAlgorithmException {
	    return new String(
	            Base64.getEncoder().encode(MessageDigest.getInstance("SHA-512").digest(clearText.getBytes(StandardCharsets.UTF_8))));
	}
 
 public static String encryptApp(String password, String text){
	  
     final String salt = "ff39a0df";//KeyGenerators.string().generateKey();
     TextEncryptor encryptor = Encryptors.text(password, salt);      
    
     String encryptedText = encryptor.encrypt(text);
     return encryptedText;
        
	}
	
	public static String decryptApp(String password, String text){
		
		final String salt = "ff39a0df";//KeyGenerators.string().generateKey();
		TextEncryptor decryptor = Encryptors.text(password, salt);
		String decryptedText = decryptor.decrypt(text);
     
		return decryptedText;
	}
	
}
