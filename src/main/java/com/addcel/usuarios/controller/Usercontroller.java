package com.addcel.usuarios.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.usuarios.model.vo.AbstractVO;
import com.addcel.usuarios.model.vo.BaseResponse;
import com.addcel.usuarios.model.vo.Establecimiento;
import com.addcel.usuarios.model.vo.InsertRequest;
import com.addcel.usuarios.model.vo.LoginRequest;
import com.addcel.usuarios.model.vo.PreRegistro;
import com.addcel.usuarios.model.vo.TUsuarios;
import com.addcel.usuarios.model.vo.TermResponse;
import com.addcel.usuarios.model.vo.UpdateResponse;
import com.addcel.usuarios.model.vo.UserPhotoReq;
import com.addcel.usuarios.model.vo.VerifyResponse;
import com.addcel.usuarios.services.UserServices;
import com.addcel.usuarios.spring.model.BankCodesResponse;
import com.addcel.usuarios.spring.model.UpdateCommerceRes;

@Controller
public class Usercontroller {
	
	private static final String USER_INSERT = "{idApp}/{idioma}/user/insert";
	
	private static final String LOGIN = "{idApp}/{idioma}/user/login";
	
	private static final String LOGIN_v2 = "{idApp}/{idPais}/{idioma}/user/login";
	
	private static final String VERIFY = "{idApp}/{idioma}/user/verify";
	
	private static final String PASSWORD_UPDATE = "{idApp}/{idioma}/{idUsuario}/password/update";
	
	private static final String PASSWORD_RESET = "{idApp}/{idioma}/password/reset";
	
	private static final String PASSWORD_RESET_CONFIRM_PASS = "{idApp}/{idioma}/pass/confirm";
	
	private static final String PASSWORD_RESET_NEW_PASS = "{idApp}/{idioma}/resetPass/newpass";
	
	private static final String PASSWORD_RESET_PASS_SUCCESS = "{idApp}/{idioma}/resetPass/success";
	
	private static final String USER_ACTIVATE = "{idApp}/{idioma}/user/activate";
	
	private static final String ACTIVATE_SEND_MAIL = "{idApp}/{idioma}/{idUsuario}/activate";
	
	private static final String USER_UPDATE = "{idApp}/{idioma}/{idUsuario}/{name}/update";
	
	private static final String USER_UPDATE_SCAN_REFERENCE = "{idApp}/{idioma}/{idPais}/user/updatereference";
	
	private static final String TERMINOS = "{idApp}/terminos/{idpais}/{idioma}";
	
	private static final String PRIVACIDAD = "{idApp}/privacidad/{idpais}/{idioma}";
	
	private static final String VALIDATE_SMS = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/{id}/sms/validate";
	
	private static final String RESEND_SMS = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/{id}/sms/resend";
	
	private static final String ADD_PHOTO_USER = "{idApp}/{idPais}/{idioma}/user/photo/add";
	
	private static final String UPDATE_PHOTO_USER = "{idApp}/{idPais}/{idioma}/user/photo/update";
	
	private static final String USER_BEFORE_REGISTRATION = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/emailVerification";

	private static final String USER_COMMERCE_ACTIVATE = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/activate";
	
	private static final String VERIFY_USER_COMMERCE = "{idApp}/{idPais}/{idioma}/{tipoUsuario}/verify";
	
	@Autowired
	UserServices userServices;
	
	@RequestMapping(value = USER_INSERT, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TUsuarios> UserInsert(@PathVariable Integer idApp, @PathVariable String idioma, 
			@RequestBody InsertRequest request,  HttpServletRequest req) {
		if(userServices.isActive(idApp)){
			if(request.getCountry() != userServices.isPeru()) {
				TUsuarios user = userServices.UserInsert(idApp, request, idioma, req.getHeader("Profile"));
				return new ResponseEntity<TUsuarios>(user,HttpStatus.OK);
			}else {
				return new ResponseEntity<TUsuarios>((TUsuarios)new AbstractVO(401, "Actualmente no se pueden registrar nuevos usuarios en Perú"),HttpStatus.UNAUTHORIZED);
			}
		}else{
			return new ResponseEntity<TUsuarios>((TUsuarios)new AbstractVO(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = USER_INSERT+"v3", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TUsuarios> UserInsertV3(@PathVariable Integer idApp, @PathVariable String idioma, 
			@RequestBody InsertRequest request,  HttpServletRequest req) {
		if(userServices.isActive(idApp) && request.getCountry() != userServices.isPeru()){
			TUsuarios user = userServices.UserInsert_v2Nuevo(idApp, request, idioma, req.getHeader("Profile"));//userServices.UserInsert(idApp, request, idioma, req.getHeader("Profile"));
			return new ResponseEntity<TUsuarios>(user,HttpStatus.OK);
		}else{
			return new ResponseEntity<TUsuarios>((TUsuarios)new AbstractVO(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = USER_INSERT+"v2", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<Object> addUserOrComercio(@PathVariable Integer idApp, @PathVariable String idioma, 
			@RequestBody Object request, HttpServletRequest req) {
		if(userServices.isActive(idApp)){
			return new ResponseEntity<Object>(userServices.addUserOrComercio(idApp, request, idioma, req.getHeader("Profile")), HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(new AbstractVO(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = USER_INSERT+"v4", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<Object> addUserOrComercioV4(@PathVariable Integer idApp, @PathVariable String idioma, 
			@RequestBody Object request, HttpServletRequest req) {
		if(userServices.isActive(idApp)){
			return new ResponseEntity<Object>(userServices.addUserOrComercioV2Nuevo(idApp, request, idioma, req.getHeader("Profile")), HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(new AbstractVO(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	/*@RequestMapping(value = USER_INSERT+"v2", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<Object> addFullUserOrComercio(@PathVariable Integer idApp,@PathVariable Integer idPais, @PathVariable String idioma, @RequestBody Object request) {
		return new ResponseEntity<Object>(userServices.AddUserMxCol(idApp, idPais, request, idioma), HttpStatus.OK);
	}*/
	
	@RequestMapping(value = LOGIN, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TUsuarios> login(@PathVariable Integer idApp, @PathVariable String idioma, @RequestBody LoginRequest request){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<TUsuarios>(userServices.userLogin(idApp, request, idioma),HttpStatus.OK);
		}else{
			return new ResponseEntity<TUsuarios>((TUsuarios)new AbstractVO(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = LOGIN_v2, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<Object> login(@PathVariable Integer idApp, @PathVariable Integer idPais, @PathVariable String idioma, @RequestBody LoginRequest request){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<Object>(userServices.login(idApp, idPais, request, idioma),HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(new AbstractVO(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = VERIFY, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<VerifyResponse> verify(@PathVariable Integer idApp, @PathVariable String idioma, @RequestParam("idUsuario") long idUsuario){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<VerifyResponse>(userServices.statusVerify(idApp, idUsuario, idioma),HttpStatus.OK);
		}else{
			return new ResponseEntity<VerifyResponse>((VerifyResponse)new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	
	@RequestMapping(value = PASSWORD_UPDATE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public  ResponseEntity<BaseResponse> PasswordUpdate(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable String idUsuario, @RequestParam("old") String pass_old,  @RequestParam("new") String pass_new){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<BaseResponse>(userServices.passwordUpdate(idApp, idioma, idUsuario, pass_old, pass_new),HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	/*@RequestMapping(value = PASSWORD_RESET, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> PasswordReset(@PathVariable Integer idApp, @PathVariable String idioma,@RequestParam("userOrEmail") String userOrEmail){
		return new ResponseEntity<BaseResponse>(userServices.passwordReset(idApp, userOrEmail, idioma),HttpStatus.OK);
	}*/
	//Envio de correo para reestablecer contraseña  {idioma}/password/reset
	@RequestMapping(value=PASSWORD_RESET, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> sendMailPassreset(@PathVariable Integer idApp,@PathVariable String idioma,@RequestParam("userOrEmail") String userOrEmail){
		if(userServices.isActive(idApp)){	
			return new ResponseEntity<BaseResponse>(userServices.sendMailPasswordReset(userOrEmail, idioma,idApp),HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	//CONFIRMAR CORREO ELECTRONICO
	@RequestMapping(value=PASSWORD_RESET_CONFIRM_PASS, method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ModelAndView reset(@PathVariable Integer idApp,@RequestParam(value = "error", required=false) String error, @RequestParam("cod") String cod,@PathVariable String idioma){
		if(userServices.isActive(idApp)){	
			return userServices.emailConfirm(cod, error, idioma,idApp);
		}else{
			ModelAndView view = new ModelAndView("/"+idioma+"/pass/confirm");
    			view.addObject("error","No estas autorizado para realizar la operacion");
    		return view;
		}
	}
	
	//despues de confirmacion de correo
	@RequestMapping(value=PASSWORD_RESET_NEW_PASS, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ModelAndView validEmail(@PathVariable Integer idApp,@PathVariable String idioma,@RequestParam("email") String email,@RequestParam("cod") String cod){
		if(userServices.isActive(idApp)){	
			return userServices.validEmail(cod, idioma, email,idApp);
		}else{
			ModelAndView view = new ModelAndView("user/confirmEmail");
    			view.addObject("error","No estas autorizado para realizar la operacion");
    		return view;
		}
	}
	
	//final reset pass
	@RequestMapping(value=PASSWORD_RESET_PASS_SUCCESS, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ModelAndView successResetPass(@PathVariable Integer idApp,@PathVariable String idioma,@RequestParam("pass") String pass, @RequestParam("cpass") String cpass,@RequestParam("cod") String cod){
		if(userServices.isActive(idApp)){		
			return userServices.resetPass(cod, cpass, idioma, idApp);
		}else{
			ModelAndView view = new ModelAndView("user/resetPSuccess");
    			view.addObject("error","No estas autorizado para realizar la operacion");
    		return view;
		}
	}
	
	
	/**ACTIVAR CUENTA DE USUARIO**/
	@RequestMapping(value = USER_ACTIVATE, method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ModelAndView Useractivate(@PathVariable Integer idApp, @PathVariable String idioma, @RequestParam("codigo") String cadena){
		if(userServices.isActive(idApp)){	
			return userServices.userActivate(idApp, cadena, idioma);
		}else{
			ModelAndView view = new ModelAndView("activate_error");
    			view.addObject("error","No estas autorizado para realizar la operacion");
    		return view;
		}
		
	}
	
	/**ENVIAR CORREO PARA CONFIRMACION DE CUENTA**/
	@RequestMapping(value = ACTIVATE_SEND_MAIL, method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public  ResponseEntity<BaseResponse> ActivationSendMail(@PathVariable Integer idApp, @PathVariable String idioma,  @PathVariable long idUsuario){
		if(userServices.isActive(idApp)){	
			return new ResponseEntity<BaseResponse>(userServices.ActivationSendMail(idApp, idioma, idUsuario),HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = USER_UPDATE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<UpdateResponse> UserUpdate(@PathVariable Integer idApp, @PathVariable String idioma,@PathVariable String name, @PathVariable long idUsuario, @RequestParam("value") String value){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<UpdateResponse>(userServices.userUpdateInfo(idApp, idioma, name, idUsuario,"", value),HttpStatus.OK);
		}else{
			return new ResponseEntity<UpdateResponse>((UpdateResponse)new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = USER_UPDATE_SCAN_REFERENCE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> UserUpdateScanReference(@PathVariable Integer idApp,@PathVariable Integer idPais, @PathVariable String idioma, 
			@RequestParam("login") String login,@RequestParam("scanReference") String scanReference){
		if(userServices.isActive(idApp)){
			UpdateResponse res = userServices.userUpdateInfo(idApp,idioma,"jumio_reference",0,login,scanReference);
			BaseResponse response = new BaseResponse(res.getIdError(), res.getMensajeError());
			return new ResponseEntity<BaseResponse>(response,HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	
	@RequestMapping(value = TERMINOS, method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TermResponse> getTerminos(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable int idpais, HttpServletRequest req){
		String pass = req.getHeader("client");
		if(userServices.isActive(idApp)){
			return new ResponseEntity<TermResponse>(userServices.getTerms(idApp, idioma, idpais, pass),HttpStatus.OK);
		}else{
			return new ResponseEntity<TermResponse>((TermResponse)new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PRIVACIDAD, method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ResponseEntity<TermResponse> getPrivacidad(@PathVariable Integer idApp,@PathVariable String idioma, @PathVariable int idpais, HttpServletRequest req){
		String pass = req.getHeader("client");
		if(userServices.isActive(idApp)){
			return new ResponseEntity<TermResponse>(userServices.getPrivacy(idioma, idpais, pass),HttpStatus.OK);
		}else{
			return new ResponseEntity<TermResponse>((TermResponse)new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	
	@RequestMapping(value = "{idApp}/{idioma}/commerce/update",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<UpdateCommerceRes> AdminUpdate(@RequestBody Establecimiento est, @PathVariable String idioma, @PathVariable int idApp){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<UpdateCommerceRes>(userServices.AdminUpdate(est, idioma, idApp),HttpStatus.OK);
		}else{
			return new ResponseEntity<UpdateCommerceRes>(new UpdateCommerceRes(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	//ACTUALIAZAR PASS DE ADMIN
	@RequestMapping(value = "{idApp}/{idioma}/commerce/passUpdate",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> AdminUpdatePass(@PathVariable("idApp") int idApp,@PathVariable String idioma, @RequestParam("userAdmin") long idAdmin,  @RequestParam("newPass") String newPass){
		if(userServices.isActive(idApp)){	
			return new ResponseEntity<BaseResponse>(userServices.adminPassUpdate(idioma, idAdmin, newPass),HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	/*@RequestMapping(value = "{idApp}/{idioma}/commerce/passReset",method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> AdminResetPass(@PathVariable String idioma, @RequestParam("userAdmin") String idAdmin, @PathVariable("idApp") int idApp){
				
			return new ResponseEntity<BaseResponse>(userServices.AdminResetPass(idioma, idAdmin, idApp),HttpStatus.OK);
		}*/
	
	@RequestMapping(value = "{idApp}/{idPais}/{idioma}/bankCodes",method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public  ResponseEntity<BankCodesResponse> getBankCodes(@PathVariable String idioma, @PathVariable("idApp") int idApp, 
			@PathVariable("idPais") int idPais){
		if(userServices.isActive(idApp)){
		return new  ResponseEntity<BankCodesResponse>(userServices.getBankCodes(idPais),HttpStatus.OK);
		}else{
			return new ResponseEntity<BankCodesResponse>((BankCodesResponse)new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	/** VALIDAR SMS - USUARIO **/
	@RequestMapping(value = VALIDATE_SMS, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<Object> validateSMS(@PathVariable Integer idApp, @PathVariable String idioma, 
			@PathVariable Integer idPais, @PathVariable String tipoUsuario, @PathVariable long id,  @RequestParam("codigo") String cadena){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<Object>(userServices.validateSMS(idApp, idPais, cadena, idioma, tipoUsuario, id), HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = RESEND_SMS, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> resendSMS(@PathVariable Integer idApp, @PathVariable String idioma, 
			@PathVariable Integer idPais, @PathVariable String tipoUsuario, @PathVariable long id){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<BaseResponse>(userServices.resendSMS(idApp, idPais, idioma, tipoUsuario, id), HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	//*********AÑADIR FOTO USUARIO*************//
	@RequestMapping(value = ADD_PHOTO_USER, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> AddUserPhoto(@PathVariable Integer idApp, @PathVariable String idioma, 
			@PathVariable Integer idPais, @RequestBody UserPhotoReq req){
		if(userServices.isActive(idApp)){
		return new ResponseEntity<BaseResponse>(userServices.addUserPhoto(idPais, idApp, idioma, req), HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = UPDATE_PHOTO_USER, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> updateUserPhoto(@PathVariable Integer idApp, @PathVariable String idioma, 
			@PathVariable Integer idPais, @RequestBody UserPhotoReq req){
		if(userServices.isActive(idApp)){
			return new ResponseEntity<BaseResponse>(userServices.updateUserPhoto(idPais, idApp, idioma, req), HttpStatus.OK);
		}else{
			return new ResponseEntity<BaseResponse>(new BaseResponse(401, "No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = USER_BEFORE_REGISTRATION, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<PreRegistro> sendActivationEmail(@PathVariable Integer idApp, @PathVariable String idioma, 
			@PathVariable Integer idPais, @PathVariable String tipoUsuario, @RequestParam("email") String email,@RequestParam("telefono") String telefono, @RequestParam("imei") String imei){
		
		if(userServices.isActive(idApp)){
			return new ResponseEntity<PreRegistro>(userServices.preRegistro(idApp, idPais, tipoUsuario, idioma, email, imei, telefono), HttpStatus.OK);
		}else{
			return new ResponseEntity<PreRegistro>(new PreRegistro(new BaseResponse(401, "No estas autorizado para realizar la operacion")),HttpStatus.UNAUTHORIZED);
		}
		
	}
	
	//confirmar correo usuario/commercio
	@RequestMapping(value = USER_COMMERCE_ACTIVATE, method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ModelAndView UserCommerceactivate(@PathVariable Integer idApp, @PathVariable Integer idPais, @PathVariable String idioma,@PathVariable String tipoUsuario, @RequestParam("codigo") String cadena){
		if(userServices.isActive(idApp)){	
			return userServices.userCommerceActivate(idApp, idPais, tipoUsuario, cadena, idioma);
		}else{
			ModelAndView view = new ModelAndView("activate_error");
    			view.addObject("error","No estas autorizado para realizar la operacion");
    		return view;
		}
		
	}
	
	@RequestMapping(value = VERIFY_USER_COMMERCE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<PreRegistro> sendActivationEmail(@PathVariable Integer idApp, @PathVariable String idioma, 
			@PathVariable Integer idPais, @PathVariable String tipoUsuario, @RequestParam("id") long id){
		
		if(userServices.isActive(idApp)){
			return new ResponseEntity<PreRegistro>(userServices.verifyUserCommerce(idApp, idPais, tipoUsuario, idioma, id), HttpStatus.OK);
		}else{
			return new ResponseEntity<PreRegistro>(new PreRegistro(new BaseResponse(401, "No estas autorizado para realizar la operacion")),HttpStatus.UNAUTHORIZED);
		}
		
	}
	
	
}
