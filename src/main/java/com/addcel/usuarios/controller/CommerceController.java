package com.addcel.usuarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.usuarios.model.vo.BaseResponse;
import com.addcel.usuarios.model.vo.VerifyResponseCommerce;
import com.addcel.usuarios.services.CommerceService;
import com.addcel.usuarios.spring.model.NegocioCreateRequest;
import com.addcel.usuarios.spring.model.NegocioCreateResponse;

@Controller
public class CommerceController {

	private static final String PASSWORD_RESET = "{idApp}/{idioma}/commerce/passReset";
	
	private static final String PASSWORD_RESET_CONFIRM_PASS = "{idApp}/{idioma}/commerce/pass/confirm";
	
	private static final String PASSWORD_RESET_NEW_PASS = "{idApp}/{idioma}/commerce/resetPass/newpass";
	
	private static final String PASSWORD_RESET_PASS_SUCCESS = "{idApp}/{idioma}/commerce/resetPass/success";
	
	private static final String VERIFY_COMMERCE = "{idApp}/{idioma}/commerce/verify";
	
	private static final String SEND_EMAIL_COMMERCE = "{idApp}/{idioma}/commerce/email/resend";
	
	private static final String UPDATE_REFERENCE_COMMERCE = "{idApp}/{idPais}/{idioma}/commerce/update/reference";
	
	private static final String REGISTER_COMMERCE = "{idApp}/{idPais}/{idioma}/comercio/alta";
		
	@Autowired
	CommerceService Cservice;
	
	/**ACTIVAR CUENTA DE USUARIO Establecimiento**/
	@RequestMapping(value = "{idioma}/commerce/activate", method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
	public ModelAndView Useractivate(@PathVariable String idioma, @RequestParam("codigo") String cadena){
		return Cservice.userActivate(cadena, idioma);
	}
	
	@RequestMapping(value = PASSWORD_RESET,method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> AdminResetPass(@PathVariable String idioma, @RequestParam("userAdmin") String userAdmin, @PathVariable("idApp") int idApp){
				
			return new ResponseEntity<BaseResponse>(Cservice.sendMailPasswordReset(userAdmin, idioma, idApp),HttpStatus.OK);
		}
	
	//CONFIRMAR CORREO ELECTRONICO
		@RequestMapping(value=PASSWORD_RESET_CONFIRM_PASS, method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
		public ModelAndView reset(@PathVariable Integer idApp,@RequestParam(value = "error", required=false) String error, @RequestParam("cod") String cod,@PathVariable String idioma){
			
			return Cservice.emailConfirm(cod, error, idioma,idApp);
		}
		
		//despues de confirmacion de correo
		@RequestMapping(value=PASSWORD_RESET_NEW_PASS, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		public ModelAndView validEmail(@PathVariable Integer idApp,@PathVariable String idioma,@RequestParam("email") String email,@RequestParam("cod") String cod){
				
			return Cservice.validEmail(cod, idioma, email,idApp);
		}
		
		//final reset pass
		@RequestMapping(value=PASSWORD_RESET_PASS_SUCCESS, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		public ModelAndView successResetPass(@PathVariable Integer idApp,@PathVariable String idioma,@RequestParam("pass") String pass, @RequestParam("cpass") String cpass,@RequestParam("cod") String cod){
						
			return Cservice.resetPass(cod, cpass, idioma, idApp);
		}
		
		@RequestMapping(value = VERIFY_COMMERCE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		public ResponseEntity<VerifyResponseCommerce> verify(@PathVariable Integer idApp, @PathVariable String idioma, 
				@RequestParam("id") long idUsuario){
			return new ResponseEntity<VerifyResponseCommerce>(Cservice.statusVerify(idApp, idUsuario, idioma),HttpStatus.OK);
		}
		
		@RequestMapping(value = SEND_EMAIL_COMMERCE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		public ResponseEntity<BaseResponse> sendEMailCommerce(@PathVariable Integer idApp, @PathVariable String idioma, 
				@RequestParam("id") long id){
			return new ResponseEntity<BaseResponse>(Cservice.sendEMailCommerce(idApp, id, idioma),HttpStatus.OK);
		}
		
		@RequestMapping(value = UPDATE_REFERENCE_COMMERCE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		public ResponseEntity<BaseResponse> updateReferenceCommerce(@PathVariable Integer idApp,@PathVariable Integer idPais, @PathVariable String idioma, 
				@RequestParam("login") String login,@RequestParam("scanReference") String scanReference){
			return new ResponseEntity<BaseResponse>(Cservice.updateReferenceCommerce(idApp, idPais, idioma, login, scanReference),HttpStatus.OK);
		}
		
		@RequestMapping(value = REGISTER_COMMERCE, method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
		public ResponseEntity<NegocioCreateResponse> updateReferenceCommerce(@PathVariable Integer idApp,@PathVariable Integer idPais, @PathVariable String idioma,@RequestBody NegocioCreateRequest req){
			if(!idPais.equals(Cservice.isPeru())) {
				return new ResponseEntity<NegocioCreateResponse>(Cservice.CommerceRegister(req, idApp, idPais, idioma),HttpStatus.OK);
			}else {
				return new ResponseEntity<NegocioCreateResponse>(new NegocioCreateResponse(), HttpStatus.UNAUTHORIZED);
			}
		}
		
		
}