package com.addcel.usuarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.addcel.usuarios.model.vo.BalanceResponse;
import com.addcel.usuarios.model.vo.BaseResponse;
import com.addcel.usuarios.model.vo.Tag;
import com.addcel.usuarios.model.vo.TagResponse;
import com.addcel.usuarios.services.TagServices;

@Controller
public class TagsController {

	private static final String GET_TAG = "/{idApp}/{idioma}/{idUsuario}/tags/";
	
	private static final String GET_TAG_BY_PROVIDER = "/{idApp}/{idioma}/{idUsuario}/tags/{provider}";
	
	private static final String DELETE_TAG = "/{idApp}/{idioma}/{idUsuario}/tags/{idTag}/delete";
	
	private static final String DELETE_TAG_BY_PROVIDER = "/{idApp}/{idioma}/{idUsuario}/tags/{provider}/{idTag}/delete";
	
	private static final String ADD_TAG = "/{idApp}/{idioma}/{idUsuario}/tags/add";
	
	private static final String ADD_TAG_BY_PROVIDER = "/{idApp}/{idioma}/{idUsuario}/tags/{provider}/add";
	
	private static final String UPDATE_TAG_BY_PROVIDER = "/{idApp}/{idioma}/{idUsuario}/tags/{provider}/update";
	
	private static final String CHECK_BALANCE_TAG = "/{idApp}/{idioma}/{idUsuario}/tags/checkBalance";
	
	@Autowired
	TagServices tagServices;

	// OBTENER TODOS LOS TAGS DE UN USUARIO
	@RequestMapping(value = GET_TAG, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<TagResponse> getTag(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario) {
		return new ResponseEntity<TagResponse>(tagServices.getTag(idApp, idUsuario, idioma, -1), HttpStatus.OK);
	}

	// OBTENER LOS TAGS POR PROVEEDOR DE UN USUARIO
	@RequestMapping(value = GET_TAG_BY_PROVIDER, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<TagResponse> getTagByProvider(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario,
			@PathVariable int provider) {
		return new ResponseEntity<TagResponse>(tagServices.getTag(idApp, idUsuario, idioma, provider), HttpStatus.OK);
	}
	
	// BORRAR TAG DE USUARIO
	@RequestMapping(value = DELETE_TAG, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<BaseResponse> deleteTag(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario,
			@PathVariable String idTag) {
		return new ResponseEntity<BaseResponse>(tagServices.deleteTag(idApp, idioma, idUsuario, idTag), HttpStatus.OK);
	}
	
	// BORRAR TAG DE USUARIO DEVOLVIENDO LOS TAGS ACTUALES POR PROVEEDOR
	@RequestMapping(value = DELETE_TAG_BY_PROVIDER, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public ResponseEntity<TagResponse> deleteTagByProvider(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario,
			@PathVariable int provider, @PathVariable String idTag) {
		return new ResponseEntity<TagResponse>(tagServices.deleteTagByProvider(idApp, idioma, idUsuario, idTag, provider),
				HttpStatus.OK);
	}

	// AGREGAR TAG A USUARIO REGRESA TODOS LOS TAGS
	@RequestMapping(value = ADD_TAG, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<TagResponse> addTag(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario,
			@RequestBody Tag tag) {
		return new ResponseEntity<TagResponse>(tagServices.addTag(idApp, idioma, idUsuario, tag, -1), HttpStatus.OK);
	}

	// GREGAR TAG A USUARIO REGRESA TAGS POR PROVIDER
	@RequestMapping(value = ADD_TAG_BY_PROVIDER, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<TagResponse> addTagByProovider(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario,
			@PathVariable int provider, @RequestBody Tag tag) {
		return new ResponseEntity<TagResponse>(tagServices.addTag(idApp, idioma, idUsuario, tag, provider), HttpStatus.OK);
	}

	private static final String UPDATE_TAG = "/{idApp}/{idioma}/{idUsuario}/tags/update";
	// ACTUALIZA TAG, REGRESA TODOS LOS TAGS
	@RequestMapping(value = UPDATE_TAG, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<TagResponse> updateTag(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario,
			@RequestBody Tag tag) {
		return new ResponseEntity<TagResponse>(tagServices.updateTag(idApp, idioma, idUsuario, tag, -1), HttpStatus.OK);
	}

	// ACTUALIZA TAG, REGRESA LOS TAGS POR PROVEEDOR
	@RequestMapping(value = UPDATE_TAG_BY_PROVIDER, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<TagResponse> updateTagByProvider(@PathVariable Integer idApp,@PathVariable String idioma, @PathVariable long idUsuario,
			@PathVariable int provider, @RequestBody Tag tag) {
		return new ResponseEntity<TagResponse>(tagServices.updateTag(idApp, idioma, idUsuario, tag, provider), HttpStatus.OK);
	}

	// CONSULTA EL SALDO DE UN TAG
	@RequestMapping(value = CHECK_BALANCE_TAG, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public ResponseEntity<BalanceResponse> checkBalanceTag(@PathVariable Integer idApp, @PathVariable String idioma, @PathVariable long idUsuario,
			@RequestBody Tag tag) {
		return new ResponseEntity<BalanceResponse>(tagServices.checkBalanceTag(idApp, idioma, idUsuario, tag), HttpStatus.OK);
	}

}
