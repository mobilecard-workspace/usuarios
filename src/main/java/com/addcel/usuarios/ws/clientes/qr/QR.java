package com.addcel.usuarios.ws.clientes.qr;

public class QR {

	private int code;
	private String message;
	private String qrTag;
	private String qrBase64;
	
	public QR() {
		// TODO Auto-generated constructor stub
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getQrTag() {
		return qrTag;
	}

	public void setQrTag(String qrTag) {
		this.qrTag = qrTag;
	}

	public String getQrBase64() {
		return qrBase64;
	}

	public void setQrBase64(String qrBase64) {
		this.qrBase64 = qrBase64;
	}
}
