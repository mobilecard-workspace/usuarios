package com.addcel.usuarios.ws.clientes.qr;

public interface QRClient {

	QR getQRComercio(long idComercio,int idApp , int idPais, String idioma, QRequest qrequest);
	QRMXResponse getQRComercioMx(QRMXRequest req, int idApp, String idioma );
}
