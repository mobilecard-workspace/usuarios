/**
 * CheckBalanceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.usuarios.ws.clientes.balance;

public class CheckBalanceLocator extends org.apache.axis.client.Service implements com.addcel.usuarios.ws.clientes.balance.CheckBalance {

    public CheckBalanceLocator() {
    }


    public CheckBalanceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CheckBalanceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CheckBalanceSoap
    private java.lang.String CheckBalanceSoap_address = "https://www.idmexico.com.mx/CheckBalance/CheckBalance.asmx";

    public java.lang.String getCheckBalanceSoapAddress() {
        return CheckBalanceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CheckBalanceSoapWSDDServiceName = "CheckBalanceSoap";

    public java.lang.String getCheckBalanceSoapWSDDServiceName() {
        return CheckBalanceSoapWSDDServiceName;
    }

    public void setCheckBalanceSoapWSDDServiceName(java.lang.String name) {
        CheckBalanceSoapWSDDServiceName = name;
    }

    public com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoap getCheckBalanceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CheckBalanceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCheckBalanceSoap(endpoint);
    }

    public com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoap getCheckBalanceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoapStub _stub = new com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoapStub(portAddress, this);
            _stub.setPortName(getCheckBalanceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCheckBalanceSoapEndpointAddress(java.lang.String address) {
        CheckBalanceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoapStub _stub = new com.addcel.usuarios.ws.clientes.balance.CheckBalanceSoapStub(new java.net.URL(CheckBalanceSoap_address), this);
                _stub.setPortName(getCheckBalanceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CheckBalanceSoap".equals(inputPortName)) {
            return getCheckBalanceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "CheckBalance");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "CheckBalanceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CheckBalanceSoap".equals(portName)) {
            setCheckBalanceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
