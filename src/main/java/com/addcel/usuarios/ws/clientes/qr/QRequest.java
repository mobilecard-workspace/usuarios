package com.addcel.usuarios.ws.clientes.qr;

public class QRequest {

	private String expirationDate;
	private String description;
	//private String amount;
	
	public QRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}*/
	
}
