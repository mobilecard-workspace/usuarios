package com.addcel.usuarios.ws.clientes.emaildomains;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmailDomains {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailDomains.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	public boolean isDisposable(String domain){
		try{
			DisposableResponse response = restTemplate.getForObject("https://open.kickbox.com/v1/disposable/"+domain, DisposableResponse.class);
			LOGGER.debug("RETORNANDO RESPUESTA API DISPOSABLE...");
			return response.isDisposable();
		}catch(Exception ex){
			LOGGER.error("ERROR AL LLAMAR API EMAIL DISPOSABLE... ", ex);
			return false;
		}
	}
}
