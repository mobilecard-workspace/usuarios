package com.addcel.usuarios.ws.clientes.qr;

import com.addcel.usuarios.model.vo.BaseResponse;

public class QRMXResponse extends BaseResponse{

	private String qrBase64;
	
	public void setQrBase64(String qrBase64) {
		this.qrBase64 = qrBase64;
	}
	
	public String getQrBase64() {
		return qrBase64;
	}
	
}
