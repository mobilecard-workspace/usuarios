package com.addcel.usuarios.ws.clientes.datacredit;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.addcel.usuarios.model.vo.ExperianUserVO;
import com.addcel.usuarios.model.vo.InsertRequest;

public class DataCreditClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataCreditClient.class);
	//{"tipoIdUsuario":"1","idUsuario":"900374820","nitUsuario":"900374820","clave":"50AVJ","tipoIdCliente":"1"}
	private static final Object CODIGO_RESPUESTA_DATACREDIT = "codigoRespuesta";

	
	public boolean valCuentastub(InsertRequest usuarioVO, ExperianUserVO user){
		
		try{
			
			String xmlRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					+ "<ValidadorCuenta>"
					+ "<Cuenta tipoIdUsuario=\""+user.getTipoIdUsuario()+"\" "
							+ "idUsuario=\""+user.getIdUsuario()+"\" "
							+ "nitUsuario=\""+user.getNitUsuario()+"\" "
							+ "clave=\""+user.getClave()+"\" "
					+ "tipoIdCliente=\""+user.getTipoIdCliente()+"\" "
					+ "idCliente=\""+usuarioVO.getCedula()+"\" "
					+ "ctaCliente=\""+usuarioVO.getCard().getPan()+"\" "
					+ "primerApellido=\""+usuarioVO.getLastName().trim()+"\""
					+ " nombreCliente=\""+usuarioVO.getFirstName().trim()+"\" /></ValidadorCuenta>";
			
			ServicioValidacionImplProxy proxyDataCredit = new ServicioValidacionImplProxy();
			String response = proxyDataCredit.valCuenta(xmlRequest);
			
			String codigoRespuesta = convertStringToDocument(response);
			if("00".equals(codigoRespuesta))
				return true;
			else
				return false;
			
		}catch(Exception ex){
			ex.printStackTrace();
			LOGGER.error("Error de conexcion datacredit ", ex);
			return false;
		}
	}
	
	
	private String convertStringToDocument(String xmlRequest) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		String res = "";
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(
					xmlRequest)));
			if (doc.hasChildNodes()) {
				res = printNote(doc.getChildNodes());
			}
			LOGGER.info("Doc: " +doc);
			return res;
		} catch (Exception e) {
			LOGGER.info("Error validando datacredit", e);
		}
		return null;
	}
	
	
	private String printNote(NodeList nodeList) {
		LOGGER.debug("Validando respuesta: " );
		String codResp = null;
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				LOGGER.debug("nombre nodo: " +tempNode.getNodeName());
				LOGGER.debug("valor nombre " +tempNode.getTextContent());
				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();
					for (int i = 0; i < nodeMap.getLength(); i++) {
						Node node = nodeMap.item(i);
						LOGGER.debug("nombre atributo: "+node.getNodeName());
						LOGGER.debug("valor atributo: " +node.getNodeValue());
						if (CODIGO_RESPUESTA_DATACREDIT.equals(node.getNodeName())) {
							codResp = node.getNodeValue();
							
							break; 
						}
					}
				}
				if (tempNode.hasChildNodes() && codResp == null) {
					printNote(tempNode.getChildNodes());
				}
			}
		}
		return codResp;
	}
	
	public String valCuenta(InsertRequest usuarioVO, ExperianUserVO user){
		try{
			
			String xmlRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					+ "<ValidadorCuenta>"
					+ "<Cuenta tipoIdUsuario=\""+user.getTipoIdUsuario()+"\" "
							+ "idUsuario=\""+user.getIdUsuario()+"\" "
							+ "nitUsuario=\""+user.getNitUsuario()+"\" "
							+ "clave=\""+user.getClave()+"\" "
					+ "tipoIdCliente=\""+user.getTipoIdCliente()+"\" "
					+ "idCliente=\""+usuarioVO.getCedula()+"\" "
					+ "ctaCliente=\""+usuarioVO.getCard().getPan()+"\" "
					+ "primerApellido=\""+usuarioVO.getLastName().trim()+"\""
					+ " nombreCliente=\""+usuarioVO.getFirstName().trim()+"\" /></ValidadorCuenta>";
			
			RestTemplate restTemplate =  new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_XML);
		    HttpEntity<String> request = new HttpEntity<String>(xmlRequest, headers);
		    
		    ResponseEntity<String> response = restTemplate.postForEntity("http://172.24.14.7:8080/ValidacionWS/services/ServicioValidacion", request, String.class);
			return response.getBody();
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
}
