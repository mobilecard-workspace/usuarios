package com.addcel.usuarios.ws.clientes.jumio;

public interface JumioClient {

	JumioVerifyRes verifcarReferencia(String referencia,int idApp,int idPais, String idioma);
	
}
