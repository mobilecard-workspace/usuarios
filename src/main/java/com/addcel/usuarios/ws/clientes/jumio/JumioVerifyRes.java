package com.addcel.usuarios.ws.clientes.jumio;

public class JumioVerifyRes {

	private int code;
	private String message;
	private int jumioStatus;
	
	public JumioVerifyRes() {
		// TODO Auto-generated constructor stub
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getJumioStatus() {
		return jumioStatus;
	}

	public void setJumioStatus(int jumioStatus) {
		this.jumioStatus = jumioStatus;
	}
	
}
