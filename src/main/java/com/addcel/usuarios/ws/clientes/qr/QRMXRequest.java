package com.addcel.usuarios.ws.clientes.qr;

public class QRMXRequest {
	
	private Long idEstablecimiento;
	private Double monto;
	private Integer idPais;
	private String typeQR; //ESTATICO /DINAMICO
	
	public QRMXRequest() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdEstablecimiento() {
		return idEstablecimiento;
	}

	public void setIdEstablecimiento(Long idEstablecimiento) {
		this.idEstablecimiento = idEstablecimiento;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Integer getIdPais() {
		return idPais;
	}

	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}

	public String getTypeQR() {
		return typeQR;
	}

	public void setTypeQR(String typeQR) {
		this.typeQR = typeQR;
	}

}
