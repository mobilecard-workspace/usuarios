/**
 * CheckBalanceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.usuarios.ws.clientes.balance;

public interface CheckBalanceSoap extends java.rmi.Remote {
    public java.math.BigDecimal consultarSaldoTag(java.lang.String tagId) throws java.rmi.RemoteException;
}
