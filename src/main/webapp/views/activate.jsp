<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>MobileCard</title>
    <style>
      p {
        color: #666;
        text-align: center;
        padding-bottom: 20px;
        font-family: 'Roboto', sans-serif;
      }
      img {
        text-align: : center;
        vertical-align: middle;
      }
        .logo {
            width: 200px;
            height: auto;
            text-align: center;
        }
        .sobre {
            width: 50px;
            height: auto;
            margin: 20px;
        }
        .logo_back {
            background-color: #F0F0F0;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        #banner_mobile {
            text-align: center;
            padding: 20px 0;
            padding: 7% 0px;
        }
        p {
            margin-left: 20px;
            margin-right: 20px;
        }
      </style>
  </head>
    
<body>
    <div class="container">
        <div id="banner_mobile" class="col-12">
             <img class="logo" src="https://www.mobilecard.mx/img/Logotipo_MobileCard.svg" href="MobileCard">
        </div>
    </div>
    <div class="row logo_back">
        <div class="fluid-container" style="text-align: center" >
            <img class="sobre" src="https://www.mobilecard.mx/img/Sobre.svg" href="MobileCard">
            <p> ${message} </p>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>