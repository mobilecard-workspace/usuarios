<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<script type="text/javascript"></script>
<link href="<c:url value="/resources/core/styles/style${idApp}.css" />" rel="stylesheet">
<script type="text/javascript" src="<c:url value="/resources/core/js/jquery-3.2.1.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/core/js/vue.js" />"></script>
<title></title>
</head>
<body>
	<div id="envelope" style="-webkit-max-logical-width: 300px;">
		<form id="app" @submit="checkForm" action="${pageContext.request.contextPath}/${idApp}/${idioma}/commerce/resetPass/success" method="post" novalidate="true">
  			<p v-if="errors.length" style="color: red;">
    			<ul>
      				<li v-for="error in errors" style="color: red;">{{ error }}</li>
    			</ul>
  			</p>
			<input type="hidden" value="${cod}" id="cod" name="cod">
			<input type="hidden" value="${idioma}" id="idioma" name="idioma">
  			<p>
    			<label for="pass">
    			<c:choose>
  									<c:when test='${idioma=="es"}'>
               							*Contrase&ntilde;a (Debe contener entre 8 y 12 caracteres)<label>
			  						</c:when>
									  <c:otherwise>
									   *Password (Must contain between 8 and 12 characters)<label>
									  </c:otherwise>
			</c:choose>
    			<input type="password" name="pass" id="pass" v-model="pass" maxlength="12">
  			</p>
  			<p>
    			<label for="cpass">
    			<c:choose>
  									<c:when test='${idioma=="es"}'>
               							*Confirme contraseņa<label>
			  						</c:when>
									  <c:otherwise>
									   *Confirm password<label>
									  </c:otherwise>
			</c:choose>
    			<input type="password" name="cpass" id="cpass" v-model="cpass" maxlength="12">
  			</p>

  			<p>
    			<c:choose>
  									<c:when test='${idioma=="es"}'>
               							<input type="submit" value="Enviar">
			  						</c:when>
									  <c:otherwise>
									   <input type="submit" value="Send">
									  </c:otherwise>
			</c:choose>  
  			</p>

		</form>
	</div>	

<script type="text/javascript" src="<c:url value="/resources/core/js/resetpass.js" />"></script>
</body>
</html>