<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<script type="text/javascript"></script>
<link href="<c:url value="/resources/core/styles/style${idApp}.css" />" rel="stylesheet">

<title></title>
</head>
<body>
	<div id="envelope" style="-webkit-max-logical-width: 300px;">
		<p style="text-align: center;">${message}</p>		
	</div>	
</body>
</html>