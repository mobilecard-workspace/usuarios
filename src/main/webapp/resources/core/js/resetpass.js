const app = new Vue({
  el:'#app',
  data:{
    errors:[],
    pass:null,
    cpass:null
  },
  methods:{
    checkForm:function(e) {
      this.errors = [];
      if(!this.pass || !this.cpass) {
    	  if($("#idioma").val() == "es")
    		  this.errors.push("Ingrese los campos requeridos");
    	  else
    		  this.errors.push("Enter the required fields");
      }else if(this.pass.length < 8 || this.pass.length > 12){
    	  if($("#idioma").val() == "es")
    		  this.errors.push("La contrase\u00F1as debe contener entre 8 y 12 caracteres");
    	  else
    		  this.errors.push("Then password must contain between 8 and 12 characters");
      }else if(!this.validPass(this.pass, this.cpass)) {
    	  if($("#idioma").val() == "es")
    		  this.errors.push("Las contrase\u00F1as no coinciden");
    	  else
    		  this.errors.push("Passwords do not match");
      }
      if(!this.errors.length) return true;
      e.preventDefault();
    },
    validPass:function(pass,cpass) {
    	
    	if(pass == cpass)
        	return true;
        else
        	return false;
    }
  }
})