const app = new Vue({
  el:'#app',
  data:{
    errors:[],
    email:null,
  },
  methods:{
    checkForm:function(e) {
      this.errors = [];
      if(!this.email) {
    	  if($("#idioma").val()=="es")
    		  this.errors.push("Ingrese su email.");
    	  else
    		  this.errors.push("Enter your email.");
      } else if(!this.validEmail(this.email)) {
    	  if($("#idioma").val()=="es")
    		  this.errors.push("Introduzca un email válido.");
    	  else
    		  this.errors.push("Enter a valid email.");
      }
      if(!this.errors.length) return true;
      e.preventDefault();
    },
    validEmail:function(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    }
  }
})